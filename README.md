# Vision 2022

This web platform was developed to give access people to information about our country. 
For example if you want to know about a district&#39;s current or previous MP(s), about the projects of that district and all other information (i.e. police stations, fire service). You can also know about every upazillas, unions details of that district. In short every details of our country can be found here.