-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 01, 2016 at 08:38 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `311project`
--
CREATE DATABASE IF NOT EXISTS `311project` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `311project`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(30) NOT NULL,
  `type` varchar(30) NOT NULL,
  PRIMARY KEY (`username`),
  KEY `admin_id` (`admin_id`,`username`,`password`,`type`),
  KEY `admin_id_2` (`admin_id`,`username`,`password`,`type`),
  KEY `admin_id_3` (`admin_id`,`username`,`password`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `username`, `password`, `type`) VALUES
('DAD', 'Bagerhat', '123456', 'district'),
('DAD', 'Bandarban', '123456', 'district'),
('DAD', 'Barguna', '123456', 'district'),
('DAD', 'Barisal', '123456', 'district'),
('DAD', 'Bhola', '123456', 'district'),
('DAD', 'Bogra', '123456', 'district'),
('DAD', 'Brahmanbaria', '123456', 'district'),
('DAD', 'Chandpur', '123456', 'district'),
('DAD', 'Chapainawabganj', '123456', 'district'),
('DAD', 'Chittagong', '123456', 'district'),
('DAD', 'Chuadanga', '123456', 'district'),
('DAD', 'Comilla', '123456', 'district'),
('DAD', 'Coxs-Bazar', '123456', 'district'),
('DAD', 'Dhaka', '654321', 'district'),
('DAD', 'Dinajpur', '123456', 'district'),
('DAD', 'Faridpur', '123456', 'district'),
('DAD', 'Feni', '123456', 'district'),
('DAD', 'Gaibandha', '123456', 'district'),
('DAD', 'Gazipur', '123456', 'district'),
('DAD', 'Gopalganj', '123456', 'district'),
('DAD', 'Habiganj', '123456', 'district'),
('DAD', 'Jamalpur', '123456', 'district'),
('DAD', 'Jessore', '123456', 'district'),
('DAD', 'Jhalokati', '123456', 'district'),
('DAD', 'Jhenaidah', '123456', 'district'),
('DAD', 'Joypurhat', '123456', 'district'),
('DAD', 'Khagrachhari', '123456', 'district'),
('DAD', 'Khulna', '123456', 'district'),
('DAD', 'Kishoreganj', '123456', 'district'),
('DAD', 'Kurigram', '123456', 'district'),
('DAD', 'Kushtia', '123456', 'district'),
('DAD', 'Lakshmipur', '123456', 'district'),
('DAD', 'Lalmonirhat', '123456', 'district'),
('DAD', 'Madaripur', '123456', 'district'),
('DAD', 'Magura', '123456', 'district'),
('DAD', 'Manikganj', '123456', 'district'),
('DAD', 'Meherpur', '123456', 'district'),
('DAD', 'Moulvibazar', '123456', 'district'),
('DAD', 'Munshiganj', '123456', 'district'),
('DAD', 'Mymensingh', '123456', 'district'),
('DAD', 'Naogaon', '123456', 'district'),
('DAD', 'Narail', '123456', 'district'),
('DAD', 'Narayanganj', '123456', 'district'),
('DAD', 'Narsingdi', '123456', 'district'),
('DAD', 'Natore', '123456', 'district'),
('DAD', 'Netrokona', '123456', 'district'),
('DAD', 'Nilphamari', '123456', 'district'),
('DAD', 'Noakhali', '123456', 'district'),
('DAD', 'Pabna', '123456', 'district'),
('DAD', 'Panchagarh', '123456', 'district'),
('DAD', 'Patuakhali', '123456', 'district'),
('DAD', 'Pirojpur', '123456', 'district'),
('DAD', 'Rajbari', '123456', 'district'),
('DAD', 'Rajshahi', '123456', 'district'),
('DAD', 'Rangamati', '123456', 'district'),
('DAD', 'Rangpur', '123456', 'district'),
('DAD', 'Satkhira', '123456', 'district'),
('DAD', 'Shariatpur', '123456', 'district'),
('DAD', 'Sherpur', '123456', 'district'),
('DAD', 'Sirajganj', '123456', 'district'),
('DAD', 'Sunamganj', '123456', 'district'),
('DAD', 'Sylhet', '123456', 'district'),
('DAD', 'Tangail', '123456', 'district'),
('DAD', 'Thakurgaon', '123456', 'district'),
('super', 'super_admin', '123456', 'superadmin'),
('UAD', 'Agailjhara', '123456', 'upazilla'),
('UAD', 'Alfadanga', '123456', 'upazilla'),
('UAD', 'AliKadam', '123456', 'upazilla'),
('UAD', 'Anwara', '123456', 'upazilla'),
('UAD', 'Azmiriganj', '123456', 'upazilla'),
('UAD', 'Bagerhat_Sadar', '123456', 'upazilla'),
('UAD', 'Bagha', '123456', 'upazilla'),
('UAD', 'Bagmara', '123456', 'upazilla'),
('UAD', 'Bakerganj', '123456', 'upazilla'),
('UAD', 'Baksiganj_', '123456', 'upazilla'),
('UAD', 'Bandarban_Sadar', '123456', 'upazilla'),
('UAD', 'Baniachong', '123456', 'upazilla'),
('UAD', 'Banshkhali', '123456', 'upazilla'),
('UAD', 'Barisha_Sadar', '123456', 'upazilla'),
('UAD', 'Batiaghata', '123456', 'upazilla'),
('UAD', 'Birol', '123456', 'upazilla'),
('UAD', 'Boalkhali', '123456', 'upazilla'),
('UAD', 'Boalmari', '123456', 'upazilla'),
('UAD', 'Chandanaish', '123456', 'upazilla'),
('UAD', 'Charghat', '123456', 'upazilla'),
('UAD', 'Chirirbandar', '123456', 'upazilla'),
('UAD', 'Chitalmari', '123456', 'upazilla'),
('UAD', 'Companiganj', '123456', 'upazilla'),
('UAD', 'Dacope', '123456', 'upazilla'),
('UAD', 'Dakshin_Surma', '123456', 'upazilla'),
('UAD', 'Daulatpur', '123456', 'upazilla'),
('UAD', 'Dewanganj', '123456', 'upazilla'),
('UAD', 'Dhamrai', '123456', 'upazilla'),
('UAD', 'Dhobaura', '123456', 'upazilla'),
('UAD', 'Dinajpur_Sadar', '123456', 'upazilla'),
('UAD', 'Dohar', '123456', 'upazilla'),
('UAD', 'Durgapur', '123456', 'upazilla'),
('UAD', 'Faridpur_Sadar', '123456', 'upazilla'),
('UAD', 'Fulbaria', '123456', 'upazilla'),
('UAD', 'Gaffargaon', '123456', 'upazilla'),
('UAD', 'Gauripur', '123456', 'upazilla'),
('UAD', 'Gaurnadi_Upazila', '123456', 'upazilla'),
('UAD', 'Ghoraghat', '123456', 'upazilla'),
('UAD', 'Godagari', '123456', 'upazilla'),
('UAD', 'Habiganj_Sadar', '123456', 'upazilla'),
('UAD', 'Hizla', '123456', 'upazilla'),
('UAD', 'Islampur', '123456', 'upazilla'),
('UAD', 'Jamalpur_Sadar', '123456', 'upazilla'),
('UAD', 'Jhalokati_Sadar', '123456', 'upazilla'),
('UAD', 'Kachua_', '123456', 'upazilla'),
('UAD', 'Kanaighat', '123456', 'upazilla'),
('UAD', 'Kathalia', '123456', 'upazilla'),
('UAD', 'Kaunia', '123456', 'upazilla'),
('UAD', 'Keraniganj', '123456', 'upazilla'),
('UAD', 'Lama', '123456', 'upazilla'),
('UAD', 'Madarganj', '123456', 'upazilla'),
('UAD', 'Madaripur_Sadar', '123456', 'upazilla'),
('UAD', 'Mithapukur', '123456', 'upazilla'),
('UAD', 'Mollahat', '123456', 'upazilla'),
('UAD', 'Nabiganj', '123456', 'upazilla'),
('UAD', 'Nalchity', '123456', 'upazilla'),
('UAD', 'Nawabganj', '123456', 'upazilla'),
('UAD', 'Pirgacha', '123456', 'upazilla'),
('UAD', 'Rajapur', '123456', 'upazilla'),
('UAD', 'Rampal', '123456', 'upazilla'),
('UAD', 'Rangpur_Sadar', '123456', 'upazilla'),
('UAD', 'Raozan', '123456', 'upazilla'),
('UAD', 'Rowangchhari', '123456', 'upazilla'),
('UAD', 'Ruma', '123456', 'upazilla'),
('UAD', 'Saltha', '123456', 'upazilla'),
('UAD', 'Sandwip', '123456', 'upazilla'),
('UAD', 'Savar', '123456', 'upazilla'),
('UAD', 'Shekhpara', '123456', 'upazilla'),
('UAD', 'Sonadanga', '123456', 'upazilla'),
('UAD', 'Sylhet_Sadar', '123456', 'upazilla'),
('UAD', 'Trishal	', '123456', 'upazilla'),
('UAD', 'Wazirpur', '123456', 'upazilla');

-- --------------------------------------------------------

--
-- Table structure for table `administrators_info`
--

CREATE TABLE IF NOT EXISTS `administrators_info` (
  `adminis_id_info` varchar(30) NOT NULL,
  `position` varchar(60) NOT NULL,
  `address` varchar(100) DEFAULT NULL,
  `contact` varchar(30) DEFAULT NULL,
  `join_date` year(4) DEFAULT NULL,
  `priority` int(1) NOT NULL,
  PRIMARY KEY (`adminis_id_info`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrators_info`
--

INSERT INTO `administrators_info` (`adminis_id_info`, `position`, `address`, `contact`, `join_date`, `priority`) VALUES
('Abbasd52501', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Pirojpur', '9087535', 2009, 3),
('Abduld26272', 'Deputy Commissioner (DC)', 'West Raja Bazar,Joypurhat', '9087564', 2011, 1),
('Abduld45985', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Natore', '9087321', 2010, 2),
('Abduld4792', 'Deputy Commissioner (DC)', 'West Raja Bazar, Nilphamari', '9087453', 2011, 1),
('Abduld53110', 'Deputy Commissioner (DC)', 'West Raja Bazar, Rajbari', '9087123', 2009, 1),
('Abduld55588', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Rangamati', '9087456', 2011, 3),
('Abdund10959', 'Deputy Commissioner (DC)', 'West Raja Bazar,Chittagong', '9087123', 2011, 1),
('Abdusd2023', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Gopalganj', '9027321', 2010, 2),
('Abdusd51645', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Patuakhali', '9087342', 2011, 3),
('Abhijd46937', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Netrokona', '9087321', 2010, 2),
('Abhijd873', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Chandpur', '9087321', 2010, 2),
('Abir d38603', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Moulvibazar', '9087562', 2010, 3),
('Abu Hd121000', 'Deputy Commissioner (DC)', 'West Raja Bazar,Comilla', '9087123', 2011, 1),
('Abu Hd8666', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Chandpur', '9087432', 2011, 3),
('Abul d16544', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Faridpur', '9078651', 2011, 3),
('Abul d25269', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Jhenaidah', '9078651', 2010, 3),
('Abul d30468', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Kurigram', '9086547', 2011, 2),
('Abul d39729', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Munshiganj', '9087654', 2010, 3),
('Abul d40767', 'Superintendent of Police (SP)', 'Haji Shaheb Lane , Mymensingh', '9087654', 2011, 3),
('Abul d58207', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Shariatpur', '9087453', 2015, 3),
('Abul d59119', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Sherpur', '9087654', 2011, 3),
('Abul d61319', 'Deputy Commissioner (DC)', 'West Raja Bazar,Sunamganj', '9087652', 2011, 1),
('Abul d63589', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Tangail', '9087654', 2010, 3),
('Ahmedd44861', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Narsingdi', '9027321', 2010, 2),
('Ahsand12143', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Comilla', '9087456', 2009, 4),
('Ahsand4947', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Pabna', '9087654', 2009, 3),
('Ahsand5710', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Satkhira', '9087456', 2008, 4),
('Akkasd57324', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Satkhira', '9027321', 2010, 2),
('Aktard63795', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Tangail', '9027321', 2010, 2),
('Alim d49631', 'Deputy Commissioner (DC)', 'West Raja Bazar,Pabna', '9087123', 2011, 1),
('Amit d2433', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Bandarban', '9087654', 2009, 3),
('Amit d41999', 'Deputy Commissioner (DC)', 'West Raja Bazar,Naogaon', '9087123', 2011, 1),
('Arafad13535', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Coxs-Bazar', '9087456', 2008, 4),
('Arafad25619', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Jhenaidah', '9087456', 2015, 4),
('Arafad29774', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Kishoreganj', '9087652', 2008, 4),
('Arafad32890', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Lakshmipur', '9087456', 2009, 4),
('Arafad3461', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Madaripur', '9087654', 2011, 3),
('Asif d17819', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Feni', '9087654', 2011, 3),
('Asif d2336', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Jessore', '9087321', 2010, 2),
('Asif d52504', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Pirojpur', '9807564', 2008, 2),
('Asif d56800', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Rangpur', '9087564', 2012, 2),
('Asifud1247', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Bagerhat', '9087563', 2008, 4),
('Asifud20300', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Gopalganj', '9087652', 2008, 4),
('Asifud2937', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Bandarban', '9087456', 2008, 4),
('Asifud31863', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Kushtia', '908754', 2011, 4),
('Asifud44652', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Narsingdi', '9087456', 2008, 4),
('Asifud51338', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Patuakhali', '9087546', 2015, 4),
('Asir d25797', 'Deputy Commissioner (DC)', 'West Raja Bazar, Jhenaidah', '9087123', 2009, 1),
('Atishd1773', 'Deputy Commissioner (DC)', 'West Raja Bazar, Feni', '9087123', 2011, 1),
('Azim d64618', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Thakurgaon', '9087563', 2012, 2),
('Badrud13847', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Coxs-Bazar', '9087321', 2009, 2),
('Badrud6588', 'Deputy Commissioner (DC)', 'West Raja Bazar,Bogra', '9087123', 2011, 1),
('Bidhad61978', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Sunamganj', '9087653', 2009, 4),
('Borhad15876', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Dinajpur', '9087345', 2013, 2),
('d37628', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Meherpur ', '9087654', 2010, 3),
('Didard11100', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Chuadanga', '9087654', 2010, 3),
('Didard22904', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Jamalpur', '9087654', 2011, 3),
('Ehsand52634', 'Deputy Commissioner (DC)', 'West Raja Bazar, Pirojpur', '9087543', 2011, 1),
('Enamud7830', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Brahmanbaria', '9087563', 2008, 4),
('Faisad18510', 'Deputy Commissioner (DC)', 'West Raja Bazar, Gaibandha', '9087345', 2011, 1),
('Faisad46421', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Netrokona', '9087654', 2011, 3),
('Faisad9334', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Chapainawabganj', '9087654', 2010, 3),
('Gazi d59592', 'Deputy Commissioner (DC)', 'West Raja Bazar, Sherpur', '9087123', 2009, 1),
('Gias d2098', 'Deputy Commissioner (DC)', 'West Raja Bazar, Gopalganj', '9087123', 2009, 1),
('Gias d42326', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Narail', '9087456', 2015, 4),
('Golamd61989', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Sunamganj', '9087655', 2010, 2),
('Habibd164', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Bagerhat', '9087654', 2011, 3),
('Habibd29174', 'Deputy Commissioner (DC)', 'West Raja Bazar, Kishoreganj', '9087123', 2009, 1),
('Habibd44728', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Narsingdi', '9087654', 2010, 3),
('Habibd47696', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Nilphamari', '9087564', 2011, 3),
('Habibd57477', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Satkhira', '9087654', 2011, 3),
('Habibd6751', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Bogra', '9087654', 2011, 3),
('Haided10948', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Chittagong', '9087143', 2010, 2),
('Hannad56411', 'Deputy Commissioner (DC)', 'West Raja Bazar,Rangpur', '9087564', 2011, 1),
('Hasand24581', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Jhalokati', '9808123', 2009, 4),
('Hasand25863', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Jhenaidah', '9027321', 2010, 2),
('Hasand35201', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Magura', '9087436', 2008, 4),
('Hasand59202', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Sherpur', '9087652', 2008, 4),
('Hashed21496', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Habiganj', '9086712', 2011, 2),
('Iqbald50244', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Panchagarh', '9087453', 2015, 3),
('Irfand14932', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Dhaka', '9027321', 2010, 2),
('Irfand1847', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Gaibandha', '9087456', 2013, 3),
('Irfand30861', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Kurigram', '9087436', 2012, 4),
('Ishtid3973', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Munshiganj', '9027321', 2010, 2),
('Ishtid50615', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Panchagarh', '9085674', 2011, 2),
('Istiad890', 'Deputy Commissioner (DC)', 'West Raja Bazar, Chandpur', '9087123', 2009, 1),
('Jack d40875', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Mymensingh', '9087321', 2010, 2),
('Jack d5369', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Rajbari', '9027321', 2010, 2),
('Jami d1545', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Dinajpur', '9087651', 2011, 4),
('Jami d46445', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Netrokona', '9087436', 2008, 4),
('Jami d6113', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Bogra', '9087456', 2014, 4),
('Jishad23558', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Jessore', '9087654', 2011, 3),
('Jishad36356', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Manikganj', '9027321', 2010, 2),
('Junaid29673', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Kishoreganj', '9027321', 2010, 2),
('K.M Md14729', 'Deputy Commissioner (DC)', 'Taltola, Shewrapara, Dhaka', '9087123', 2009, 1),
('Kaised21512', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Habiganj', '9087563', 2009, 4),
('Kaised36859', 'Deputy Commissioner (DC)', 'West Raja Bazar, Manikganj', '9087123', 2009, 1),
('Kaised3735', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Meherpur', '9027321', 2010, 2),
('Kaised40265', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane , Mymensingh', '9087456', 2008, 4),
('Kaised47731', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Nilphamari', '9087653', 2008, 2),
('Kaised55808', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Rangamati', '9087654', 2014, 4),
('Kaised64644', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Thakurgaon', '9087654', 2011, 3),
('Kamald21333', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Habiganj', '9081223', 2009, 3),
('Kamrud33105', 'Deputy Commissioner (DC)', 'West Raja Bazar,Lalmonirhat', '9087534', 2013, 1),
('Karimd54253', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Rajshahi', '9087321', 2010, 2),
('Kazi d35219', 'Deputy Commissioner (DC)', 'West Raja Bazar, Magura', '9087123', 2009, 1),
('KhadiDhaka1d14627254', 'Upazila Nirbahi Officer (UNO)', 'Dhamrai', '06222-71034', 2011, 1),
('Khokad35723', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Magura', '9087654', 2011, 3),
('Khokad4267', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Narail', '9087654', 2011, 3),
('Khokad61171', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Sunamganj', '9078651', 2008, 3),
('Latifd31633', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Kushtia', '9027321', 2010, 2),
('Latifd62618', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Sylhet', '9087436', 2009, 4),
('Lotusd29313', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Kishoreganj', '9087654', 2010, 3),
('Mainud37826', 'Deputy Commissioner (DC)', 'West Raja Bazar, Meherpur', '9087123', 2009, 1),
('Mainud39284', 'Deputy Commissioner (DC)', 'West Raja Bazar, Munshiganj', '9087123', 2009, 1),
('Mainud42939', 'Deputy Commissioner (DC)', 'West Raja Bazar, Narail', '9087123', 2009, 1),
('Mainud5589', 'Deputy Commissioner (DC)', 'West Raja Bazar, Rangamati', '9087123', 2009, 1),
('Mamund30615', 'Deputy Commissioner (DC)', 'West Raja Bazar,Kurigram', '9087546', 2007, 1),
('Mamund5517', 'Deputy Commissioner (DC)', 'West Raja Bazar,Bhola', '9087546', 2009, 1),
('Mamund56188', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Rangpur', '9087564', 2014, 4),
('Mannad15157', 'Deputy Commissioner (DC)', 'West Raja Bazar, Dinajpur', '9087546', 2008, 1),
('Mashrd38325', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Moulvibazar', '9087652', 2009, 4),
('Mashrd62527', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Sylhet', '9087563', 2011, 2),
('Md Ald24493', 'Deputy Commissioner (DC)', 'West Raja Bazar,Jhalokati', '9087546', 2011, 1),
('Md Kid5934', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Bhola', '9087563', 1999, 4),
('Md. Hd4231', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Barisal', '9132898', 2013, 2),
('Md. Ld3326', 'Deputy Commissioner (DC)', 'West Raja Bazar, Barguna', '9143987', 2012, 1),
('Md. Md14549', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Mirpur, Dhaka', '9087654', 2011, 3),
('Md. MDhaka2d14519750', 'Upazila Nirbahi Officer (UNO)', 'Dohar', '97654321', 2014, 1),
('Md. MFarid1d1618759', 'Upazila Nirbahi Officer (UNO)', 'Boalmari', '06324-56124', 2012, 1),
('Md. Nd4276', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Barisal', '9087654', 2009, 4),
('Md. Sd4678', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Barisal', '9087154', 2010, 3),
('Md. Sd48128', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Noakhali', '9087321', 2010, 2),
('Md. Sd48459', 'Deputy Commissioner (DC)', 'West Raja Bazar, Noakhali', '9087123', 2011, 1),
('Md. Sd4893', 'Deputy Commissioner (DC)', 'West Raja Bazar,Barisal', '9132897', 2011, 1),
('Mehedd33735', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Lalmonirhat', '9078654', 2012, 3),
('Mehedd34584', 'Deputy Commissioner (DC)', 'West Raja Bazar, Madaripur', '9087123', 2009, 1),
('Mehedd50780', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Panchagarh', '9087561', 2015, 4),
('Milond1114', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Chuadanga', '9027231', 2010, 2),
('Milond20834', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Gopalganj', '9087654', 2011, 3),
('Milond42855', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Narail', '9027321', 2009, 2),
('Milond55114', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Rangamati', '9027123', 2010, 2),
('Mirzad50231', 'Deputy Commissioner (DC)', 'West Raja Bazar,Panchagarh', '9087453', 2008, 1),
('Mirzad52491', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Pirojpur', '9087654', 2006, 4),
('Mirzad54655', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Rajshahi', '9087456', 2015, 4),
('Mirzad64511', 'Deputy Commissioner (DC)', 'West Raja Bazar,Thakurgaon', '9087453', 2011, 1),
('Moin d21867', 'Deputy Commissioner (DC)', 'West Raja Bazar,Habiganj', '9087652', 2010, 1),
('Moktad3418', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Barguna', '9156967', 2016, 3),
('Moshad28830', 'Deputy Commissioner (DC)', 'West Raja Bazar, Khulna', '9087123', 2011, 1),
('Muhitd12773', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Comilla', '9087321', 2013, 2),
('Muhitd4174', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Naogaon', '9087321', 2010, 2),
('Muhitd9283', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Chapainawabganj', '9087456', 2009, 4),
('Muktad56111', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Rangpur', '9087234', 2008, 3),
('Mushfd62384', 'Deputy Commissioner (DC)', 'West Raja Bazar, Sylhet', '9083564', 2010, 1),
('Naimud2765', 'Deputy Commissioner (DC)', 'West Raja Bazar,Khagrachhari', '9087123', 2011, 1),
('Naimud46491', 'Deputy Commissioner (DC)', 'West Raja Bazar, Netrokona', '9087123', 2009, 1),
('Narayd26280', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Joypurhat', '9087652', 2011, 2),
('Nasrud11573', 'Deputy Commissioner (DC)', 'West Raja Bazar, Chuadanga', '9087123', 2009, 1),
('Nasrud47925', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Nilphamari', '9078654', 2008, 4),
('Nazifd2737', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Khagrachhari', '9087654', 2010, 3),
('Nazifd40660', 'Deputy Commissioner (DC)', 'West Raja Bazar, Mymensingh', '9087123', 2009, 1),
('Niaz d51921', 'Deputy Commissioner (DC)', 'West Raja Bazar, Patuakhali', '9087456', 2011, 1),
('Nion d34935', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Madaripur', '9087652', 2014, 4),
('Nizamd27366', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Khagrachhari', '9087321', 2010, 2),
('Noor d31831', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Kushtia', '9078651', 2011, 3),
('Noor d32914', 'Deputy Commissioner (DC)', 'West Raja Bazar,Lakshmipur', '9087123', 2011, 1),
('Noor d3671', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Manikganj', '9078651', 2010, 3),
('Noor d5355', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Rajbari', '9087654', 2010, 3),
('Noor d60523', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Sirajganj', '9087654', 2010, 3),
('Nurund5908', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Bhola', '9087234', 2014, 3),
('Obaidd22897', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Jamalpur', '9027321', 2010, 2),
('Obaidd4828', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Noakhali', '9087456', 2009, 4),
('Obaidd58940', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Shariatpur', '9027321', 2010, 2),
('Pinkud24415', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Jhalokati', '9087123', 2010, 2),
('Qazi d7522', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Brahmanbaria', '9087654', 2011, 3),
('Radifd33232', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Lalmonirhat', '9085674', 2006, 2),
('Raisud28618', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Khulna', '9087321', 2010, 2),
('Rashed37387', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Meherpur ', '9087436', 2008, 4),
('Rashed6334', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Tangail', '9087653', 2009, 4),
('Rezaud9824', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Chapainawabganj', '9087321', 2010, 2),
('Rifatd22763', 'Deputy Commissioner (DC)', 'West Raja Bazar, Jamalpur', '9087123', 2009, 1),
('Rifatd30198', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Kurigram', '9085647', 2007, 3),
('Rifatd45293', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Natore', '9087654', 2011, 3),
('Rifatd7795', 'Deputy Commissioner (DC)', 'West Raja Bazar, Brahmanbaria', '9087123', 2009, 1),
('Rudrod2665', 'Deputy Commissioner (DC)', 'West Raja Bazar,Bandarban', '9087123', 2011, 1),
('Ruhuld54348', 'Deputy Commissioner (DC)', 'West Raja Bazar, Rajshahi', '9087122', 2011, 1),
('Ruhuld57115', 'Deputy Commissioner (DC)', 'West Raja Bazar, Satkhira', '9087123', 2009, 1),
('Rupomd1435', 'Deputy Commissioner (DC)', 'West Raja Bazar, Bagerhat', '9087123', 2009, 1),
('Rupomd3283', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Lakshmipur', '9087654', 2011, 3),
('Rupomd41625', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Naogaon', '9087654', 2010, 3),
('Saidud3304', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Barguna', '9165978', 2011, 2),
('Saifud1029', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Chittagong', '9087456', 2009, 4),
('Saifud1139', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Chuadanga', '9087456', 2008, 4),
('Saifud1611', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Faridpur', '9078654', 2008, 4),
('Saifud16372', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Faridpur', '9027321', 2010, 2),
('Saifud27455', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Khagrachhari', '9087456', 2009, 4),
('Saifud58931', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Shariatpur', '9087456', 2008, 4),
('Saifud5978', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Sherpur', '9087321', 2010, 2),
('Saifud60528', 'Deputy Commissioner (DC)', 'West Raja Bazar,Sirajganj', '9087123', 2009, 1),
('Saifud6940', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Bogra', '9087321', 2010, 2),
('Sajidd23748', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Jessore', '9087436', 2008, 4),
('Sajidd28669', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Khulna', '9087456', 2008, 4),
('Sajidd41755', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Naogaon', '9087456', 2011, 4),
('Sajidd8677', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Chandpur', '9087652', 2015, 4),
('Sakibd38107', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Moulvibazar', '9087563', 2010, 2),
('Salehd26642', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Joypurhat', '9087123', 2010, 3),
('Salmad2809', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Bandarban', '9087321', 2010, 2),
('Salmad38912', 'Deputy Commissioner (DC)', 'West Raja Bazar, Moulvibazar', '9087564', 2011, 1),
('Salmad6487', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Thakurgaon', '9087652', 2012, 4),
('Shafid3590', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Barguna', '9134765', 2014, 4),
('Shah d9818', 'Deputy Commissioner (DC)', 'West Raja Bazar,Chapainawabganj', '9087123', 2011, 1),
('Shahad1817', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Gaibandha', '9087623', 2014, 2),
('Shahrd13350', 'Deputy Commissioner (DC)', 'West Raja Bazar, Coxs-Bazar', '9087123', 2011, 1),
('Shakid17156', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Feni', '9087125', 2013, 2),
('Shamid31537', 'Deputy Commissioner (DC)', 'West Raja Bazar, Kushtia', '9087123', 2009, 1),
('Shamid44140', 'Deputy Commissioner (DC)', 'West Raja Bazar, Narsingdi', '9087123', 2009, 1),
('Shamid58940', 'Deputy Commissioner (DC)', 'West Raja Bazar, Shariatpur', '9087123', 2009, 1),
('Shared49781', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Pabna', '9087321', 2010, 2),
('Sharid35369', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Magura', '9027321', 2010, 2),
('Sheikd14810', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Mirpur, Dhaka', '9087652', 2009, 4),
('Sheikd3260', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Lakshmipur', '9087321', 2010, 2),
('Sheikd5162', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Bhola', '9087542', 2011, 2),
('Shovod34546', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Madaripur', '9027321', 2010, 2),
('Solaid33700', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Lalmonirhat', '9086473', 2016, 4),
('Subrod54278', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Rajshahi', '9087654', 2008, 3),
('Sujitd63716', 'Deputy Commissioner (DC)', 'West Raja Bazar, Tangail', '9087123', 2009, 1),
('Sujond45566', 'Deputy Commissioner (DC)', 'West Raja Bazar, Natore', '9087123', 2011, 1),
('Sultad1541', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Bagerhat', '9087321', 2010, 2),
('Sultad45744', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Natore', '9087456', 2008, 4),
('Sumitd13277', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Coxs-Bazar', '9087654', 2010, 3),
('Sumitd26322', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Joypurhat', '9087654', 2009, 4),
('Sumitd48237', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Noakhali', '9087654', 2011, 3),
('Sumond10793', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Chittagong', '9087654', 2010, 3),
('Sumond16760', 'Deputy Commissioner (DC)', 'West Raja Bazar, Faridpur', '9087123', 2009, 1),
('Sumond24357', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Jhalokati', '9087546', 2011, 3),
('Sumond60597', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar,Sirajganj', '9087321', 2011, 2),
('Surujd51400', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Patuakhali', '9087534', 2012, 2),
('Syed d62192', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Sylhet', '9087561', 2010, 3),
('Tausid23642', 'Deputy Commissioner (DC)', 'West Raja Bazar, Jessore', '9087123', 2009, 1),
('Tipu d28421', 'Superintendent of Police (SP)', 'Haji Shaheb Lane, Khulna', '9087654', 2011, 3),
('Tonmod12578', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Comilla', '9087654', 2010, 3),
('Tonmod15921', 'Superintendent of Police (SP)', 'Haji Shaheb Lane,Dinajpur', '9087453', 2014, 3),
('Tonmod49169', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Pabna', '9087456', 2008, 4),
('Wasi d7177', 'Additional Deputy Commissioner (ADC)', 'West Raja Bazar, Brahmanbaria', '9087321', 2010, 2),
('Wasifd17497', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Feni', '9087456', 2008, 4),
('Wasifd36776', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Manikganj', '9087563', 2008, 4),
('Wasifd39708', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Munshiganj', '9087652', 2008, 4),
('Wasifd53177', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Rajbari', '9087563', 2015, 4),
('Wasifd60635', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Sirajganj', '9087456', 2009, 4),
('Zakird18902', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane, Gaibandha', '9087654', 2011, 4),
('Zakird22219', 'Additional Superintendent of Police (ASP)', 'Haji Shaheb Lane,Jamalpur', '9087456', 2009, 4);

-- --------------------------------------------------------

--
-- Table structure for table `administrators_name`
--

CREATE TABLE IF NOT EXISTS `administrators_name` (
  `adminis_id` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  `dis_id_ad` varchar(40) NOT NULL,
  PRIMARY KEY (`adminis_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrators_name`
--

INSERT INTO `administrators_name` (`adminis_id`, `name`, `dis_id_ad`) VALUES
('Abbasd52501', 'Abbas Uddin', 'd52'),
('Abduld26272', 'Abdul Aziz', 'd26'),
('Abduld45985', 'Abdul Bin Aziz', 'd45'),
('Abduld4792', 'Abdullah Ibn Hossain', 'd47'),
('Abduld53110', 'Abdul Kader', 'd53'),
('Abduld55588', 'Abdullah Ibn Hossain', 'd55'),
('Abdund10959', 'Abdun Noor Tushar', 'd10'),
('Abdusd2023', 'Abdus Sattar', 'd20'),
('Abdusd51645', 'Abdus Salam', 'd51'),
('Abhijd46937', 'Abhijit Asad', 'd46'),
('Abhijd873', 'Abhijit Halder', 'd8'),
('Abir d38603', 'Abir Mahmud', 'd38'),
('Abu Hd121000', 'Abu Haider Roni', 'd12'),
('Abu Hd8666', 'Abu Haider Shaiyyan', 'd8'),
('Abul d16544', 'Abul Haque', 'd16'),
('Abul d25269', 'Abul Haque', 'd25'),
('Abul d30468', 'Abul Mal Abdul Muhit', 'd30'),
('Abul d39729', 'Abul Haque', 'd39'),
('Abul d40767', 'Abul Haque', 'd40'),
('Abul d58207', 'Abul Haque', 'd58'),
('Abul d59119', 'Abul Haque', 'd59'),
('Abul d61319', 'Abul Mal Abdul Muhit', 'd61'),
('Abul d63589', 'Abul Haque', 'd63'),
('Ahmedd44861', 'Ahmed Ibrahim', 'd44'),
('Ahsand12143', 'Ahsan Habib ', 'd12'),
('Ahsand4947', 'Ahsan Habib Himel', 'd49'),
('Ahsand5710', 'Ahsan Habib ', 'd57'),
('Akkasd57324', 'Akkas Uddin', 'd57'),
('Aktard63795', 'Aktar Uddin', 'd63'),
('Alim d49631', 'Alim Ul Karim', 'd49'),
('Amit d2433', 'Amit Majumder', 'd2'),
('Amit d41999', 'Amit Majumder', 'd41'),
('Arafad13535', 'Arafat Rahman', 'd13'),
('Arafad25619', 'Arafat Rahman', 'd25'),
('Arafad29774', 'Arafat Rahman', 'd29'),
('Arafad32890', 'Arafat Rahman', 'd32'),
('Arafad3461', 'Arafat Rahman Aposh', 'd34'),
('Asif d17819', 'Asif Iqbal', 'd17'),
('Asif d2336', 'Asif Iqbal', 'd23'),
('Asif d52504', 'Asif Mahbub', 'd52'),
('Asif d56800', 'Asif Iqbal', 'd56'),
('Asifud1247', 'Asifur Rahman', 'd1'),
('Asifud20300', 'Asifur Rahman', 'd20'),
('Asifud2937', 'Asifur Rahman', 'd2'),
('Asifud31863', 'Asifur Rahman', 'd31'),
('Asifud44652', 'Asifur Rahman', 'd44'),
('Asifud51338', 'Asifur Rahman', 'd51'),
('Asir d25797', 'Asir Intesar', 'd25'),
('Atishd1773', 'Atish Sarker', 'd17'),
('Azim d64618', 'Azim U. Ahmed', 'd64'),
('Badrud13847', 'Badrul Amin', 'd13'),
('Badrud6588', 'Badrul Amin', 'd6'),
('Bidhad61978', 'Bidhan Chondro Roy', 'd61'),
('Borhad15876', 'Borhan Mia', 'd15'),
('Didard11100', 'Didar Ul Islam', 'd11'),
('Didard22904', 'Didar Ul Islam', 'd22'),
('Ehsand52634', 'Ehsan Ul Karim', 'd52'),
('Enamud7830', 'Enamul Islam', 'd7'),
('Faisad18510', 'Faisal Karim', 'd18'),
('Faisad46421', 'Faisal Bashar', 'd46'),
('Faisad9334', 'Faisal Bashar', 'd9'),
('Gazi d59592', 'Gazi Ahsan', 'd59'),
('Gias d2098', 'Gias Uddin Ahmed', 'd20'),
('Gias d42326', 'Gias Ahsan', 'd42'),
('Golamd61989', 'Golam Mowla', 'd61'),
('Habibd164', 'Habib Niaz', 'd1'),
('Habibd29174', 'Habib Ullah Bahar', 'd29'),
('Habibd44728', 'Habib Niaz', 'd44'),
('Habibd47696', 'Habibullah Bahar', 'd47'),
('Habibd57477', 'Habib Niaz', 'd57'),
('Habibd6751', 'Habib Niaz', 'd6'),
('Haided10948', 'Haider Bin Kibria', 'd10'),
('Hannad56411', 'Hannan Uddin', 'd56'),
('Hasand24581', 'Hasan Masud', 'd24'),
('Hasand25863', 'Hasan Mahmud', 'd25'),
('Hasand35201', 'Hasan Masud', 'd35'),
('Hasand59202', 'Hasan Masud', 'd59'),
('Hashed21496', 'Hashem Bin Kibria', 'd21'),
('Iqbald50244', 'Iqbal Ur Rahman', 'd50'),
('Irfand14932', 'Irfan Ibn Hossain', 'd14'),
('Irfand1847', 'Irfan Ibn Hossain', 'd18'),
('Irfand30861', 'Irfan Ibn Haider', 'd30'),
('Ishtid3973', 'Ishtiak Hossain', 'd39'),
('Ishtid50615', 'Ishtiak Hossain', 'd50'),
('Istiad890', 'Istiak Haider', 'd8'),
('Jack d40875', 'Jack Halder', 'd40'),
('Jack d5369', 'Jack Halder', 'd53'),
('Jami d1545', 'Jami Talukder', 'd15'),
('Jami d46445', 'Jami Talukder', 'd46'),
('Jami d6113', 'Jami Talukder', 'd6'),
('Jishad23558', 'Jishan Kabir', 'd23'),
('Jishad36356', 'Jishan Kabir', 'd36'),
('Junaid29673', 'Junaid Kamal', 'd29'),
('K.M Md14729', 'K.M Mehedi Hasan Sonet ', 'd14'),
('Kaised21512', 'Kaiser Haider', 'd21'),
('Kaised36859', 'Kaiser Hamid', 'd36'),
('Kaised3735', 'Kaiser Kabir', 'd37'),
('Kaised40265', 'Kaiser Haider', 'd40'),
('Kaised47731', 'Kaiser Kibria', 'd47'),
('Kaised55808', 'Kaiser Haider', 'd55'),
('Kaised64644', 'Kaiser Kibria', 'd64'),
('Kamald21333', 'Kamal Uddin', 'd21'),
('Kamrud33105', 'Kamrul Islam', 'd33'),
('Karimd54253', 'Karim Molla', 'd54'),
('Kazi d35219', 'Kazi Ashfaque', 'd35'),
('KhadiDhaka1d14627254', 'Khadiza Najnin', 'Dhaka1d14627'),
('Khokad35723', 'Khoka Mir', 'd35'),
('Khokad4267', 'Khoka Mir', 'd42'),
('Khokad61171', 'Khoka Mir', 'd61'),
('Latifd31633', 'Latif Siddique', 'd31'),
('Latifd62618', 'Latifur Rahman', 'd62'),
('Lotusd29313', 'Lotus Kamal', 'd29'),
('Mainud37826', 'Mainul Islam', 'd37'),
('Mainud39284', 'Mainul Islam', 'd39'),
('Mainud42939', 'Mainul Islam', 'd42'),
('Mainud5589', 'Mainul Islam', 'd55'),
('Mamund30615', 'Mamun Molla', 'd30'),
('Mamund5517', 'Mamun Molla', 'd5'),
('Mamund56188', 'Mamun Molla', 'd56'),
('Mannad15157', 'Mannan Bhuiyan', 'd15'),
('Mashrd38325', 'Mashroof Hossain', 'd38'),
('Mashrd62527', 'Mashrafee Mortuza', 'd62'),
('Md Ald24493', 'Md Alamin Hossain', 'd24'),
('Md Kid5934', 'Md Kibria Hasan', 'd5'),
('Md. Hd4231', 'Md. Hasan Ali', 'd4'),
('Md. Ld3326', 'Md. Latifur Rahman', 'd3'),
('Md. Md14549', 'Md. Mustafizur Rahman', 'd14'),
('Md. MDhaka2d14519750', 'Md. Mahbubur Rahman', 'Dhaka2d14519'),
('Md. MFarid1d1618759', 'Md. Mahbubur Rahman', 'Farid1d16187'),
('Md. Nd4276', 'Md. Nuru Miah', 'd4'),
('Md. Sd4678', 'Md. Solaiman Chowdhury', 'd4'),
('Md. Sd48128', 'Md. Solaiman Chowdhuty', 'd48'),
('Md. Sd48459', 'Md. Shakilur Rahman', 'd48'),
('Md. Sd4893', 'Md. Shakilur Rahaman', 'd4'),
('Mehedd33735', 'Mehedi Hasan', 'd33'),
('Mehedd34584', 'Mehedi Hasan Sonet', 'd34'),
('Mehedd50780', 'Mehedi Hasan Sonet', 'd50'),
('Milond1114', 'Milon Ahmed', 'd11'),
('Milond20834', 'Milon Ahmed', 'd20'),
('Milond42855', 'Milon Ahmed', 'd42'),
('Milond55114', 'Milon Ahmed', 'd55'),
('Mirzad50231', 'Mirza Md. Lutfe Elahi', 'd50'),
('Mirzad52491', 'Mirza Galib', 'd52'),
('Mirzad54655', 'Mirza Faqrul', 'd54'),
('Mirzad64511', 'Mirza Galib', 'd64'),
('Moin d21867', 'Moin Uddin Badal', 'd21'),
('Moktad3418', 'Moktar Ali', 'd3'),
('Monjud18372', 'Monju Ahmed', 'd18'),
('Moshad28830', 'Mosharraf Karim', 'd28'),
('Muhitd12773', 'Muhit Islam', 'd12'),
('Muhitd4174', 'Muhit U. Zaman', 'd41'),
('Muhitd9283', 'Muhit Islam', 'd9'),
('Muktad56111', 'Muktar Ali', 'd56'),
('Mushfd62384', 'Mushfiqur Rahim', 'd62'),
('Naimud2765', 'Naimul Bashar', 'd27'),
('Naimud46491', 'Naimul Bashar', 'd46'),
('Narayd26280', 'Narayan Das', 'd26'),
('Nasrud11573', 'Nasrul Hamid', 'd11'),
('Nasrud47925', 'Nasrullah Bin Kashem', 'd47'),
('Nazifd2737', 'Nazif Muhammed', 'd27'),
('Nazifd40660', 'Nazif Muhammed', 'd40'),
('Niaz d51921', 'Niaz Morshed', 'd51'),
('Nion d34935', 'Nion Ahmed', 'd34'),
('Nizamd27366', 'Nizam Uddin', 'd27'),
('Noor d31831', 'Noor Hossain', 'd31'),
('Noor d32914', 'Noor Noby Haider', 'd32'),
('Noor d3671', 'Noor Hossain', 'd36'),
('Noor d5355', 'Noor Hossain', 'd53'),
('Noor d60523', 'Noor Hossain', 'd60'),
('Nurund5908', 'Nurunnobi Faisal', 'd5'),
('Obaidd22897', 'Obaidul Kader', 'd22'),
('Obaidd4828', 'Obaidul Kader', 'd48'),
('Obaidd58940', 'Obaidul Islam', 'd58'),
('Pinkud24415', 'Pinku Deb Nath', 'd24'),
('Qazi d7522', 'Qazi Tanvir', 'd7'),
('Radifd33232', 'Radif Ahmed', 'd33'),
('Raisud28618', 'Raisul Islam', 'd28'),
('Rashed37387', 'Rashed Chowdhury', 'd37'),
('Rashed6334', 'Rashed Chowdhury', 'd63'),
('Rezaud9824', 'Rezaul Kawser', 'd9'),
('Rifatd22763', 'Rifat Mahmud', 'd22'),
('Rifatd30198', 'Rifat Haider', 'd30'),
('Rifatd45293', 'Rifat Mahmud', 'd45'),
('Rifatd7795', 'Rifat Islam', 'd7'),
('Rudrod2665', 'Rudro Tahsin', 'd2'),
('Ruhuld54348', 'Ruhul Mashbu', 'd54'),
('Ruhuld57115', 'Ruhul Amin', 'd57'),
('Rupomd1435', 'Rupom Chowdhury', 'd1'),
('Rupomd3283', 'Rupom Chowdhury', 'd32'),
('Rupomd41625', 'Rupom Chowdhury', 'd41'),
('Saidud3304', 'Saidul Islam', 'd3'),
('Saifud1029', 'Saiful Azim', 'd10'),
('Saifud1139', 'Saiful Azim', 'd11'),
('Saifud1611', 'Saiful Azim', 'd16'),
('Saifud16372', 'Saiful Islam', 'd16'),
('Saifud27455', 'Saiful Azim', 'd27'),
('Saifud58931', 'Saiful Azim', 'd58'),
('Saifud5978', 'Saiful Azam', 'd59'),
('Saifud60528', 'Saiful Islam', 'd60'),
('Saifud6940', 'Saiful Azad', 'd6'),
('Sajidd23748', 'Sajid Mahmud', 'd23'),
('Sajidd28669', 'Sajid Mahmud', 'd28'),
('Sajidd41755', 'Sajid Mahmud', 'd41'),
('Sajidd8677', 'Sajid Mahmud', 'd8'),
('Sakibd38107', 'Sakib Bin Kibria', 'd38'),
('Salehd26642', 'Saleh Uddin', 'd26'),
('Salmad2809', 'Salman Khan', 'd2'),
('Salmad38912', 'Salman Taseen Haque', 'd38'),
('Salmad6487', 'Salman Taseen', 'd64'),
('Shafid3590', 'Shafikul Islam', 'd3'),
('Shah d9818', 'Shah Jahan Khan', 'd9'),
('Shahad1817', 'Shahadat Hossain', 'd18'),
('Shahrd13350', 'Shahriar Alam', 'd13'),
('Shakid17156', 'Shakil Ahmed', 'd17'),
('Shamid31537', 'Shamim Alam', 'd31'),
('Shamid44140', 'Shamim Ahmed', 'd44'),
('Shamid58940', 'Shamim Osman', 'd58'),
('Shared49781', 'Sharek Bin Shiraj', 'd49'),
('Sharid35369', 'Sharif Howlader', 'd35'),
('Sheikd14810', 'Sheikh Faisal Basher', 'd14'),
('Sheikd3260', 'Sheikh Faisal ', 'd32'),
('Sheikd5162', 'Sheikh Salam', 'd5'),
('Shovod34546', 'Shovon Ahmed', 'd34'),
('Solaid33700', 'Solaiman Chowdhury', 'd33'),
('sonneDhaka1d14874780', 'sonnet', 'Dhaka1d148'),
('Subrod54278', 'Subroto Halder', 'd54'),
('Sujitd63716', 'Sujit Majumder', 'd63'),
('Sujond45566', 'Sujon Kumar ', 'd45'),
('Sultad1541', 'Sultan Mahmud', 'd1'),
('Sultad45744', 'Sultan Mahmud', 'd45'),
('Sumitd13277', 'Sumit Narayan', 'd13'),
('Sumitd26322', 'Sumit Majumder', 'd26'),
('Sumitd48237', 'Sumit Narayan', 'd48'),
('Sumond10793', 'Sumon Talukder', 'd10'),
('Sumond16760', 'Sumon Kumar Nath', 'd16'),
('Sumond24357', 'Sumon Kumar Nath', 'd24'),
('Sumond60597', 'Sumon Talukder', 'd60'),
('Surujd51400', 'Suruj Uddin', 'd51'),
('Syed d62192', 'Syed Almas Kabir', 'd62'),
('Tausid23642', 'Tausif Rashid', 'd23'),
('Tipu d28421', 'Tipu Sultan', 'd28'),
('Tonmod12578', 'Tonmoy Sikder', 'd12'),
('Tonmod15921', 'Tonmoy Sikder', 'd15'),
('Tonmod49169', 'Tonmoy Sikder', 'd49'),
('Wasi d7177', 'Wasi Bin Kibria', 'd7'),
('Wasifd17497', 'Wasif Mimo', 'd17'),
('Wasifd36776', 'Wasif Mimo', 'd36'),
('Wasifd39708', 'Wasif Mimo', 'd39'),
('Wasifd53177', 'Wasif Mimo', 'd53'),
('Wasifd60635', 'Wasif Mimo', 'd60'),
('Zakird18902', 'Zakir Hossain', 'd18'),
('Zakird22219', 'Zakir Hossain', 'd22');

-- --------------------------------------------------------

--
-- Table structure for table `budget`
--

CREATE TABLE IF NOT EXISTS `budget` (
  `b_id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) NOT NULL,
  `year` year(4) NOT NULL,
  `dis_id` varchar(100) NOT NULL,
  PRIMARY KEY (`b_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `budget`
--

INSERT INTO `budget` (`b_id`, `amount`, `year`, `dis_id`) VALUES
(1, 200, 2016, 'd14');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `commentator` varchar(60) NOT NULL DEFAULT 'Anonymus',
  `comment` text NOT NULL,
  `p_id` varchar(100) NOT NULL,
  `cmt_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_cmnt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cmt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`commentator`, `comment`, `p_id`, `cmt_id`, `date_cmnt`) VALUES
('Sonnet', 'Waited long for that bridge . At last ...Thank To Our chairman', 'DhaMd_Bha924', 1, '2016-08-01 22:59:03'),
('Biplob', 'Really Very happy to see that', 'DhaMd_Bha924', 2, '2016-08-01 22:59:03'),
('Tauhid', 'Thanks Mr. Shohel', 'DhaMojJad960', 3, '2016-08-01 22:59:03'),
('Irfan', 'That school was badly needed', 'DhaMojJad960', 4, '2016-08-01 23:02:14'),
('Sonnet', 'Another Comment', 'DhaMd_Bha924', 5, '2016-08-01 23:03:19'),
('', 'Someone does not want to reveal his/her name', 'DhaMd_Bha924', 6, '2016-08-01 23:04:02');

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE IF NOT EXISTS `district` (
  `dis_id` varchar(11) NOT NULL DEFAULT '0',
  `name` varchar(20) DEFAULT NULL,
  `division_name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`dis_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`dis_id`, `name`, `division_name`) VALUES
('d1', 'Bagerhat', 'Khulna'),
('d10', 'Chittagong', 'Chittagong'),
('d11', 'Chuadanga', 'Khulna'),
('d12', 'Comilla', 'Chittagong'),
('d13', 'Coxs-Bazar', 'Chittagong'),
('d14', 'Dhaka', 'Dhaka'),
('d15', 'Dinajpur', 'Rangpur'),
('d16', 'Faridpur', 'Dhaka'),
('d17', 'Feni', 'Chittagong'),
('d18', 'Gaibandha', 'Rangpur'),
('d19', 'Gazipur', 'Dhaka'),
('d2', 'Bandarban', 'Chittagong'),
('d20', 'Gopalganj', 'Dhaka'),
('d21', 'Habiganj', 'Sylhet'),
('d22', 'Jamalpur', 'Mymensingh'),
('d23', 'Jessore', 'Khulna'),
('d24', 'Jhalokati', 'Barisal'),
('d25', 'Jhenaidah', 'Khulna'),
('d26', 'Joypurhat', 'Rajshahi'),
('d27', 'Khagrachhari', 'Chittagong'),
('d28', 'Khulna', 'Khulna'),
('d29', 'Kishoreganj', 'Dhaka'),
('d3', 'Barguna', 'Barisal'),
('d30', 'Kurigram', 'Rangpur'),
('d31', 'Kushtia', 'Khulna'),
('d32', 'Lakshmipur', 'Chittagong'),
('d33', 'Lalmonirhat', 'Rangpur'),
('d34', 'Madaripur', 'Dhaka'),
('d35', 'Magura', 'Khulna'),
('d36', 'Manikganj', 'Dhaka'),
('d37', 'Meherpur', 'Khulna'),
('d38', 'Moulvibazar', 'Sylhet'),
('d39', 'Munshiganj', 'Dhaka'),
('d4', 'Barisal', 'Barisal'),
('d40', 'Mymensingh', 'Mymensingh'),
('d41', 'Naogaon', 'Rajshahi'),
('d42', 'Narail', 'Khulna'),
('d43', 'Narayanganj', 'Dhaka'),
('d44', 'Narsingdi', 'Dhaka'),
('d45', 'Natore', 'Rajshahi'),
('d46', 'Netrokona', 'Mymensingh'),
('d47', 'Nilphamari', 'Rangpur'),
('d48', 'Noakhali', 'Chittagong'),
('d49', 'Pabna', 'Rajshahi'),
('d5', 'Bhola', 'Barisal'),
('d50', 'Panchagarh', 'Rangpur'),
('d51', 'Patuakhali', 'Barisal'),
('d52', 'Pirojpur', 'Barisal'),
('d53', 'Rajbari', 'Dhaka'),
('d54', 'Rajshahi', 'Rajshahi'),
('d55', 'Rangamati', 'Chittagong'),
('d56', 'Rangpur', 'Rangpur'),
('d57', 'Satkhira', 'Khulna'),
('d58', 'Shariatpur', 'Dhaka'),
('d59', 'Sherpur', 'Mymensingh'),
('d6', 'Bogra', 'Rajshahi'),
('d60', 'Sirajganj', 'Rajshahi'),
('d61', 'Sunamganj', 'Sylhet'),
('d62', 'Sylhet', 'Sylhet'),
('d63', 'Tangail', 'Dhaka'),
('d64', 'Thakurgaon', 'Rangpur'),
('d7', 'Brahmanbaria', 'Chittagong'),
('d8', 'Chandpur', 'Chittagong'),
('d9', 'Chapainawabganj', 'Rajshahi');

-- --------------------------------------------------------

--
-- Table structure for table `election_seats`
--

CREATE TABLE IF NOT EXISTS `election_seats` (
  `name` varchar(25) DEFAULT NULL,
  `dis_id` varchar(30) DEFAULT NULL,
  `seat_id` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`seat_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `election_seats`
--

INSERT INTO `election_seats` (`name`, `dis_id`, `seat_id`) VALUES
('Bagerhat-1', 'd1', 'Bager1'),
('Bagerhat-2', 'd1', 'Bager2'),
('Bandarban-1', 'd2', 'Banda1'),
('Bandarban-2', 'd2', 'Banda2'),
('Barguna-1', 'd3', 'Bargu1'),
('Barguna-2', 'd3', 'Bargu2'),
('Barisal-1', 'd4', 'Baris1'),
('Barisal-2', 'd4', 'Baris2'),
('Barisal-3', 'd4', 'Baris3'),
('Barisal-4', 'd4', 'Baris4'),
('Bhola-1', 'd5', 'Bhola1'),
('Bhola-2', 'd5', 'Bhola2'),
('Bogra-1', 'd6', 'Bogra1'),
('Bogra-2', 'd6', 'Bogra2'),
('Bogra-3', 'd6', 'Bogra3'),
('Brahmanbaria-1', 'd7', 'Brahm1'),
('Brahmanbaria-2', 'd7', 'Brahm2'),
('Chandpur-1', 'd8', 'Chand1'),
('Chandpur-2', 'd8', 'Chand2'),
('Chapainawabganj-1', 'd9', 'Chapa1'),
('Chapainawabganj-2', 'd9', 'Chapa2'),
('Chittagong-1', 'd10', 'Chitt1'),
('Chittagong-2', 'd10', 'Chitt2'),
('Chittagong-3', 'd10', 'Chitt3'),
('Chittagong-4', 'd10', 'Chitt4'),
('Chuadanga-1', 'd11', 'Chuad1'),
('Chuadanga-2', 'd11', 'Chuad2'),
('Comilla-1', 'd12', 'Comil1'),
('Comilla-2', 'd12', 'Comil2'),
('Coxs-Bazar-1', 'd13', 'Coxs-1'),
('Coxs-Bazar-2', 'd13', 'Coxs-2'),
('Dhaka-1', 'd14', 'Dhaka1'),
('Dhaka-2', 'd14', 'Dhaka2'),
('Dhaka-3', 'd14', 'Dhaka3'),
('Dhaka-4', 'd14', 'Dhaka4'),
('Dinajpur-1', 'd15', 'Dinaj1'),
('Dinajpur-2', 'd15', 'Dinaj2'),
('Faridpur-1', 'd16', 'Farid1'),
('Faridpur-2', 'd16', 'Farid2'),
('Feni-1', 'd17', 'Feni-1'),
('Feni-2', 'd17', 'Feni-2'),
('Gaibandha-1', 'd18', 'Gaiba1'),
('Gaibandha-2', 'd18', 'Gaiba2'),
('Gazipur-1', 'd19', 'Gazip1'),
('Gazipur-2', 'd19', 'Gazip2'),
('Gazipur-3', 'd19', 'Gazip3'),
('Gopalganj-1', 'd20', 'Gopal1'),
('Gopalganj-2', 'd20', 'Gopal2'),
('Habiganj-1', 'd21', 'Habig1'),
('Habiganj-2', 'd21', 'Habig2'),
('Habiganj-3', 'd21', 'Habig3'),
('Jamalpur-1', 'd22', 'Jamal1'),
('Jamalpur-2', 'd22', 'Jamal2'),
('Jamalpur-3', 'd22', 'Jamal3'),
('Jessore-1', 'd23', 'Jesso1'),
('Jessore-2', 'd23', 'Jesso2'),
('Jessore-3', 'd23', 'Jesso3'),
('Jessore-4', 'd23', 'Jesso4'),
('Jhalokati-1', 'd24', 'Jhalo1'),
('Jhalokati-2', 'd24', 'Jhalo2'),
('Jhalokati-3', 'd24', 'Jhalo3'),
('Jhenaidah-1', 'd25', 'Jhena1'),
('Jhenaidah-2', 'd25', 'Jhena2'),
('Joypurhat-1', 'd26', 'Joypu1'),
('Joypurhat-2', 'd26', 'Joypu2'),
('Joypurhat-3', 'd26', 'Joypu3'),
('Khagrachhari-1', 'd27', 'Khagr1'),
('Khagrachhari-2', 'd27', 'Khagr2'),
('Khagrachhari-3', 'd27', 'Khagr3'),
('Khulna-1', 'd28', 'Khuln1'),
('Khulna-2', 'd28', 'Khuln2'),
('Khulna-3', 'd28', 'Khuln3'),
('Khulna-4', 'd28', 'Khuln4'),
('Kishoreganj-1', 'd29', 'Kisho1'),
('Kishoreganj-2', 'd29', 'Kisho2'),
('Kurigram-1', 'd30', 'Kurig1'),
('Kurigram-2', 'd30', 'Kurig2'),
('Kushtia-1', 'd31', 'Kusht1'),
('Kushtia-2', 'd31', 'Kusht2'),
('Kushtia-3', 'd31', 'Kusht3'),
('Kushtia-4', 'd31', 'Kusht4'),
('Lakshmipur-1', 'd32', 'Laksh1'),
('Lakshmipur-2', 'd32', 'Laksh2'),
('Lalmonirhat-1', 'd33', 'Lalmo1'),
('Lalmonirhat-2', 'd33', 'Lalmo2'),
('Lalmonirhat-3', 'd33', 'Lalmo3'),
('Madaripur-1', 'd34', 'Madar1'),
('Madaripur-2', 'd34', 'Madar2'),
('Madaripur-3', 'd34', 'Madar3'),
('Madaripur-4', 'd34', 'Madar4'),
('Magura-1', 'd35', 'Magur1'),
('Magura-2', 'd35', 'Magur2'),
('Magura-3', 'd35', 'Magur3'),
('Manikganj-1', 'd36', 'Manik1'),
('Manikganj-2', 'd36', 'Manik2'),
('Manikganj-3', 'd36', 'Manik3'),
('Meherpur-1', 'd37', 'Meher1'),
('Meherpur-2', 'd37', 'Meher2'),
('Meherpur-3', 'd37', 'Meher3'),
('Moulvibazar-1', 'd38', 'Moulv1'),
('Moulvibazar-2', 'd38', 'Moulv2'),
('Munshiganj-1', 'd39', 'Munsh1'),
('Munshiganj-2', 'd39', 'Munsh2'),
('Mymensingh-1', 'd40', 'Mymen1'),
('Mymensingh-2', 'd40', 'Mymen2'),
('Mymensingh-3', 'd40', 'Mymen3'),
('Naogaon-1', 'd41', 'Naoga1'),
('Naogaon-2', 'd41', 'Naoga2'),
('Naogaon-3', 'd41', 'Naoga3'),
('Narail-1', 'd42', 'Narai1'),
('Narail-2', 'd42', 'Narai2'),
('Narayanganj-1', 'd43', 'Naray1'),
('Narayanganj-2', 'd43', 'Naray2'),
('Narayanganj-3', 'd43', 'Naray3'),
('Narayanganj-4', 'd43', 'Naray4'),
('Narsingdi-1', 'd44', 'Narsi1'),
('Narsingdi-2', 'd44', 'Narsi2'),
('Narsingdi-3', 'd44', 'Narsi3'),
('Natore-1', 'd45', 'Nator1'),
('Natore-2', 'd45', 'Nator2'),
('Netrokona-1', 'd46', 'Netro1'),
('Netrokona-2', 'd46', 'Netro2'),
('Netrokona-3', 'd46', 'Netro3'),
('Nilphamari-1', 'd47', 'Nilph1'),
('Nilphamari-2', 'd47', 'Nilph2'),
('Noakhali-1', 'd48', 'Noakh1'),
('Noakhali-2', 'd48', 'Noakh2'),
('Noakhali-3', 'd48', 'Noakh3'),
('Pabna-1', 'd49', 'Pabna1'),
('Pabna-2', 'd49', 'Pabna2'),
('Panchagarh-1', 'd50', 'Panch1'),
('Panchagarh-2', 'd50', 'Panch2'),
('Panchagarh-3', 'd50', 'Panch3'),
('Patuakhali-1', 'd51', 'Patua1'),
('Patuakhali-2', 'd51', 'Patua2'),
('Patuakhali-3', 'd51', 'Patua3'),
('Patuakhali-4', 'd51', 'Patua4'),
('Pirojpur-1', 'd52', 'Piroj1'),
('Pirojpur-2', 'd52', 'Piroj2'),
('Pirojpur-3', 'd52', 'Piroj3'),
('Rajbari-1', 'd53', 'Rajba1'),
('Rajbari-2', 'd53', 'Rajba2'),
('Rajshahi-1', 'd54', 'Rajsh1'),
('Rajshahi-2', 'd54', 'Rajsh2'),
('Rajshahi-3', 'd54', 'Rajsh3'),
('Rajshahi-4', 'd54', 'Rajsh4'),
('Rangamati-1', 'd55', 'Ranga1'),
('Rangamati-2', 'd55', 'Ranga2'),
('Rangamati-3', 'd55', 'Ranga3'),
('Rangpur-1', 'd56', 'Rangp1'),
('Rangpur-2', 'd56', 'Rangp2'),
('Satkhira-1', 'd57', 'Satkh1'),
('Satkhira-2', 'd57', 'Satkh2'),
('Satkhira-3', 'd57', 'Satkh3'),
('Shariatpur-1', 'd58', 'Shari1'),
('Shariatpur-2', 'd58', 'Shari2'),
('Sherpur-1', 'd59', 'Sherp1'),
('Sherpur-2', 'd59', 'Sherp2'),
('Sherpur-3', 'd59', 'Sherp3'),
('Sirajganj-1', 'd60', 'Siraj1'),
('Sirajganj-2', 'd60', 'Siraj2'),
('Sirajganj-3', 'd60', 'Siraj3'),
('Sirajganj-4', 'd60', 'Siraj4'),
('Sunamganj-1', 'd61', 'Sunam1'),
('Sunamganj-2', 'd61', 'Sunam2'),
('Sunamganj-3', 'd61', 'Sunam3'),
('Sylhet-1', 'd62', 'Sylhe1'),
('Sylhet-2', 'd62', 'Sylhe2'),
('Tangail-1', 'd63', 'Tanga1'),
('Tangail-2', 'd63', 'Tanga2'),
('Tangail-3', 'd63', 'Tanga3'),
('Thakurgaon-1', 'd64', 'Thaku1'),
('Thakurgaon-2', 'd64', 'Thaku2'),
('Thakurgaon-3', 'd64', 'Thaku3'),
('Thakurgaon-4', 'd64', 'Thaku4');

-- --------------------------------------------------------

--
-- Table structure for table `elective_representative`
--

CREATE TABLE IF NOT EXISTS `elective_representative` (
  `name` varchar(50) NOT NULL,
  `position` varchar(90) NOT NULL,
  `address` varchar(90) NOT NULL,
  `contact` int(11) NOT NULL,
  `join_date` date NOT NULL,
  `location_id` varchar(90) NOT NULL,
  `priority` int(3) NOT NULL,
  `er_id` varchar(100) NOT NULL,
  `party` varchar(80) NOT NULL,
  PRIMARY KEY (`er_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `elective_representative`
--

INSERT INTO `elective_representative` (`name`, `position`, `address`, `contact`, `join_date`, `location_id`, `priority`, `er_id`, `party`) VALUES
('Abdul Aziz Sarker', 'Chairman', 'West Taltola ,Bhararia', 9132456, '2014-07-20', 'BharaDhaka1d14627987', 1, 'AbdBharaDhaka1d14627987252', 'Bangladesh-Awami-League'),
('Abdul Muhit Uddin', 'Chairman', 'West Kazi Para , Bilaspur', 9876453, '2016-07-05', 'BilasDhaka2d14519651', 1, 'AbdBilasDhaka2d14519651736', 'Bangladesh-Nationalist-Party[BNP]'),
('Abu Ibrahim', 'Chairman', 'North Farmgate, Buraich', 9067854, '2016-07-12', 'BuraiFarid1d16947829', 1, 'AbuBuraiFarid1d16947829587', 'Bangladesh-Awami-League'),
('Alamgir Hossain', 'Upazilla-Chairman', 'Dohar Super Market, Dohar', 938472, '2013-01-01', 'Dhaka2d14519', 1, 'AlamgDhaka2d14519174', 'Bangladesh-Awami-League'),
('Gour Gobinda', 'Chairman', 'West Kalyanpur, Mahmudpur', 9132653, '2016-07-21', 'MahmuDhaka2d14519725', 1, 'GouMahmuDhaka2d14519725896', 'Bangladesh-Awami-League'),
('Jack Halder', 'Chairman', 'South Puler Par, Chatul', 9087564, '2016-07-11', 'ChatuFarid1d16187297', 1, 'JacChatuFarid1d16187297453', 'Bangladesh-Awami-League'),
('Jomirun Necha', 'Upazilla-Female-Vice-Chairman', 'Dohar Super Market, Dohar', 9347212, '2013-01-01', 'Dhaka2d14519', 3, 'JomirDhaka2d14519670', 'Bangladesh-Nationalist-Party[BNP]'),
('Khodeja Begum', 'Upazilla-Female-Vice-Chairman', 'Dhamrai Paka Mosjid, Dhamrai', 9236234, '2015-01-01', 'Dhaka1d14627', 3, 'KhodeDhaka1d14627284', 'Bangladesh-Awami-League'),
('Md Tamij Uddin', 'Upazilla-Chairman', 'Dhamrai Paka Mosjid, Dhamrai', 9203746, '2014-01-01', 'Dhaka1d14627', 1, 'Md TaDhaka1d14627479', 'Bangladesh-Nationalist-Party[BNP]'),
('Mozammel Ahmed', 'Upazilla-Vice-Chairman', 'Dohar Super Market, Dohar', 34845734, '2013-01-01', 'Dhaka2d14519', 2, 'MozamDhaka2d14519136', 'Jatiya-Party'),
('Nazrul Islam', 'Chairman', 'West Kalyanpur, Alfadanga', 9087654, '2016-07-26', 'AlfadFarid1d16947251', 1, 'NazAlfadFarid1d16947251709', 'Bangladesh-Awami-League'),
('Nizam Uddin', 'Chairman', 'North Farmgate, Dadpur', 9087432, '2016-07-04', 'DadpuFarid1d16187531', 1, 'NizDadpuFarid1d16187531647', 'Bangladesh-Jatiya-Party[BJP]'),
('Nuruzzaman Mithu', 'Chairman', 'South Rampura,Kushumhati', 7906746, '2016-07-04', 'KushuDhaka2d14519581', 1, 'NurKushuDhaka2d14519581778', 'Bangladesh-Jatiya-Party[BJP]'),
('Shamsur Rahman Khan', 'Upazilla-Vice-Chairman', 'Dhamrai Paka Mosjid, Dhamrai', 9237523, '2014-01-01', 'Dhaka1d14627', 2, 'ShamsDhaka1d14627410', 'Bangladesh-Awami-League'),
('Sohel Taj', 'Chairman', 'East Taltola,Jadabpur', 9132653, '2016-05-16', 'JadabDhaka1d14627567', 1, 'SohJadabDhaka1d1462756788', 'Jatiya-Party'),
('Suman Biswas', 'Chairman', 'South Puler Par, Sanora', 9786543, '2016-07-15', 'SanorDhaka1d14627979', 1, 'SumSanorDhaka1d1462797985', 'Bangladesh-Awami-League'),
('Suruj Uddin Bhuiyan', 'Chairman', 'West Kalyanpur, Chandpur', 9876875, '2016-07-19', 'ChandFarid1d16187443', 1, 'SurChandFarid1d1618744391', 'Bangladesh-Awami-League'),
('Wasif Kader', 'Chairman', 'South Puler Par, Bana', 9087651, '2016-07-03', 'BanaFarid1d16947227', 1, 'WasBanaFarid1d16947227206', 'Jatiya-Party');

-- --------------------------------------------------------

--
-- Table structure for table `leader_former_position`
--

CREATE TABLE IF NOT EXISTS `leader_former_position` (
  `fposition` varchar(30) NOT NULL,
  `start` year(4) NOT NULL,
  `end` year(4) NOT NULL,
  `leader_id_fpos` varchar(40) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `leader_former_position`
--

INSERT INTO `leader_former_position` (`fposition`, `start`, `end`, `leader_id_fpos`, `id`) VALUES
('Member-of-Parliament (MP)', 2008, 2013, 'Md. SJha731', 1),
('Member-of-Parliament (MP)', 1996, 2001, 'Md. SJha732', 2),
('Member-of-Parliament (MP)', 2001, 2007, 'Md. SJha732', 3),
('Minister', 2008, 2013, 'AtishJha565', 4),
('Member-of-Parliament (MP)', 1996, 2001, 'AbrarJha929', 5),
('Member-of-Parliament (MP)', 2001, 2007, 'AbrarJha929', 6),
('Member-of-Parliament (MP)', 2008, 2013, 'AbdunJha550', 7),
('Minister', 2001, 2007, 'SheikJha645', 8),
('Member-of-Parliament (MP)', 1996, 2001, 'Md. SBar779', 9),
('Member-of-Parliament (MP)', 2001, 2007, 'Md. SBar779', 10),
('Minister', 2008, 2013, 'Md. SBar779', 11),
('Member-of-Parliament (MP)', 1996, 2001, 'AbrarBar984', 12),
('Member-of-Parliament (MP)', 2008, 2013, 'AbrarBar984', 13),
('Minister', 2008, 2013, 'AtishBar258', 14),
('Member-of-Parliament (MP)', 2008, 2013, 'AbdunBar228', 15),
('Minister', 2001, 2007, 'SheikBar514', 16),
('Member-of-Parliament (MP)', 2001, 2007, 'ObaidBar459', 17),
('Member-of-Parliament (MP)', 2008, 2013, 'ObaidBar459', 18),
('Member-of-Parliament (MP)', 1996, 2001, 'Md. SDha885', 19),
('Member-of-Parliament (MP)', 2001, 2006, 'Md. SDha885', 20),
('Minister', 2008, 2013, 'Md. SDha885', 21),
('Member-of-Parliament (MP)', 1996, 2001, 'AbrarDha870', 22),
('Member-of-Parliament (MP)', 2008, 2013, 'AbrarDha870', 23),
('Minister', 2008, 2013, 'AtishDha980', 24),
('Member-of-Parliament (MP)', 2008, 2013, 'AbdunDha581', 25),
('Minister', 2001, 2006, 'SheikDha698', 26),
('Member-of-Parliament (MP)', 2001, 2006, 'ObaidDha586', 27),
('Member-of-Parliament (MP)', 2008, 2013, 'ObaidDha586', 28),
('Member-of-Parliament (MP)', 1996, 2001, 'Md. SFar545', 29),
('Member-of-Parliament (MP)', 2001, 2006, 'Md. SFar545', 30),
('Minister', 2008, 2013, 'Md. SFar545', 31),
('Member-of-Parliament (MP)', 1996, 2001, 'AbrarFar971', 32),
('Member-of-Parliament (MP)', 2008, 2013, 'AbrarFar971', 33),
('Member-of-Parliament (MP)', 2008, 2013, 'AtishFar92', 34),
('Member-of-Parliament (MP)', 1996, 2001, 'Md. SKhu101', 35),
('Member-of-Parliament (MP)', 2001, 2006, 'Md. SKhu101', 36),
('Minister', 2008, 2013, 'Md. SKhu101', 37),
('Member-of-Parliament (MP)', 1996, 2001, 'AbrarKhu407', 38),
('Member-of-Parliament (MP)', 2008, 2013, 'AbrarKhu407', 39),
('Minister', 2008, 2013, 'AtishKhu390', 40),
('Member-of-Parliament (MP)', 2001, 2006, 'ObaidKhu567', 41),
('Member-of-Parliament (MP)', 2008, 2013, 'ObaidKhu567', 42),
('Member-of-Parliament (MP)', 2008, 2013, 'AbdunKhu695', 43),
('Member-of-Parliament (MP)', 2001, 2006, 'SheikKhu58', 44),
('Member-of-Parliament (MP)', 1996, 2001, 'AbrarBag960', 45),
('Member-of-Parliament (MP)', 2008, 2013, 'AbrarBag960', 46),
('Member-of-Parliament (MP)', 2008, 2013, 'AtishBag513', 47),
('Member-of-Parliament (MP)', 1996, 2001, 'Md. SBag547', 48),
('Member-of-Parliament (MP)', 2001, 2006, 'Md. SBag547', 49),
('Minister', 2008, 2013, 'Md. SBag547', 50);

-- --------------------------------------------------------

--
-- Table structure for table `leader_info`
--

CREATE TABLE IF NOT EXISTS `leader_info` (
  `leader_id_info` varchar(30) NOT NULL,
  `current_position` varchar(80) NOT NULL,
  `address` varchar(100) NOT NULL,
  `contact` varchar(30) NOT NULL,
  `party` varchar(80) NOT NULL,
  `bio` text NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`leader_id_info`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leader_info`
--

INSERT INTO `leader_info` (`leader_id_info`, `current_position`, `address`, `contact`, `party`, `bio`, `priority`) VALUES
('AbdunBar228', 'Member-of-Parliament (MP)', 'West Mirpur, Barisal', '9123658', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('AbdunDha581', 'Member-of-Parliament (MP)', ' West Mirpur,Dhaka', '9123658', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('AbdunJha550', 'Member-of-Parliament (MP)', 'West Mirpur, Jhalokati', '9123658', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('AbdunKhu695', 'Member-of-Parliament (MP)', 'West Mirpur,Khulna', '9023654', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('AbrarBag960', 'Former-Member-Of-Parliament', 'West Mirpur,Bagerhat', '9087123', 'Bangladesh-Nationalist-Party[BNP]', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('AbrarBar984', 'Former-Member-Of-Parliament', 'West Kazir Par,Barisal', '9012654', 'Bangladesh-Nationalist-Party[BNP]', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('AbrarDha870', 'Former-Member-Of-Parliament', 'West Kazir Par, Dhaka', '9012654', 'Bangladesh-Nationalist-Party[BNP]', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('AbrarFar971', 'Former-Member-Of-Parliament', 'West Kazir Par,Faridpur', '9123652', 'Bangladesh-Nationalist-Party[BNP]', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('AbrarJha929', 'Former-Member-Of-Parliament', 'West Mirpur, Jhalokati', '9125469', 'Bangladesh-Nationalist-Party[BNP]', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it. ', 0),
('AbrarKhu407', 'Former-Member-Of-Parliament', 'West Mirpur,Faridpur', '9132598', 'Bangladesh-Nationalist-Party[BNP]', ' He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('AtishBag513', 'Member-of-Parliament (MP)', 'West Raja Bazar, Bagerhat', '9123658', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('AtishBar258', 'Member-of-Parliament (MP)', 'West Kazir Par,Barisal', '9132598', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('AtishDha980', 'Member-of-Parliament (MP)', 'West Kazir Par,Dhaka', '9132598', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('AtishFar92', 'Member-of-Parliament (MP)', 'West Raja Bazar, Faridpur', '9132598', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('AtishJha565', 'Member-of-Parliament (MP)', 'West Mirpur, Jhalokati', '9135624', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('AtishKhu390', 'Member-of-Parliament (MP)', 'West Raja Bazar, Khulna', '9123658', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('IrfanBag824', 'MP-Candidate', 'West Mirpur,Bagerhat', '9087123', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('IrfanBar924', 'MP-Candidate', 'West Mirpur,Barisal', '9132598', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('IrfanDha817', 'MP-Candidate', 'West Mirpur,Dhaka', '9132598', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('IrfanFar343', 'MP-Candidate', 'West Mirpur,Faridpur', '9087123', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('IrfanJha611', 'MP-Candidate', 'West Kamalapur,Jhalokati', '9132598', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('IrfanKhu354', 'MP-Candidate', 'West Mirpur,Faridpur', '9087123', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('KayesBar413', 'MP-Candidate', 'West Mirpur,Barisal', '9023654', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('KayesDha830', 'MP-Candidate', 'West Mirpur,Dhaka', '9023654', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('KayesKhu412', 'MP-Candidate', 'West Kazir Par,Faridpur', '9023654', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('LutfaBar328', 'MP-Candidate', 'West Kazir Par,Barisal', '9123658', 'Bangladesh-Awami-League', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('LutfaDha769', 'MP-Candidate', 'West Kazir Par,Barisal', '9123658', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('LutfaJha968', 'MP-Candidate', 'West Kazir Par,Jhalokati', '9132598', 'Bangladesh-Awami-League', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('LutfaKhu210', 'MP-Candidate', 'West Kazir Par,Khulna', '9087123', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('MamunBar663', 'Member-of-Parliament (MP)', 'West Kazir Par,Barisal', '9012654', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('MamunDha214', 'Member-of-Parliament (MP)', 'West Kazir Par,Dhaka', '9087123', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('MamunKhu621', 'Member-of-Parliament (MP)', 'West Kazir Par,Khulna', '9132598', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('Md. SBag472', 'MP-Candidate', 'West Kazir Par,Bagerhat', '9023654', 'Bangladesh-Nationalist-Party[BNP]', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('Md. SBag547', 'Minister', 'West Mirpur,Bagerhat', '9023654', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('Md. SBar1', 'MP-Candidate', 'West Kazir Par,Barisal', '9023654', 'Bangladesh-Awami-League', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('Md. SBar779', 'Minister', 'West Kamalapur,Barisal', '9012654', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('Md. SDha687', 'MP-Candidate', 'West Kazir Par,Dhaka', '9023654', 'Bangladesh-Nationalist-Party[BNP]', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('Md. SDha885', 'Minister', 'West Kamalapur, Dhaka', '9012654', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('Md. SFar414', 'MP-Candidate', 'West Kazir Par,Faridpur', '9023654', 'Bangladesh-Nationalist-Party[BNP]', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('Md. SFar545', 'Minister', 'West Raja Bazar, Faridpur', '9123659', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('Md. SJha731', 'Member-of-Parliament (MP)', 'West Mirpur, Jhalokati', '9135624', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('Md. SJha732', 'Former-Member-Of-Parliament', 'West Kazir Par,Jhalokati', '9165234', 'Bangladesh-Nationalist-Party[BNP]', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it. ', 0),
('Md. SKhu101', 'Minister', 'West Raja Bazar, Faridpur', '9132598', 'Bangladesh-Awami-League', 'Joined Awami League in 1996. Back then, His father used to organize all the political activities.But after his father''s death in 1993, he decided to join the party.', 0),
('Md. SKhu572', 'MP-Candidate', 'West Kazir Par, Khulna', '9023654', 'Bangladesh-Nationalist-Party[BNP]', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('ObaidBar459', 'Former-Member-Of-Parliament', 'West Mirpur,Barisal', '9132598', 'Bangladesh-Nationalist-Party[BNP]', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('ObaidDha586', 'Former-Member-Of-Parliament', 'West Raja Bazar, Dhaka', '9123658', 'Bangladesh-Nationalist-Party[BNP]', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('ObaidKhu567', 'Former-Member-Of-Parliament', 'West Mirpur,Khulna', '9132598', 'Bangladesh-Nationalist-Party[BNP]', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('RifatBag770', 'MP-Candidate', 'West Kazir Par,Bagerhat', '9023654', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('RifatBar433', 'MP-Candidate', 'West Mirpur,Barisal', '9023654', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('RifatDha204', 'MP-Candidate', 'West Mirpur, Dhaka', '9023654', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('RifatFar49', 'MP-Candidate', 'West Kazir Par,Faridpur', '9132598', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('RifatJha82', 'MP-Candidate', 'West Kamalapur,Jhalokati', '9123658', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('RifatKhu240', 'MP-Candidate', 'West Kazir Par,Faridpur', '9023654', 'Jamaat-e-Islami-Bangladesh', 'He was made Chief or Amir of Barisal zone during the regime of BNP in 2001. Since then he was actively doing politics in the banner of JAMAT-E-ISLAM', 0),
('SheikBar514', 'Former-Minister', 'West Kamalapur,Barisal', '9132598', 'Bangladesh-Nationalist-Party[BNP]', ' He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('SheikDha698', 'Former-Minister', 'West Kamalapur,Dhaka', '9132598', 'Bangladesh-Nationalist-Party[BNP]', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('SheikJha645', 'Former-Minister', 'West Kamalapur,Jhalokati', '9325647', 'Bangladesh-Nationalist-Party[BNP]', ' He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0),
('SheikKhu58', 'Former-Minister', 'West Raja Bazar, Khulna', '9087123', 'Bangladesh-Nationalist-Party[BNP]', 'He basically started doing politics in the era of Hussain Muhammed Ershad''s regime. As there were autocratic government ruling BD at that time, so he had a lot of influence in organizing political activites for defeating it.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `leader_name`
--

CREATE TABLE IF NOT EXISTS `leader_name` (
  `leader_id` varchar(30) NOT NULL,
  `name` varchar(40) NOT NULL,
  `seat_id` varchar(40) NOT NULL,
  PRIMARY KEY (`leader_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leader_name`
--

INSERT INTO `leader_name` (`leader_id`, `name`, `seat_id`) VALUES
('AbdunBar228', 'Abdun Nur Tushar', 'Baris3'),
('AbdunDha581', 'Abdun Nur Tushar', 'Dhaka3'),
('AbdunJha550', 'Abdun Nur Tushar', 'Jhalo3'),
('AbdunKhu695', 'Abdun Nur Tushar', 'Khuln4'),
('AbrarBag960', 'Abrar Ahsan Kabir', 'Bager1'),
('AbrarBar984', 'Abrar Ahsan Kabir', 'Baris2'),
('AbrarDha870', 'Abrar Ahsan Kabir', 'Dhaka2'),
('AbrarFar971', 'Abrar Ahsan Kabir', 'Farid2'),
('AbrarJha929', 'Abrar Ahsan Kabir', 'Jhalo2'),
('AbrarKhu407', 'Abrar Ahsan Kabir', 'Khuln2'),
('AtishBag513', 'Atish Sarker', 'Bager1'),
('AtishBar258', 'Atish Sarker', 'Baris2'),
('AtishDha980', 'Atish Sarker', 'Dhaka2'),
('AtishFar92', 'Atish Sarker', 'Farid2'),
('AtishJha565', 'Atish Sarker', 'Jhalo2'),
('AtishKhu390', 'Atish Sarker', 'Khuln2'),
('IrfanBag824', 'Irfan Ibn Hossain', 'Bager2'),
('IrfanBar924', 'Irfan Ibn Hossain', 'Baris1'),
('IrfanDha817', 'Irfan Ibn Hossain', 'Dhaka1'),
('IrfanFar343', 'Irfan Ibn Hossain', 'Farid1'),
('IrfanJha611', 'Irfan Ibn Hossain', 'Jhalo1'),
('IrfanKhu354', 'Irfan Ibn Hossain Personal Info', 'Khuln1'),
('KayesBar413', 'Kayes Ahmed Khan', 'Baris4'),
('KayesDha830', 'Kayes Ahmed Khan', 'Dhaka4'),
('KayesKhu412', 'Kayes Ahmed Khan', 'Khuln3'),
('LutfaBar328', 'Lutfar Rahman', 'Baris3'),
('LutfaDha769', 'Lutfar Rahman', 'Dhaka3'),
('LutfaJha968', 'Lutfar Rahman ', 'Jhalo3'),
('LutfaKhu210', 'Lutfar Rahman', 'Khuln4'),
('MamunBar663', 'Mamun Molla', 'Baris4'),
('MamunDha214', 'Mamun Molla', 'Dhaka4'),
('MamunKhu621', 'Mamun Molla', 'Khuln3'),
('Md. SBag472', 'Md. Shakilur Rahman', 'Bager2'),
('Md. SBag547', 'Md. Solaiman Chowdhury', 'Bager2'),
('Md. SBar1', 'Md. Shakilur Rahman', 'Baris1'),
('Md. SBar779', 'Md. Solaiman Chowdhury', 'Baris1'),
('Md. SDha687', 'Md. Shakilur Rahman', 'Dhaka1'),
('Md. SDha885', 'Md. Solaiman Chowdhury', 'Dhaka1'),
('Md. SFar414', 'Md. Shakilur Rahman', 'Farid1'),
('Md. SFar545', 'Md. Solaiman Chowdhury', 'Farid1'),
('Md. SJha731', 'Md. Solaiman Chowdhury', 'Jhalo1'),
('Md. SJha732', 'Md. Shakilur Rahman', 'Jhalo1'),
('Md. SKhu101', 'Md. Solaiman Chowdhury', 'Khuln1'),
('Md. SKhu572', 'Md. Shakilur Rahman', 'Khuln1'),
('ObaidBar459', 'Obaidul Islam', 'Baris4'),
('ObaidDha586', 'Obaidul Islam', 'Dhaka4'),
('ObaidKhu567', 'Obaidul Islam', 'Khuln3'),
('RifatBag770', 'Rifat Mahmud', 'Bager1'),
('RifatBar433', 'Rifat Mahmud', 'Baris2'),
('RifatDha204', 'Rifat Mahmud', 'Dhaka2'),
('RifatFar49', 'Rifat Mahmud', 'Farid2'),
('RifatJha82', 'Rifat Mahmud', 'Jhalo2'),
('RifatKhu240', 'Rifat Mahmud', 'Khuln2'),
('SheikBar514', 'Sheikh Faisal Basher', 'Baris3'),
('SheikDha698', 'Sheikh Faisal Basher', 'Dhaka3'),
('SheikJha645', 'Sheikh Faisal Basher', 'Jhalo3'),
('SheikKhu58', 'Sheikh Faisal Basher', 'Khuln4');

-- --------------------------------------------------------

--
-- Table structure for table `mp_candidate_yr`
--

CREATE TABLE IF NOT EXISTS `mp_candidate_yr` (
  `year` varchar(100) NOT NULL,
  `leader_id_mp_can` varchar(30) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `mp_candidate_yr`
--

INSERT INTO `mp_candidate_yr` (`year`, `leader_id_mp_can`, `id`) VALUES
('2001,2008', 'IrfanJha611', 1),
('2001,2008', 'RifatJha82', 2),
('2008', 'LutfaJha968', 3),
('2001,2008', 'Md. SBar1', 4),
('2008', 'IrfanBar924', 5),
('2001,2008', 'RifatBar433', 6),
('2001,2008', 'LutfaBar328', 7),
('2001,2008', 'KayesBar413', 8),
('2001, 2008', 'Md. SDha687', 9),
('2008', 'IrfanDha817', 10),
('2001, 2008', 'RifatDha204', 11),
('2001, 2008', 'LutfaDha769', 12),
('2001, 2008', 'KayesDha830', 13),
('2001, 2008', 'Md. SFar414', 14),
('2008', 'IrfanFar343', 15),
('2001, 2008', 'RifatFar49', 16),
('2001, 2008', 'Md. SKhu572', 17),
('2008', 'IrfanKhu354', 18),
('2001, 2008', 'RifatKhu240', 19),
('2001, 2008', 'KayesKhu412', 20),
('2001, 2008', 'LutfaKhu210', 21),
('2001, 2008', 'RifatBag770', 22),
('2001, 2008', 'Md. SBag472', 23),
('2008', 'IrfanBag824', 24);

-- --------------------------------------------------------

--
-- Table structure for table `project_details`
--

CREATE TABLE IF NOT EXISTS `project_details` (
  `p_id` varchar(100) NOT NULL,
  `name` varchar(70) NOT NULL,
  `contractor` varchar(70) NOT NULL,
  `sdate` date NOT NULL,
  `edate` date NOT NULL,
  `type` varchar(20) NOT NULL,
  `location` varchar(50) NOT NULL,
  `description` longtext NOT NULL,
  `budget` int(11) NOT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_details`
--

INSERT INTO `project_details` (`p_id`, `name`, `contractor`, `sdate`, `edate`, `type`, `location`, `description`, `budget`) VALUES
('DhaMd_Bha924', 'Dhamrai Bridge', 'Md. Musa Sajid', '2014-01-01', '2017-02-02', 'Ongoing', 'BharaDhaka1d14627987', 'Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  Some Descriptive Text  ', 200000000),
('DhaMojJad960', 'Dhamrai High School', 'Mojammel Hossain', '2015-01-01', '2016-02-04', 'Completed', 'JadabDhaka1d14627567', 'Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description Some Description ', 5000000),
('KusMojKus838', 'Kushumhati Health Complex', 'Mojammel Hossain', '2015-01-01', '2018-01-01', 'Ongoing', 'KushuDhaka2d14519581', 'SOme text SOme text SOme text SOme text SOme text SOme text SOme text SOme text SOme text SOme text SOme text SOme text SOme text SOme text SOme text SOme text SOme text SOme text SOme text SOme text ', 30000000);

-- --------------------------------------------------------

--
-- Table structure for table `project_gallery`
--

CREATE TABLE IF NOT EXISTS `project_gallery` (
  `p_id` varchar(100) NOT NULL,
  `prog_id` varchar(100) NOT NULL,
  `img_link` varchar(255) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `project_gallery`
--

INSERT INTO `project_gallery` (`p_id`, `prog_id`, `img_link`, `id`) VALUES
('DhaMd_Bha924', '2014-01-012017-02-022072', './uploads/Dhamrai Bridge845/02_Padma-Bridge_Construction_020115_0024.jpg', 1),
('DhaMd_Bha924', '2014-01-012017-02-022072', './uploads/Dhamrai Bridge845/AAEAAQAAAAAAAALKAAAAJDU4MWQ3MTQ0LWFjODktNDQwMy04YjhiLTZlM2ViNzY0NjFmNg.jpg', 2),
('DhaMd_Bha924', '2014-01-012017-02-022072', './uploads/Dhamrai Bridge845/Bay_bridge_construction.jpg', 3),
('DhaMd_Bha924', '2014-01-012017-02-022072', './uploads/Dhamrai Bridge845/P1130914-copy-288x175.jpg', 4),
('DhaMojJad960', '2015-01-012016-02-049347', './uploads/Dhamrai High School583/banner-6.jpg', 5),
('DhaMojJad960', '2015-01-012016-02-049347', './uploads/Dhamrai High School583/Savar_Adhar_Chandra_Model_High_School.jpg', 6),
('DhaMd_Bha924', '2014-07-012014-10-044246', './uploads/892/39_big.jpg', 7),
('DhaMd_Bha924', '2014-07-012014-10-044246', './uploads/892/42_big.jpg', 8),
('DhaMd_Bha924', '2014-07-012014-10-044246', './uploads/892/44262857.jpg', 9),
('KusMojKus838', '2015-01-012018-01-018813', './uploads/Kushumhati Health Complex743/bera@uhfpo_dghs_gov_bd.jpg', 10),
('KusMojKus838', '2015-01-012018-01-018813', './uploads/Kushumhati Health Complex743/hosainpur@uhfpo_dghs_gov_bd.jpg', 11),
('KusMojKus838', '2015-01-012018-01-018813', './uploads/Kushumhati Health Complex743/putia@uhfpo_dghs_gov_bd.jpg', 12);

-- --------------------------------------------------------

--
-- Table structure for table `project_progress`
--

CREATE TABLE IF NOT EXISTS `project_progress` (
  `start` date NOT NULL,
  `end` date NOT NULL,
  `prog_id` varchar(100) NOT NULL,
  `p_id` varchar(100) NOT NULL,
  `progress_percentage` int(3) NOT NULL,
  PRIMARY KEY (`prog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_progress`
--

INSERT INTO `project_progress` (`start`, `end`, `prog_id`, `p_id`, `progress_percentage`) VALUES
('2014-03-04', '2014-06-04', '2014-01-012017-02-022072', 'DhaMd_Bha924', 80),
('2014-07-01', '2014-10-04', '2014-07-012014-10-044246', 'DhaMd_Bha924', 90),
('2015-12-01', '2006-02-04', '2015-01-012016-02-049347', 'DhaMojJad960', 100),
('2015-01-01', '2015-04-05', '2015-01-012018-01-018813', 'KusMojKus838', 40);

-- --------------------------------------------------------

--
-- Table structure for table `unions`
--

CREATE TABLE IF NOT EXISTS `unions` (
  `name` varchar(50) NOT NULL,
  `up_id` varchar(80) NOT NULL,
  `un_id` varchar(90) NOT NULL,
  PRIMARY KEY (`un_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unions`
--

INSERT INTO `unions` (`name`, `up_id`, `un_id`) VALUES
('Alfadanga', 'Farid1d16947', 'AlfadFarid1d16947251'),
('Bana', 'Farid1d16947', 'BanaFarid1d16947227'),
('Bhararia_', 'Dhaka1d14627', 'BharaDhaka1d14627987'),
('Bilaspur', 'Dhaka2d14519', 'BilasDhaka2d14519651'),
('Buraich', 'Farid1d16947', 'BuraiFarid1d16947829'),
('Chandpur', 'Farid1d16187', 'ChandFarid1d16187443'),
('Chatul', 'Farid1d16187', 'ChatuFarid1d16187297'),
('Dadpur', 'Farid1d16187', 'DadpuFarid1d16187531'),
('Jadabpur_', 'Dhaka1d14627', 'JadabDhaka1d14627567'),
('Kushumhati', 'Dhaka2d14519', 'KushuDhaka2d14519581'),
('Mahmudpur', 'Dhaka2d14519', 'MahmuDhaka2d14519725'),
('Sanora', 'Dhaka1d14627', 'SanorDhaka1d14627979');

-- --------------------------------------------------------

--
-- Table structure for table `upazilla`
--

CREATE TABLE IF NOT EXISTS `upazilla` (
  `name` varchar(40) NOT NULL,
  `up_id` varchar(60) NOT NULL,
  `seat_id` varchar(50) NOT NULL,
  `dis_id` varchar(60) NOT NULL,
  PRIMARY KEY (`up_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `upazilla`
--

INSERT INTO `upazilla` (`name`, `up_id`, `seat_id`, `dis_id`) VALUES
('Rampal', 'Bager1d1210', 'Bager1', 'd1'),
('Kachua_', 'Bager1d1556', 'Bager1', 'd1'),
('Mollahat', 'Bager1d1815', 'Bager1', 'd1'),
('Bagerhat_Sadar', 'Bager2d1186', 'Bager2', 'd1'),
('Chitalmari', 'Bager2d1683', 'Bager2', 'd1'),
('AliKadam', 'Banda1d2182', 'Banda1', 'd2'),
('Bandarban_Sadar', 'Banda1d244', 'Banda1', 'd2'),
('Lama', 'Banda1d2984', 'Banda1', 'd2'),
('Ruma', 'Banda2d2460', 'Banda2', 'd2'),
('Rowangchhari', 'Banda2d2729', 'Banda2', 'd2'),
('Barisha_Sadar', 'Baris1d4127', 'Baris1', 'd4'),
('Agailjhara', 'Baris1d4382', 'Baris1', 'd4'),
('Bakerganj', 'Baris2d4447', 'Baris2', 'd4'),
('Wazirpur', 'Baris2d4885', 'Baris2', 'd4'),
('Hizla', 'Baris3d4154', 'Baris3', 'd4'),
('Gaurnadi_Upazila', 'Baris4d4907', 'Baris4', 'd4'),
('Chandanaish', 'Chitt1d10656', 'Chitt1', 'd10'),
('Anwara', 'Chitt1d10834', 'Chitt1', 'd10'),
('Banshkhali', 'Chitt2d10203', 'Chitt2', 'd10'),
('Raozan', 'Chitt2d10299', 'Chitt2', 'd10'),
('Boalkhali', 'Chitt3d10606', 'Chitt3', 'd10'),
('Sandwip', 'Chitt4d10581', 'Chitt4', 'd10'),
('Nawabganj', 'Dhaka1d1441', 'Dhaka1', 'd14'),
('Dhamrai', 'Dhaka1d14627', 'Dhaka1', 'd14'),
('Dohar', 'Dhaka2d14519', 'Dhaka2', 'd14'),
('Keraniganj', 'Dhaka3d1418', 'Dhaka3', 'd14'),
('Savar', 'Dhaka4d14304', 'Dhaka4', 'd14'),
('Ghoraghat', 'Dinaj1d15326', 'Dinaj1', 'd15'),
('Birol', 'Dinaj1d1533', 'Dinaj1', 'd15'),
('Dinajpur_Sadar', 'Dinaj2d15722', 'Dinaj2', 'd15'),
('Chirirbandar', 'Dinaj2d15898', 'Dinaj2', 'd15'),
('Boalmari', 'Farid1d16187', 'Farid1', 'd16'),
('Alfadanga', 'Farid1d16947', 'Farid1', 'd16'),
('Saltha', 'Farid2d16328', 'Farid2', 'd16'),
('Faridpur_Sadar', 'Farid2d16513', 'Farid2', 'd16'),
('Habiganj_Sadar', 'Habig1d21580', 'Habig1', 'd21'),
('Azmiriganj', 'Habig1d21948', 'Habig1', 'd21'),
('Baniachong', 'Habig2d2193', 'Habig2', 'd21'),
('Nabiganj', 'Habig3d21507', 'Habig3', 'd21'),
('Dewanganj', 'Jamal1d22222', 'Jamal1', 'd22'),
('Islampur', 'Jamal1d22448', 'Jamal1', 'd22'),
('Baksiganj_', 'Jamal2d22933', 'Jamal2', 'd22'),
('Jamalpur_Sadar', 'Jamal3d22266', 'Jamal3', 'd22'),
('Madarganj', 'Jamal3d22512', 'Jamal3', 'd22'),
('Kathalia', 'Jhalo1d24276', 'Jhalo1', 'd24'),
('Jhalokati_Sadar', 'Jhalo1d2440', 'Jhalo1', 'd24'),
('Nalchity', 'Jhalo2d24604', 'Jhalo2', 'd24'),
('Rajapur', 'Jhalo3d24702', 'Jhalo3', 'd24'),
('Batiaghata', 'Khuln1d2816', 'Khuln1', 'd28'),
('Shekhpara', 'Khuln1d28706', 'Khuln1', 'd28'),
('Dacope', 'Khuln2d28821', 'Khuln2', 'd28'),
('Sonadanga', 'Khuln3d28101', 'Khuln3', 'd28'),
('Daulatpur', 'Khuln4d28961', 'Khuln4', 'd28'),
('Madaripur_Sadar', 'Madar1d34516', 'Madar1', 'd34'),
('Trishal	', 'Mymen1d40313', 'Mymen1', 'd40'),
('Gaffargaon', 'Mymen1d40850', 'Mymen1', 'd40'),
('Dhobaura', 'Mymen2d40230', 'Mymen2', 'd40'),
('Gauripur', 'Mymen2d4035', 'Mymen2', 'd40'),
('Fulbaria', 'Mymen3d40490', 'Mymen3', 'd40'),
('Charghat', 'Rajsh1d54740', 'Rajsh1', 'd54'),
('Bagmara', 'Rajsh1d54792', 'Rajsh1', 'd54'),
('Bagha', 'Rajsh2d5473', 'Rajsh2', 'd54'),
('Durgapur', 'Rajsh3d54508', 'Rajsh3', 'd54'),
('Godagari', 'Rajsh4d54825', 'Rajsh4', 'd54'),
('Pirgacha', 'Rangp1d56313', 'Rangp1', 'd56'),
('Mithapukur', 'Rangp1d5681', 'Rangp1', 'd56'),
('Kaunia', 'Rangp2d56233', 'Rangp2', 'd56'),
('Rangpur_Sadar', 'Rangp2d56998', 'Rangp2', 'd56'),
('Companiganj', 'Sylhe1d62279', 'Sylhe1', 'd62'),
('Sylhet_Sadar', 'Sylhe1d62354', 'Sylhe1', 'd62'),
('Kanaighat', 'Sylhe2d62425', 'Sylhe2', 'd62'),
('Dakshin_Surma', 'Sylhe2d62512', 'Sylhe2', 'd62');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
