<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Division extends CI_Controller {

		public function __construct()
        {
                parent::__construct();
                $this->load->model('Division_model');
       //          $this->load->helper('url_helper');
       //          $this->load->helper('form');
    			// $this->load->library('form_validation');

        }

		public function index()
		{
			
			$data['dhaka'] = $this->Division_model->get_district('Dhaka');
			$data['barisal'] = $this->Division_model->get_district('Barisal');
			$data['chittagong'] = $this->Division_model->get_district('Chittagong');
			$data['sylhet'] = $this->Division_model->get_district('Sylhet');
			$data['khulna'] = $this->Division_model->get_district('Khulna');
			$data['mymensingh'] = $this->Division_model->get_district('Mymensingh');
			$data['rajshahi'] = $this->Division_model->get_district('Rajshahi');
			$data['rangpur'] = $this->Division_model->get_district('Rangpur');

			$this->load->view('pages/divisionlist',$data);
			

		}

        public function district($name)
		{
	        if ( ! file_exists(APPPATH.'views/pages/'.$name.'.php'))
	        {
	                show_404();
	        }
	        else if($data['error']=$this->Division_model->get_error($this->uri->segment(2,0)))
	        {
	        	
	        $data['district'] = $this->Division_model->get_district_not($this->uri->segment(1,0),
	        					$this->uri->segment(2,0));
	       	$data['division']= $this->Division_model->get_division_not($this->uri->segment(1,0));

	       	$data['election_seat']=$this->Division_model->get_es($this->uri->segment(2,0));
	       	$data['election_seat_skeleton']=$this->Division_model->get_es_skeleton($this->uri->segment(2,0));

	       	$data['administration']=$this->Division_model->get_administration($this->uri->segment(2,0));

	       	$data['leader']=$this->Division_model->get_leaders($this->uri->segment(2,0));
	       	$data['former']=$this->Division_model-> get_former_position();

	       	$data['upazilla']=$this->Division_model->get_upazilla($this->uri->segment(2,0));

	       	$data['budget']=$this->Division_model->get_budget($this->uri->segment(2,0));

	       	$data['ongoing']=$this->Division_model->get_ongoing_dis($this->uri->segment(2,0));
	       	$data['upcoming']=$this->Division_model->get_upcoming_dis($this->uri->segment(2,0));
	       	$data['completed']=$this->Division_model->get_completed_dis($this->uri->segment(2,0));


	       	
	       	$this->load->view('pages/'.$name,$data);
	        }
	        else{
	      		show_404();
	        }
	      
		}
		public function upazilla ($name){
			 if ( ! file_exists(APPPATH.'views/pages/'.$name.'.php'))
	        {
	                show_404();
	        }
	        else if($data['error']=$this->Division_model->get_error($this->uri->segment(2,0)))
	        {
	        	
	        $data['district'] = $this->Division_model->get_district_not($this->uri->segment(1,0),
	        					$this->uri->segment(2,0));
	       	$data['division']= $this->Division_model->get_division_not($this->uri->segment(1,0));

	       	$data['upazilla_list']= $this->Division_model->get_upazilla_not($this->uri->segment(2,0), $this->uri->segment(4,0));

	       	$data['upazilla_dafault']= $this->Division_model->get_upazilla_default($this->uri->segment(2,0));

	       	
	       	$data['administration']= $this->Division_model->get_up_administration($this->uri->segment(4,0));

	       	$data['up_er']= $this->Division_model->get_up_er($this->uri->segment(4,0));

	       	$data['union']= $this->Division_model->get_un($this->uri->segment(4,0));

	       	$data['ongoing']= $this->Division_model->get_ongoing($this->uri->segment(4,0));
	       	$data['upcoming']= $this->Division_model->get_upcoming($this->uri->segment(4,0));
	       	$data['completed']= $this->Division_model->get_completed($this->uri->segment(4,0));

	       	$data['ch_project']= $this->Division_model->get_ch_project($this->uri->segment(4,0));




	       	$this->load->view('pages/'.$name,$data);
	        }
	        else{
	      		show_404();
	        }
			// $this->load->view('pages/'.$name);
		}

	public function projects ($name){
			 if ( ! file_exists(APPPATH.'views/pages/'.$name.'.php'))
	        {
	                show_404();
	        }
	        $data['district'] = $this->Division_model->get_district_not($this->uri->segment(1,0),
	        					$this->uri->segment(2,0));
	        $data['upazilla']=$this->Division_model->get_upazilla($this->uri->segment(2,0));
	        $data['project_list']=$this->Division_model->get_project_list_for_project_page($this->uri->segment(2,0), $this->uri->segment(4, 0));
	        $data['project_nav']=$this->Division_model->get_project_list_for_project_nav($this->uri->segment(2,0), $this->uri->segment(4, 0));
	        $data['project_progress']=$this->Division_model->get_project_progress($this->uri->segment(5, 0));
	        $data['project_img']=$this->Division_model->get_project_img($this->uri->segment(5, 0));
	        $data['project_details']=$this->Division_model->get_project_details($this->uri->segment(5, 0));
	        $data['project_comment']=$this->Division_model->get_project_comment($this->uri->segment(5, 0));
	        $data['recent_comment']=$this->Division_model->get_recent_comment($this->uri->segment(5, 0));

	       	$this->load->view('pages/'.$name,$data);
		}	

	
}