<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{

	public function __construct()
        {
                parent::__construct();
                $this->load->model('Admin_model');
 
       	}

    public function index()
  		{		if($this->session->userdata('logged_in'))
  				{
  					 $session_data = $this->session->userdata('logged_in');
				     $data['username'] = $session_data['username'];
				     $data['type']=$session_data['type'];

				     if(  $data['type']=='superadmin')
				     	$this->load->view('pages/superadmin',$data);

				     else if(  $data['type']=='district') {
				     	  $data['dis_id']=$this-> Admin_model-> get_district_id_for_districtadmin($session_data['username']);
				   		  $data['election_seat']=$this-> Admin_model-> get_election_seat_id_for_districtadmin($session_data['username']);
				   		  $this->load->view('pages/districtadmin',$data);
				     } 
				     	
				     
				     else if( $data['type'] =='upazilla'){
				     	$data['up_id']=$this-> Admin_model-> get_upazilla_id_for_upazillaadmin($session_data['username']);
				     	$data['ongoing_project']=$this-> Admin_model-> get_ongoing($session_data['username']);
				     	$this->load->view('pages/upazillaadmin',$data);
				     }
    					

  				}
  				else{
  					$this->load->view('pages/adminlogin');
  				}
        }

    public function login_validation(){

    			$this->form_validation->set_rules('username', 'Username', 'trim|required');
   				$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

    			if($this->form_validation->run()==FALSE)
    				$this->load->view('pages/adminlogin');
    			else{

    				if( $type = $this->input->post('type')=='superadmin')
    					redirect('/admin/superadmin');
    				else if( $type = $this->input->post('type')=='district')
    					redirect('/admin/districtadmin');
    				else if( $type = $this->input->post('type')=='upazilla')
    					redirect('/admin/upazillaadmin');
    			} 


   		}

		public function check_database($password)
		 {
		
		   $username = $this->input->post('username');
		   $type = $this->input->post('type');

		   $result = $this->Admin_model->login($username, $password, $type);
		 
		   if($result)
		   {
		     $sess_array = array();
		   
		       $sess_array = array(
		         'username' => $username,
		         'type'		=> $type
		       );
		       $this->session->set_userdata('logged_in', $sess_array);
		     
		     return TRUE;
		   }
		   else
		   {
		     $this->form_validation->set_message('check_database', 'Invalid username or password or admin type');
		     return false;
		   }
		 }

   	public function success($name){
   		if($this->session->userdata('logged_in'))
		   {
		     $session_data = $this->session->userdata('logged_in');
		     $data['username'] = $session_data['username'];
		     $data['type']=$session_data['type'];

		     $data['district_list']=$this-> Admin_model-> get_district_for_superadmin();
		     
		     $data['dis_id']=$this-> Admin_model-> get_district_id_for_districtadmin($session_data['username']);
		     $data['election_seat']=$this-> Admin_model-> get_election_seat_id_for_districtadmin($session_data['username']);

		     $data['up_id']=$this-> Admin_model-> get_upazilla_id_for_upazillaadmin($session_data['username']);
		     $data['union']=$this-> Admin_model-> get_union_for_upazillaadmin($session_data['username']);

		     $data['ongoing_project']=$this-> Admin_model-> get_ongoing($session_data['username']);
		     $this->load->view('pages/'.$name, $data);
		   }
		else
		   {
		     //If no session, redirect to login page
		     redirect('admin', 'refresh');
		   }
   		
   	}

	public function logout()
	 {
	   $this->session->unset_userdata('logged_in');
	   session_destroy();
	   redirect('admin', 'refresh');
	 }

	 public function updateDisPassword()
	 {
	   if($this->Admin_model->updateDisPasswordToDb()){

            redirect('/admin/districtadmin/passwordUpdateSuccess');
        }
        else 
            redirect('/admin/districtadmin/passwordUpdatedFailure');
	 }
}

?>