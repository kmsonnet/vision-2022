<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InsertData extends CI_Controller{

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Insert_model');  

    }

    public function index()
  	{		

    }
    public function insertDistrict()
    {

    	if($this->Insert_model->insertDisToDb($this->input->post())){

    		redirect('/admin/superadmin/success');
    	}

    	
    }

    public function insertEs()
    {

        if($this->Insert_model->insertEsToDb($this->input->post())){

            redirect('/admin/superadmin/success');
        }
        else 
            redirect('/admin/superadmin/failure');

        
    }

     public function insertSiteAdmin()
    {

        if($this->Insert_model->insertSiteAdminToDb()){

            redirect('/admin/superadmin/success');
        }
        else 
            redirect('/admin/superadmin/failure');

        
    }

      public function insertDisAdministrator()
    {

        if($this->Insert_model->insertDisAdminToDb()){

            redirect('/admin/districtadmin/successadmin');
        }
        else 
            redirect('/admin/districtadmin/failureadmin');

        
    }

       public function insertLeaders()
    {

        if($this->Insert_model->insertLeaders($this->input->post())){

            redirect('/admin/districtadmin/successleader');
        }
        else 
            redirect('/admin/districtadmin/failureleader');

        
    }

      public function insertUpazilla()
    {

        if($this->Insert_model->insertUpazillaToDb($this->input->post())){

            redirect('/admin/districtadmin/successupazilla');
        }
        else 
            redirect('/admin/districtadmin/failureupazilla');

        
    }

     public function insertUpAdministrator(){
         if($this->Insert_model->insertUpAdminToDb()){

            redirect('/admin/upazillaadmin/successadmin');
        }
        else 
            redirect('/admin/upazillaadmin/failureadmin');
     }
   

     public function insertEr(){
         if($this->Insert_model->insertErToDb()){

            redirect('/admin/upazillaadmin/successer');
        }
        else 
            redirect('/admin/upazillaadmin/failureer');
     }

    public function insertUnion(){
         if($this->Insert_model->insertUnionToDb($this->input->post())){

            redirect('/admin/upazillaadmin/successunion');
        }
        else 
            redirect('/admin/upazillaadmin/failureunion');
     }

     public function insertChairman(){
         if($this->Insert_model->insertChToDb()){

            redirect('/admin/upazillaadmin/successch');
        }
        else 
            redirect('/admin/upazillaadmin/failurech');
     }
     public function insertProject(){
         if($this->Insert_model->insertProjectToDb()){

            redirect('/admin/upazillaadmin/successproject');
        }
        else 
            redirect('/admin/upazillaadmin/failureproject');
     }

      public function insertProgress(){
         if($this->Insert_model->insertProgressToDb()){

            redirect('/admin/upazillaadmin/successprogress');
        }
        else 
            redirect('/admin/upazillaadmin/failureprogress');
     }
     public function insertComment(){
         $p=$this->Insert_model->insertCommentToDb();
         if($p!=NULL){

            redirect($p);
        }
        else 
            redirect('/admin/upazillaadmin/failureprogress');
     }

     public function insertBudget(){
         if($this->Insert_model->insertBudgetToDb()){

            redirect('/admin/superadmin/success');
        }
        else 
            redirect('/admin/superadmin/failure');
     }
   
}
?>