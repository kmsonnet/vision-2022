<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// Default page
$route['default_controller'] = 'division';

// District page
$route['Dhaka/(:any)'] = 'division/district/district';
$route['Barisal/(:any)'] = 'division/district/district';
$route['Chittagong/(:any)'] = 'division/district/district';
$route['Khulna/(:any)'] = 'division/district/district';
$route['Mymensingh/(:any)'] = 'division/district/district';
$route['Rajshahi/(:any)'] = 'division/district/district';
$route['Rangpur/(:any)'] = 'division/district/district';
$route['Sylhet/(:any)'] = 'division/district/district';

$route['(:any)/(:any)/upazilla/(:any)']='division/upazilla/upazilla';
$route['(:any)/(:any)/upazilla/default']='division/upazilla/upazilla';

$route['(:any)/(:any)/Projects/Ongoing/(:any)']='division/projects/projects';
$route['(:any)/(:any)/Projects/Upcoming/(:any)']='division/projects/projects';
$route['(:any)/(:any)/Projects/Completed/(:any)']='division/projects/projects';

$route['admin/adminlogin']='admin';
$route['admin/login_validation']='admin/login_validation';

$route['admin/superadmin']='admin/success/superadmin';
$route['admin/districtadmin']='admin/success/districtAdmin';
$route['admin/upazillaadmin']='admin/success/upazillaAdmin';

$route['admin/superadmin/addDistrict']='insertdata/insertDistrict';
$route['admin/superadmin/addEs']='insertdata/insertEs';
$route['admin/superadmin/addAdmin']='insertdata/insertSiteAdmin';
$route['admin/superadmin/addBudget']='insertdata/insertBudget';


$route['admin/districtadmin/addDisAdmin']='insertdata/insertDisAdministrator';
$route['admin/districtadmin/addLeaders']='insertdata/insertLeaders';
$route['admin/districtadmin/addUpazilla']='insertdata/insertUpazilla';
$route['admin/districtadmin/updatePassword']='admin/updateDisPassword';


$route['admin/upazillaadmin/addUpAdmin']='insertdata/insertUpAdministrator';
$route['admin/upazillaadmin/addEr']='insertdata/insertEr';
$route['admin/upazillaadmin/addUnion']='insertdata/insertUnion';
$route['admin/upazillaadmin/addCh']='insertdata/insertChairman';
$route['admin/upazillaadmin/addProject']='insertdata/insertProject';
$route['admin/upazillaadmin/addProgress']='insertdata/insertProgress';

$route['(:any)/(:any)/Projects/(:any)/(:any)/addComment']='insertdata/insertComment';


$route['admin/superadmin/success']='admin/success/superadmin';
$route['admin/districtadmin/successadmin']='admin/success/districtAdmin';
$route['admin/districtadmin/successleader']='admin/success/districtAdmin';
$route['admin/districtadmin/successupazilla']='admin/success/districtAdmin';
$route['admin/upazillaadmin/successadmin']='admin/success/upazillaadmin';
$route['admin/upazillaadmin/successer']='admin/success/upazillaadmin';
$route['admin/upazillaadmin/successunion']='admin/success/upazillaadmin';
$route['admin/upazillaadmin/successch']='admin/success/upazillaadmin';
$route['admin/upazillaadmin/successproject']='admin/success/upazillaadmin';
$route['admin/upazillaadmin/successprogress']='admin/success/upazillaadmin';
$route['admin/districtadmin/passwordUpdateSuccess']='admin/success/districtadmin';


$route['admin/superadmin/failure']='admin/success/superadmin';
$route['admin/districtadmin/failureadmin']='admin/success/districtAdmin';
$route['admin/districtadmin/failureleader']='admin/success/districtAdmin';
$route['admin/upazillaadmin/failureadmin']='admin/success/upazillaadmin';
$route['admin/upazillaadmin/failureer']='admin/success/upazillaadmin';
$route['admin/upazillaadmin/failureunion']='admin/success/upazillaadmin';
$route['admin/upazillaadmin/failurech']='admin/success/upazillaadmin';
$route['admin/upazillaadmin/failureproject']='admin/success/upazillaadmin';
$route['admin/upazillaadmin/failureprogress']='admin/success/upazillaadmin';
$route['admin/districtadmin/passwordUpdatedFailure']='admin/success/districtadmin';


$route['admin/logout']='admin/logout';
$route['admin/construct']='admin/success/construct';
$route['admin/construct/addLeaders']='insertdata/insertLeaders';


$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;
