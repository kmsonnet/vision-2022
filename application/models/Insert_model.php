<?php
class Insert_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

        public function insertDisToDb($postdata){
        	$data =array();
        	extract($postdata);
        	$count = count($dis);

        	for($i=0; $i<$count; $i++) {
    			$data[$i]=array(
              'dis_id'=>$dis[$i].rand(0,100),
    					'name'=>$dis[$i],
              'division_name'=>$division,
    					
    			);

        	}
        	if($this->db->insert_batch('district', $data))
        		return true;
        }

        public function insertEsToDb($postdata){
            $data =array();
            extract($postdata);
            $count = count($es);

            for($i=0; $i<$count; $i++) {
                $data[$i]=array(

                        'name'=>$es[$i],
                        'dis_id'=>$district,
                        'seat_id'=>substr($es[($count-1)-$i], 0, 5).($count-$i), 
                );

            }
            if($this->db->insert_batch('election_seats', $data))
                return true;
        }

        public function insertSiteAdminToDb(){
                $admin_id=$this->input->post('adminId');
                $password =$this->input->post('defaultPass');
                $type=$this->input->post('adminType');
                if($type=='district')
                  $get="select name from district";
                else if($type=='upazilla')
                   $get="select name from upazilla";
              
              $query =  $this->db->query($get);
              foreach ($query->result_array() as $dis):
                    $name=$dis['name'];
                    $q="INSERT INTO Admin(admin_id,username, password,type) VALUES('$admin_id','$name', '$password', '$type')";
                   $this->db->query($q);
              endforeach;
              return true;
        }

        public function insertDisAdminToDb(){
            $dis_id= $this->input->post('adminDisId');
            $data_name=array(
                    array(
                         'name'=>$this->input->post('dcname'),
                         'dis_id_ad'=> $dis_id, 
                         'adminis_id'=>$dc_id=substr($this->input->post('dcname'), 0, 5).$dis_id.rand(10,1000),
                        ),
                     array(
                         'name'=>$this->input->post('adcname'),
                          'dis_id_ad'=> $dis_id, 
                         'adminis_id'=>$adc_id=substr($this->input->post('adcname'), 0, 5).$dis_id.rand(10,1000),
                        ),
                    array(
                         'name'=>$this->input->post('spname'),
                          'dis_id_ad'=> $dis_id, 
                         'adminis_id'=>$sp_id=substr($this->input->post('spname'), 0, 5).$dis_id.rand(10,1000),
                        ),
                    array(
                         'name'=>$this->input->post('aspname'),
                          'dis_id_ad'=> $dis_id, 
                         'adminis_id'=>$asp_id=substr($this->input->post('aspname'), 0, 5).$dis_id.rand(10,1000),
                        ),

                );
            $data_adminis_info=array(
                        array(
                            'adminis_id_info'=>$dc_id,
                            'position'=>$this->input->post('dcposition'),
                            'address'=>$this->input->post('dcaddress'),
                            'contact'=>$this->input->post('dccontact'),
                            'join_date'=>$this->input->post('dcjoin'),
                            'priority'=>1,
                         ),
                        array(
                            'adminis_id_info'=>$adc_id,
                            'position'=>$this->input->post('adcposition'),
                            'address'=>$this->input->post('adcaddress'),
                            'contact'=>$this->input->post('adccontact'),
                            'join_date'=>$this->input->post('adcjoin'),
                            'priority'=>2,
                         ),
                        array(
                            'adminis_id_info'=>$sp_id,
                            'position'=>$this->input->post('spposition'),
                            'address'=>$this->input->post('spaddress'),
                            'contact'=>$this->input->post('spcontact'),
                            'join_date'=>$this->input->post('spjoin'),
                            'priority'=>3,
                         ),
                        array(
                            'adminis_id_info'=>$asp_id,
                            'position'=>$this->input->post('aspposition'),
                            'address'=>$this->input->post('aspaddress'),
                            'contact'=>$this->input->post('aspcontact'),
                            'join_date'=>$this->input->post('aspjoin'),
                            'priority'=>4,
                         ),
                      
                );

             if($this->db->insert_batch('administrators_name', $data_name) && $this->db->insert_batch('administrators_info', $data_adminis_info))
                return true;

        }

        public function insertLeaders($postdata){
          $name=$this->input->post('lname');
          $seat_id=$this->input->post('seatid');
          $data_name=array(
              'leader_id'=>$leader_id=substr($name, 0, 5).substr($seat_id, 0, 3).rand(1, 1000),
              'name'=>$name,
              'seat_id'=>$seat_id,
            );

          $data_leader_info=array(
            'leader_id_info'=>$leader_id,
            'current_position'=>$this->input->post('crnt_pos'),
            'address'=>$this->input->post('laddress'),
            'contact'=>$this->input->post('lcontact'),
            'party'=>$this->input->post('party'),
            'bio'=>$this->input->post('bio'),
            );

          if($this->input->post('mpcanyr')){
            $data_mp_can_yr=array(
              'leader_id_mp_can'=>$leader_id,
              'year'=>$this->input->post('mpcanyr'),
            );
            $this->db->insert('mp_candidate_yr', $data_mp_can_yr);
          }

          $data_former =array();
          extract($postdata);
          $count = count($posSelect);
          if($count>0){

              for($i=0; $i<$count; $i++) {
              $data_former[$i]=array(
                  'leader_id_fpos'=>$leader_id,
                  'fposition'=>$posSelect[$i],
                  'start'=>$fstart[$i],
                  'end'=>$fend[$i],
                );
            }
             $this->db->insert_batch('leader_former_position',$data_former);
          }

          if($this->db->insert('leader_name', $data_name) && $this->db->insert('leader_info', $data_leader_info))
            return true;
        }

        public function insertUpazillaToDb($postdata){
            $data =array();
            extract($postdata);
            $district=$this->input->post('upDisId');
            $count = count($upname);
            for($i=0; $i<$count; $i++) {
                $data[$i]=array(

                        'name'=>str_replace(" ","_",$upname[$i]),
                        'up_id'=>$upes[$i].$district.rand(10,1000),
                        'seat_id'=>$upes[$i], 
                        'dis_id'=>$district,
                      
                );

            }
            if($this->db->insert_batch('upazilla', $data))
                return true;
        }

         public function insertUpAdminToDb(){
            $u_id= $this->input->post('adminUpId');
            $data_name=array(
                         'name'=>$this->input->post('unoname'),
                         'dis_id_ad'=> $u_id, 
                         'adminis_id'=>$uno_id=substr($this->input->post('unoname'), 0, 5).$u_id.rand(10,1000),
                      );

            $data_adminis_info=array(
                            'adminis_id_info'=>$uno_id,
                            'position'=>$this->input->post('unoposition'),
                            'address'=>$this->input->post('unoaddress'),
                            'contact'=>$this->input->post('unocontact'),
                            'join_date'=>$this->input->post('unojoin'),
                            'priority'=>1, 
                       );

             if($this->db->insert('administrators_name', $data_name) && $this->db->insert('administrators_info', $data_adminis_info))
                return true;

        }
        public function insertErToDb(){
          $pos=$this->input->post('er_pos');
          if($pos=='Upazilla-Chairman')
              $p=1;
          else if($pos=='Upazilla-Vice-Chairman')
              $p=2;
          else 
              $p=3;

           $data_er_info=array(
                            'name'=>$name=$this->input->post('er_name'),
                            'position'=>$pos,
                            'address'=>$this->input->post('er_address'),
                            'contact'=>$this->input->post('er_contact'),
                            'join_date'=>$this->input->post('er_join'),
                            'party'=>$this->input->post('er_party'),
                            'location_id'=>$lid=$this->input->post('er_UpId'),
                            'er_id'=>substr($name, 0 ,5).$lid.rand(10, 1000),
                            'priority'=>$p,

                       );
           if($this->db->insert('elective_representative', $data_er_info))
                return true;
        }

       public function insertUnionToDb($postdata){
            $data =array();
            extract($postdata);
            $up_id=$this->input->post('er_UpId');
            $count = count($unname);
            for($i=0; $i<$count; $i++) {
                $data[$i]=array(

                        'name'=>$name=str_replace(" ","_",$unname[$i]),
                        'up_id'=>$up_id,
                        'un_id'=>substr($name, 0, 5).$up_id.rand(10,1000),
                      
                );
            }
             if($this->db->insert_batch('unions', $data))
                return true;
       } 
        public function insertChToDb(){
             $data_er_info=array(

                            'name'=>$name=$this->input->post('ch_name'),
                            'position'=>$this->input->post('ch_pos'),
                            'address'=>$this->input->post('ch_address'),
                            'contact'=>$this->input->post('ch_contact'),
                            'join_date'=>$this->input->post('ch_join'),
                            'party'=>$this->input->post('ch_party'),
                            'location_id'=>$lid=$this->input->post('ch_union'),
                            'er_id'=>substr($name, 0 ,3).$lid.rand(10, 1000),
                            'priority'=>1,

                       );
           if($this->db->insert('elective_representative', $data_er_info))
                return true;
       } 

       public function insertProjectToDb(){
        $project_info=array(

              'name'=>$name=$this->input->post('pname'),
              'contractor'=>$cname=$this->input->post('pcname'),
              'sdate'=>$sdate=$this->input->post('sdate'),
              'edate'=>$edate=$this->input->post('edate'),
              'type'=>$type=$this->input->post('ptype'),
              'location'=>$loc=$this->input->post('plocation'),
              'description'=>$this->input->post('pdes'),
              'budget'=>$this->input->post('pbudget'),
              'p_id'=>$p_id=substr($name, 0 ,3).substr(str_replace(".","_",$cname), 0, 3).substr($loc, 0, 3).rand(10, 1000),
          );

        $isdate=$this->input->post('isdate');
        $iedate=$this->input->post('iedate');
        $project_progress=array(
              'start'=>$isdate,
              'end'=>$iedate,
              'prog_id'=>$prog_id=$sdate.$edate.rand(0,10000),
              'p_id'=>$p_id,
              'progress_percentage'=>$this->input->post('progress'),
          );

          $name_array = array();
          $rand=rand(0,1000);
          $count = count($_FILES['userfile']['size']);
          $path='./uploads/'.$name.$rand.'/';

          foreach($_FILES as $key=>$value)
             for($s=0; $s<=$count-1; $s++) {
                  $_FILES['userfile']['name']=$value['name'][$s];
                  $_FILES['userfile']['type']    = $value['type'][$s];
                  $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
                  $_FILES['userfile']['error']       = $value['error'][$s];
                  $_FILES['userfile']['size']    = $value['size'][$s]; 

                 
                  $config['upload_path'] = $path;
                  $config['allowed_types'] = 'gif|jpg|png|JPG|';
                      // $config['max_size']    = '10000';
                      // $config['max_width']  = '1024';
                      // $config['max_height']  = '768';
                  if (!is_dir($path)) {
                      mkdir('./uploads/' .$name.$rand.'/', TRUE);
                  }

                  $this->load->library('upload', $config);
                  $this->upload->do_upload();
                  $data = $this->upload->data();
                  $img_name= $data['file_name'];
                 
                  $project_img[$s] = array(
                               'img_link'=> $path.$img_name, 
                               'p_id'=>$p_id,
                               'prog_id'=>$prog_id,
                               );
              }
        
             
          if($this->db->insert_batch('project_gallery',$project_img) && $this->db->insert('project_details',$project_info)
            && $this->db->insert('project_progress',$project_progress))
            return true;
              

       }

       public function insertProgressToDb(){

        $p_id=$this->input->post('pid');
        $isdate=$this->input->post('isdate');
        $iedate=$this->input->post('iedate');
        $project_progress=array(
              'start'=>$isdate,
              'end'=>$iedate,
              'prog_id'=>$prog_id=$isdate.$iedate.rand(0,10000),
              'p_id'=>$p_id,
              'progress_percentage'=>$this->input->post('progress'),
          );

        
          $name_array = array();
          $rand=rand(0,1000);
          $count = count($_FILES['userfile']['size']);
          $path='./uploads/'.$name.$rand.'/';

          foreach($_FILES as $key=>$value)
             for($s=0; $s<=$count-1; $s++) {
                  $_FILES['userfile']['name']=$value['name'][$s];
                  $_FILES['userfile']['type']    = $value['type'][$s];
                  $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
                  $_FILES['userfile']['error']       = $value['error'][$s];
                  $_FILES['userfile']['size']    = $value['size'][$s]; 

                 
                  $config['upload_path'] = $path;
                  $config['allowed_types'] = 'gif|jpg|png|JPG|';
                      // $config['max_size']    = '10000';
                      // $config['max_width']  = '1024';
                      // $config['max_height']  = '768';
                  if (!is_dir($path)) {
                      mkdir('./uploads/' .$name.$rand.'/', TRUE);
                  }

                  $this->load->library('upload', $config);
                  $this->upload->do_upload();
                  $data = $this->upload->data();
                  $img_name= $data['file_name'];
                 
                  $project_img[$s] = array(
                               'img_link'=> $path.$img_name, 
                               'p_id'=>$p_id,
                               'prog_id'=>$prog_id,
                               );
              }
        
        if($this->db->insert_batch('project_gallery',$project_img) && $this->db->insert('project_progress',$project_progress)){
          return true;
        }
       }
    
    public function insertCommentToDb(){
      $purl=$this->input->post('purl');
      $data=array(
        'commentator'=>$commentator=$this->input->post('commentator'),
        'comment'=>$comment=$this->input->post('comment'),
        'p_id'=>$this->input->post('pid'),
        'cmt_id'=>substr($commentator, 0, 3).substr($comment, 0, 5).rand(0,1000),
      );
      if($this->db->insert('comments',$data))
        return $purl;
    }

     public function insertBudgetToDb(){
      $data=array(
        'amount'=>$this->input->post('amount'),
        'dis_id'=>$this->input->post('disid'),
        'year'=>$this->input->post('year'),
      );
      if($this->db->insert('budget',$data))
        return true;
    }

}
?>