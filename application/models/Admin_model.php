<?php
class Admin_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

		public function login($username, $password,$type)
		 {
		   $this -> db -> select('username, password, type');
		   $this -> db -> from('admin');
		   $this -> db -> where('username', $username);
		   $this -> db -> where('password', $password);
		   $this -> db -> where('type', $type);
		   $this -> db -> limit(1);
		 
		   $query = $this -> db -> get();
		 
		   if($query -> num_rows() == 1)
		   {
		     return $query->result();
		   }
		   else
		   {
		     return false;
		   }
		 }
	    public function get_district_for_superadmin(){
	        $get="select * from district order by name asc";
	        $query =  $this->db->query($get);
	        return $query->result_array();
	    }

	    public function get_district_id_for_districtadmin($d){
	    	$get="select dis_id from district where name='$d'";
	    	$query =  $this->db->query($get);
	        return $query->result_array();
		}

		 public function get_election_seat_id_for_districtadmin($d){
	    	$get="SELECT name, seat_id FROM election_seats WHERE dis_id=(SELECT dis_id from district WHERE name='$d') ORDER BY seat_id ASC";
	    	$query =  $this->db->query($get);
	        return $query->result_array();
		}

		public function get_upazilla_id_for_upazillaadmin($u){
	    	$get="select up_id from upazilla where name='$u'";
	    	$query =  $this->db->query($get);
	        return $query->result_array();
		}

		public function get_union_for_upazillaadmin($u){
	    	$get="SELECT * 
	    		FROM unions
	    		 WHERE up_id 
	    		   IN (SELECT up_id FROM upazilla WHERE name='$u')";
	    	$query =  $this->db->query($get);
	        return $query->result_array();
		}

		public function get_ongoing($upazilla){

			  $get="SELECT name, p_id from project_details
					WHERE type = 'Ongoing'
					AND location
					IN 
					(
						SELECT un_id
						FROM unions
						WHERE up_id

						IN (
						SELECT up_id
						FROM upazilla
						WHERE name =  '$upazilla')
					
					)";
 				
			$query=$this->db->query($get);
			return $query-> result_array();
		}

		public function updateDisPasswordToDb(){
			$newPass=$this->input->post('newpass');
			$username=$this->input->post('username');
			$data=array(
					'password'=>$newPass,
				);
			// $update="UPDATE admin SET password ='$newPass' WHERE username='$username'";

			$this->db->where('username', $username);
				if($this->db->Update('admin',$data))
			return true;
		}
	}
?>