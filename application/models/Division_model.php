<?php
class Division_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

        public function get_district($division)
		{
			$get="SELECT * FROM district WHERE division_name='$division' ORDER BY name ASC";
	        $query = $this->db->query($get);
	        return $query->result_array();
		}
		public function get_district_not($division, $district)
		{
			$get="select * from district where division_name= '$division' and name!= '$district' ORDER BY name";
	        $query =  $this->db->query($get);
	        return $query->result_array();
		}
		public function get_division_not($division)
		{
			$get="select * from district where division_name!='$division' GROUP BY division_name";
	        $query =  $this->db->query($get);
	        return $query->result_array();
		}

		public function get_error($district)
		{
			$get="select * from district where name='$district'";
	        $query =  $this->db->query($get);
	        return $query->result_array();
		}

		public function get_error_up($upazilla)
		{
			$get="select * from upazilla where name='$upazilla'";
	        $query =  $this->db->query($get);
	        return $query->result_array();
		}

		public function get_budget($district)
		{
			$get="SELECT * from budget WHERE dis_id=(select dis_id from district where name='$district')";
			$query=$this->db->query($get);
			return $query-> result_array();
		}

		public function get_es($district)
		{
			$get="SELECT e.name, e.seat_id FROM election_seats e JOIN district d ON e.dis_id=d.dis_id WHERE d.name='$district' ORDER BY e.name ASC";
			$query=$this->db->query($get);
			return $query-> result_array();
		}

		public function get_es_skeleton($district)
		{
			$get="  SELECT up.name, up.seat_id, es.seat_id
					FROM upazilla up
					JOIN election_seats es ON up.seat_id = es.seat_id
					WHERE up.dis_id

					IN (
					SELECT dis_id
					FROM district
					WHERE name ='$district'
					)";

			$query=$this->db->query($get);
			return $query-> result_array();
		}
		
		public function get_administration($district)
		{
			$get="SELECT adname.name, adinfo.position, adinfo.address, adinfo.contact, adinfo.join_date FROM administrators_info adinfo JOIN administrators_name adname ON adname.adminis_id=adinfo.adminis_id_info WHERE adname.dis_id_ad=(select dis_id from district where name='$district') ORDER BY priority ASC";

			$query=$this->db->query($get);
			return $query-> result_array();
		}

		public function get_leaders($district){
			
			$get="SELECT ln.leader_id, ln.seat_id, ln.name, li.current_position, li.address, li.contact, li.party, li.bio, mp.year
				FROM leader_name ln
				JOIN leader_info li ON ln.leader_id = li.leader_id_info
				LEFT OUTER JOIN mp_candidate_yr mp ON ln.leader_id = mp.leader_id_mp_can
	
				WHERE seat_id
				IN (
					SELECT seat_id
					FROM election_seats
					WHERE dis_id 
					  =( 
						SELECT dis_id
						FROM district
						WHERE name ='$district'
					)
				)
				ORDER BY li.priority ASC";

				$query=$this->db->query($get);
				return $query-> result_array();
		}

		public function get_former_position(){
			$get= "SELECT * FROM leader_former_position";
			$query=$this->db->query($get);
			return $query-> result_array();
		}

		public function get_upazilla($district){
			$get="SELECT * FROM upazilla WHERE dis_id=(select dis_id from district where name='$district') ORDER BY name";
			$query=$this->db->query($get);
				return $query-> result_array();
		}

		public function get_upazilla_not($district, $upazilla){
			$get="SELECT * FROM upazilla WHERE dis_id=(select dis_id from district where name='$district') AND name!='$upazilla' ORDER BY name";
			$query=$this->db->query($get);
				return $query-> result_array();
		}

		public function get_upazilla_default($district){
			$get="SELECT name FROM upazilla WHERE dis_id=(select dis_id from district where name='$district') ORDER BY name  LIMIT 1";
			$query=$this->db->query($get);
			return $query-> result_array();
		}
		public function get_up_administration($upazilla)
		{
			$get="SELECT adname.name, adinfo.position, adinfo.address, adinfo.contact, adinfo.join_date FROM administrators_info adinfo JOIN administrators_name adname ON adname.adminis_id=adinfo.adminis_id_info WHERE adname.dis_id_ad=(select up_id from upazilla where name='$upazilla') ORDER BY priority ASC";

			$query=$this->db->query($get);
			return $query-> result_array();
		}
		public function get_up_er($upazilla){

			$get="SELECT name, position, address, contact, join_date, party FROM elective_representative WHERE location_id=(SELECT up_id  FROM  upazilla WHERE name='$upazilla') ORDER BY priority ASC";
 				
			$query=$this->db->query($get);
			return $query-> result_array();
		}
		public function get_un($upazilla){

			  $get="SELECT ch.name AS ch_name, ch.position, ch.address, ch.contact, ch.party, ch.join_date, u.name, u.un_id
					FROM elective_representative ch
					LEFT OUTER JOIN unions u ON u.un_id = ch.location_id
					WHERE u.up_id
					IN 
					(
						SELECT up_id
						FROM upazilla
						WHERE name ='$upazilla'
					)";
 				
			$query=$this->db->query($get);
			return $query-> result_array();
		}
		public function get_ch_project($upazilla){

			  $get="SELECT p.name AS pname, p.p_id, p.type, p.location
					FROM project_details p
					LEFT OUTER JOIN unions u ON u.un_id= p.location
					WHERE u.up_id
					IN (
					SELECT up_id
					FROM upazilla
					WHERE name ='$upazilla'
					)";
 				
			$query=$this->db->query($get);
			return $query-> result_array();
		}
		public function get_ongoing_dis($district){

			  $get="SELECT info.name, info.p_id, g.img_link
					FROM project_details info
					JOIN project_gallery g ON info.p_id = g.p_id
					WHERE info.type = 'Ongoing'
					AND info.location
					IN 
					(
						SELECT un_id
						FROM unions
						WHERE up_id

						IN 
						(
							SELECT up_id
							FROM upazilla
							WHERE 
							dis_id =
								(
									select dis_id from district where name='$district'
								)
						)
					
					)";
 				
			$query=$this->db->query($get);
			return $query-> result_array();
		}

		public function get_upcoming_dis($district){

			  $get="SELECT info.name, info.p_id, g.img_link
					FROM project_details info
					JOIN project_gallery g ON info.p_id = g.p_id
					WHERE info.type = 'Upcoming'
					AND info.location
					IN 
					(
						SELECT un_id
						FROM unions
						WHERE up_id

						IN 
						(
							SELECT up_id
							FROM upazilla
							WHERE 
							dis_id =
								(
									select dis_id from district where name='$district'
								)
						)
					
					)";
 				
			$query=$this->db->query($get);
			return $query-> result_array();
		}
		
		public function get_completed_dis($district){

			  $get="SELECT info.name, info.p_id, g.img_link
					FROM project_details info
					JOIN project_gallery g ON info.p_id = g.p_id
					WHERE info.type = 'Completed'
					AND info.location
					IN 
					(
						SELECT un_id
						FROM unions
						WHERE up_id

						IN 
						(
							SELECT up_id
							FROM upazilla
							WHERE 
							dis_id =
								(
									select dis_id from district where name='$district'
								)
						)
					
					)";
 				
			$query=$this->db->query($get);
			return $query-> result_array();
		}
		public function get_ongoing($upazilla){

			  $get="SELECT info.name, info.p_id, g.img_link
					FROM project_details info
					JOIN project_gallery g ON info.p_id = g.p_id
					WHERE info.type = 'Ongoing'
					AND info.location
					IN 
					(
						SELECT un_id
						FROM unions
						WHERE up_id

						IN (
						SELECT up_id
						FROM upazilla
						WHERE name =  '$upazilla')
					
					)";
 				
			$query=$this->db->query($get);
			return $query-> result_array();
		}
		public function get_upcoming($upazilla){

			  $get="SELECT info.name, info.p_id, g.img_link
					FROM project_details info
					JOIN project_gallery g ON info.p_id = g.p_id
					WHERE info.type = 'Upcoming'
					AND info.location

					IN 
					(
						SELECT un_id
						FROM unions
						WHERE up_id

						IN (
						SELECT up_id
						FROM upazilla
						WHERE name =  '$upazilla')
					
					)";
 				
			$query=$this->db->query($get);
			return $query-> result_array();
		}
		public function get_completed($upazilla){

			  $get="SELECT info.name, info.p_id, g.img_link
					FROM project_details info
					JOIN project_gallery g ON info.p_id = g.p_id
					WHERE info.type = 'Completed'
					AND info.location

					IN 
					(
						SELECT un_id
						FROM unions
						WHERE up_id

						IN (
						SELECT up_id
						FROM upazilla
						WHERE name =  '$upazilla')
					
					)";
 				
			$query=$this->db->query($get);
			return $query-> result_array();
		}

		public function get_project_list_for_project_page($district, $type){
			$get="SELECT p.p_id, p.name , p.location, p.type, u.up_id 
				  FROM project_details p JOIN unions u ON p.location=u.un_id 
				  WHERE type='$type'
				  AND location 
				  IN
				  (
				  select un_id from unions where up_id 
				  	IN
				  	  (
				  	 	 select up_id from upazilla where dis_id 
				  	  		 IN 
				  	  	 		(
				  	  	 			select dis_id from district where name='$district'
				  	  	 		)
				  	   )
				   )";

			$query=$this->db->query($get);
			return $query-> result_array();
		}

		public function get_project_list_for_project_nav($district){
			$get="SELECT p.p_id, p.name , p.location, p.type, u.up_id 
				  FROM project_details p JOIN unions u ON p.location=u.un_id 
				  WHERE location 
				  IN
				  (
				  select un_id from unions where up_id 
				  	IN
				  	  (
				  	 	 select up_id from upazilla where dis_id 
				  	  		 IN 
				  	  	 		(
				  	  	 			select dis_id from district where name='$district'
				  	  	 		)
				  	   )
				   )";

			$query=$this->db->query($get);
			return $query-> result_array();
		}
		
		public function get_project_progress($pid){
			$get="SELECT * FROM project_progress where p_id='$pid'";
			$query=$this->db->query($get);
			return $query-> result_array();
		}

		public function get_project_img($pid){
			$get="SELECT * FROM project_gallery where p_id='$pid'";
			$query=$this->db->query($get);
			return $query-> result_array();
		}
		
		public function get_project_details($pid){
			$get="SELECT p.name AS pname, p.contractor AS cname, p.sdate, p.edate, p.budget, MAX(pr.progress_percentage) AS pp, p.type, p.description,  u.name as uname, ch.name AS chname 
				From project_details p
				JOIN project_progress pr ON p.p_id=pr.p_id 
				JOIN unions u ON p.location=u.un_id 
				JOIN elective_representative ch ON ch.location_id=u.un_id where p.p_id='$pid'";

			$query=$this->db->query($get);
			return $query-> result_array();
		}


		public function get_project_comment($pid){
			$get="SELECT * FROM comments WHERE p_id='$pid' ORDER BY date_cmnt ASC";

			$query=$this->db->query($get);
			return $query-> result_array();
		}

		public function get_recent_comment($pid){
			$get="SELECT * FROM comments WHERE p_id='$pid' ORDER BY date_cmnt ASC LIMIT 0,3";

			$query=$this->db->query($get);
			return $query-> result_array();
		}



		

}	