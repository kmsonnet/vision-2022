<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php foreach ($dis_id as $d):
       $d_id=$d['dis_id'];
	 endforeach;
?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>District Admin</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css"> -->
<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"> -->
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">        
<link rel="stylesheet" type="text/css" href="/css/admin.css">
<link rel="stylesheet" type="text/css" href="/css/w3.css">
<script src="/js/admin.js"></script>
</head>

<body 
<?php if($this->uri->segment(3)=="passwordUpdateSuccess") {?>
		onload="document.getElementById('msg').style.display='block'" 
<?php }?>
>
  <!-- body wrapper -->
  <div class="page-wrap">
  <div id="msg" class="w3-modal">
	  <div class="w3-modal-content w3-card-8" style="width:300px">
	    <header class="w3-container w3-teal">
	      <span onclick="document.getElementById('msg').style.display='none'"
	      class="w3-closebtn">&times;</span>
	      <h2>Update Info</h2>
	    </header>
	    <div class="w3-container">
	     	<p class="w3-text-green">Successfully Updated</p>
	    </div>
	    <hr>
	  </div>
	</div>
	  <div class="w3-row w3-green w3-padding-8" >
	  <button onclick="document.getElementById('changePass').style.display='block'" class="w3-col l2  w3-btn w3-orange w3-border w3-border-red w3-small w3-animate-right" style="top:15px;position:relative;left:15px;text-decoration:underline;">Change Password</button>>
	    <div id="changePass" class="w3-modal ">
		  <div class="w3-modal-content w3-animate-top w3-card-8 w3-col l5 w3-margin-left" >
		    <header class="w3-container w3-teal">
		      <span onclick="document.getElementById('changePass').style.display='none'"
		      class="w3-closebtn">&times;</span>
		      <h2>Change Password</h2>
		    </header>
		    <hr>
		    <div class="w3-container">
			    <form method="post" action="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'updatePassword'?>">
			    <p class="w3-text-black">Enter New Password</p>
			    	<input type="password" name="newpass" class="w3-input w3-border w3-small" placeholder="Enter New Password" required="required">
			    	<br>
			    <p class="w3-text-black">Re-enter New Password</p>
			    	<input type="password" name="newconpass" class="w3-input w3-border w3-small" placeholder="Confirm Password" required="required">
			    	<input type="password" name="username" value="<?php echo $username?>" class="w3-input w3-border w3-small w3-hide">
			    	<br>
			    	<button class="w3-btn w3-green w3-small" >Update</button>
			    </form>
		    </div>
		    <hr>
		   
		  </div>
	    </div>
	    
	  	<h2 class="text-center w3-col l9 w3-animate-left">Welcome To <?php echo $username?> District Admin Dashboard</h2>
	  	
	  	<a href="/admin/logout" class="w3-col l1  w3-btn w3-red w3-border w3-border-red w3-small w3-animate-right" style="top:15px;position:relative;right:20px;text-decoration:underline;">Logout</a>
	  </div>

	  <div class="w3-row w3-container w3-padding-8">
	 	<!-- administration starts -->
	 	
	 		<div class="w3-col l3 w3-border w3-round w3-card-2 w3-animate-top" onmouseover="focusthis(this)" onmouseout="unfocusthis(this)" style="padding-left:10px;padding-right:10px">
	 			<h3 class="w3-border-bottom w3-center">Manage Administrations</h3>
	 			
	 			<form action="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'addDisAdmin'?>" method="post">
				<div class="w3-accordion">
				  <p onclick="dcAccording('dcInfo')" class="w3-btn w3-border w3-small w3-light-grey">Deputy Commissioner (DC)</p>
				  <br>
				  <div id="dcInfo" class="w3-accordion-content w3-animate-zoom" style="margin-bottom:20px;margin-left:20px;margin-top:10px">
				   	<input type="text" name="dcname" placeholder="Enter DC Name" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<br>
				   	<input type="text" name="dcaddress" placeholder="Enter DC Office Address" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<br>
				   	<input type="text" name="dccontact" placeholder="Enter Contact Number" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<br>
				   	<input type="text" name="dcjoin" placeholder="Enter Joining Date" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<input type="text" name="dcposition" value="Deputy Commissioner (DC)" class="w3-input w3-border w3-col l9 w3-small w3-hide" required="required"> 
				  </div>

				</div>
				<br>
				<div class="w3-accordion">
				  <p onclick="dcAccording('adcInfo')" class="w3-btn w3-border w3-small w3-light-grey">Additional Deputy Commisioner (ADC)</p>
				  <div id="adcInfo" class="w3-accordion-content w3-animate-zoom" style="margin-bottom:20px;margin-left:20px;margin-top:10px">
				   	<input type="text" name="adcname" placeholder="Enter ADC Name" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<br>
				   	<input type="text" name="adcaddress" placeholder="Enter ADC Office Address" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<br>
				   	<input type="text" name="adccontact" placeholder="Enter Contact Number" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<br>
				   	<input type="text" name="adcjoin" placeholder="Enter Joining Date" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<input type="text" name="adcposition" value="Additional Deputy Commissioner (ADC)" class="w3-input w3-border w3-col l9 w3-small w3-hide" required="required"> 
				   
				  </div>

				</div>
				<br>
				<div class="w3-accordion">
				  <p onclick="dcAccording('spInfo')" class="w3-btn w3-border w3-small w3-light-grey">Superintendent of Police (SP)</p>
				  <div id="spInfo" class="w3-accordion-content w3-animate-zoom" style="margin-bottom:20px;margin-left:20px;margin-top:10px">
				   	<input type="text" name="spname" placeholder="Enter SP Name" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<br>
				   	<input type="text" name="spaddress" placeholder="Enter SP Office Address" class="w3-input w3-border w3-col l9 w3-small">
				   	<br>
				   	<br>
				   	<input type="text" name="spcontact" placeholder="Enter Contact Number" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<br>
				   	<input type="text" name="spjoin" placeholder="Enter Joining Date" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<input type="text" name="spposition" value="Superintendent of Police (SP)" class="w3-input w3-border w3-col l9 w3-small w3-hide" required="required"> 
				   
				  </div>
				</div>
				<br>
				<div class="w3-accordion">
				  <p onclick="dcAccording('aspInfo')" class="w3-btn w3-border w3-small w3-light-grey">Additional Superintendent of Police (ASP)</p>
				  <div id="aspInfo" class="w3-accordion-content w3-animate-zoom" style="margin-bottom:20px;margin-left:20px;margin-top:10px">
				   	<input type="text" name="aspname" placeholder="Enter ASP Name" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<br>
				   	<input type="text" name="aspaddress" placeholder="Enter ASP Office Address" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<br>
				   	<input type="text" name="aspcontact" placeholder="Enter Contact Number" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<br>
				   	<input type="text" name="aspjoin" placeholder="Enter Joining Date" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<input type="text" name="aspposition" value="Additional Superintendent of Police (ASP)" class="w3-input w3-border w3-col l9 w3-small w3-hide" required="required"> 
				   	<hr>
				    <button class="w3-btn w3-green w3-small">Save</button>
				  </div>
				</div>
				 <input type="text" name="adminDisId" value='<?php echo $d_id ?>' class="w3-input w3-border w3-col l9 w3-small w3-hide">
				<h2 class="w3-text-green w3-animate-opacity w3-center">
		  		<?php
		  		  if($this->uri->segment(3)=="successadmin")
		  		  	echo "Successfully Added";
		  		  if($this->uri->segment(3)=="failureadmin")
		  		  	echo "Failure. Try Again !";
		  		 ?>
	  			</h2>
	  			</form>

	 		</div>
	 	<!-- administration ends -->

	 	<!-- leader starts -->
	 		<div class="w3-col l4 w3-border w3-round w3-card-2 w3-animate-bottom" id='leader' onmouseover="focusthis(this)" onmouseout="unfocusthis(this)" style="padding-left:20px;padding-right:20px;margin-left:30px">
	 			<h3 class="w3-border-bottom w3-center">Manage Political Leaders</h3>
	 			<h2 class="w3-text-green w3-animate-opacity w3-center">
			  			<?php
			  		 	 if($this->uri->segment(3)=="successleader")
			  		 	 	echo "Successfully Added";
			  		 	 if($this->uri->segment(3)=="failureleader")
			  		  		echo "Failure. Try Again !";
			  			 ?>
	  			</h2>
	 			<form action="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'addLeaders'?>" method="post">
	 				<p>Name</p>
	 				<input type="text" name="lname" class="w3-input w3-border w3-small" placeholder="Enter Leader Name" required="required">
	 				<br>
	 			
	 				<p>Select Current Position</p>
	 				<select class="w3-col l8" onchange="createMpCandYear(this)" name="crnt_pos">
	 					<option value="Member-of-Parliament (MP)">Member of Parliament</option>
	 					<option value="Former-Member-Of-Parliament">Former Member Of Parliament</option>
	 					<option value="Minister">Minister</option>
	 					<option value="Former-Minister">Former Minister</option>
	 					<option value="MP-Candidate">MP Candidate</option>
	 				</select>
	 				<br>
	 				<br>

	 				<div id='mpcan'>
	 				</div>
	 				
	 				<br>
	 				<p>Select Election Seat</p>
	 				<select class="w3-col l8" name='seatid' id='election_seat'>
	 					<?php foreach ($election_seat as $es):?>
	 						<option value="<?php echo $es['seat_id']?>"><?php echo $es['name']?></option>
	 					<?php endforeach;?>
	 				</select>
	 				<br>
	 				<br>
	 				<p>Address</p>
	 				<input type="text" name="laddress" class="w3-input w3-border w3-small" placeholder="Enter Leader Address" required="required">
	 				<br>
	 				<p>Contact Number</p>
	 				<input type="text" name="lcontact" class="w3-input w3-border w3-small" placeholder="Enter Leader Contact Number" required="required">
	 				<br>
	 				<p>Select Political Party</p>
	 				<select name='party'>
	 					<option value="Bangladesh-Awami-League">Bangladesh Awami League</option>
	 					<option value="Bangladesh-Nationalist-Party[BNP]">Bangladesh Nationalist Party[BNP]</option>
	 					<option value="Jatiya-Party">Jatiya Party</option>
	 					<option value="Jamaat-e-Islami-Bangladesh">Jamaat-e-Islami Bangladesh</option>
	 					<option value="Bangladesh-Jatiya-Party[BJP]">Bangladesh Jatiya Party [BJP]</option>
	 				</select>
	 				<br>
	 				<br>
	 				<p>Leader Brief Bio</p>
	 				<textarea rows="4" cols="42" placeholder="Describe Leader here..." name='bio' required="required"></textarea>
	 				
	 				<p>Former Position?</p>

					<p>
					<input class="w3-radio" type="radio" onchange="showFormer()" name="formerPos" value="yes" id='yes'>
					<label class="w3-validate">Yes</label>
					</p>
					<p>
					<input class="w3-radio" type="radio" onchange="hideFormer()" name="formerPos" value="no" id='no'>
					<label class="w3-validate">No</label>
					</p>

					<div id="formerPosNum" >
						<p>How Many Position?</p>
						<input type="number" name="" min='1' max='4' placeholder="0" id='numOfField'>
						<span class="w3-btn w3-tiny" onclick="createFormerPositionField()">Create Field</span>
					</div>
					<br>
					<div id="formerCon">
					
					</div>
					<button class="w3-btn w3-green ">Save</button>
					<br>
					<br>
					
	 			</form>

	 		</div>
	 	<!-- leader ends-->

	 	<!-- Upazilla starts -->
	 		<div class="w3-col l2 w3-border w3-card-2 w3-animate-top" onmouseover="focusthis(this)" onmouseout="unfocusthis(this)" style="padding-left:10px;padding-right:10px;margin-left:30px">
	 			<h3 class="w3-border-bottom w3-center">Manage Upazillas</h3>

	 			<h4 class="w3-text-green w3-animate-opacity w3-center">
		  		<?php
		  		  if($this->uri->segment(3)=="successupazilla")
		  		  	echo "Successfully Added";
		  		  if($this->uri->segment(3)=="failureupazilla")
		  		  	echo "Failure. Try Again !";
		  		 ?>
	  			</h4>
	  			
	 			<p>How many upazillas?</p>
	 			<input type="number" id='upNum' placeholder="0" min="0" max="10">
	 			<button class="w3-btn w3-tiny" style="margin-top:-5px" onclick="createUpazillaField()">Create</button>
	 			<br>
	 
	 			<div id="upazillaCon">
		 			<form  action="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'addUpazilla'?>" method="post">
		 				<br>
		 				<div id="upinputCon">
		 					
		 				</div>
		 				<!-- <button class="w3-btn w3-green">Save</button> -->
		 				<hr>
		 				 <input type="text" name="upDisId" value='<?php echo $d_id ?>' class="w3-input w3-border w3-col l9 w3-small w3-hide">
		 			</form>
	 			</div>

	 		</div>
	 	<!-- Upazilla ends-->

	 	<!-- Upazilla starts -->
	 		<div class="w3-col l2 w3-border " style="padding-left:10px;padding-right:10px;margin-left:30px">
	 			<h4 class="w3-border-bottom w3-center">Manage Pourosobhas</h4>
	 		</div>
	 	<!-- Upazilla ends-->

	  </div>
	              
  </div>
  <!-- body wrapper ends-->

  <footer class="w3-container w3-green site-footer">
  	  <p style="text-align:center">Copyrigth <i class="fa fa-copyright"></i> 2016. Bangladesh Govt.</p>
  </footer>


</body>
</html>

