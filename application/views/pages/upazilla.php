<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>
<?php 
	$def="";
	foreach ($upazilla_dafault as $default):
		$def=str_replace("_", " ",$default['name']);
	endforeach;
	if($this->uri->segment(4,0)=='default'){
		echo $def;
	}
	else 
		echo str_replace("_", " ",$this->uri->segment(4,0))
?> Upazilla</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css"> -->
<link rel="stylesheet" type="text/css" href="/css/w3.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">        
<link rel="stylesheet" type="text/css" href="/css/upazilla.css">

</head>

<body onload="project_active(), chairman_active()">
	<!-- start body wrapper -->
	<div class="page-wrap">
		<header class="w3-row w3-container">
			<!-- district name starts -->
			<div class="w3-col l3 w3-animate-left" style="position:relative;top:7px;">
				<h3 > <i class="fa fa-home "></i>
				 <?php
			    				 
			    	if($this->uri->segment(4,0)=='default'){
			   			echo $def;
			   		}
			    	else 
			    	echo str_replace("_", " ",$this->uri->segment(4,0))
			  					 
				 ?> Upazilla</h3>
			</div>
			<!-- district name ends -->
			<br>
			<!-- search box starts -->
			<div class="w3-rest w3-animate-right">
				<form >
				 <div class="w3-twothird">
				   <input class="w3-input w3-border w3-hover-light-grey" placeholder="Search <?php echo str_replace("_", " ", $this->uri->segment(4,0));?>..." type="text">
				  </div>
				  <div class="w3-third">
				   <button class="w3-btn w3-hover-green w3-red w3-large w3-round">Search</button>
				  </div>
				</form>
			</div>
			<!-- search box ends -->
		</header>

		<br>
		<nav class="w3-row w3-animate-zoom">		
			<ul class="w3-navbar w3-card-1 w3-green">
			  <li ><a class="w3-hover-red"  href="<?php echo base_url()?>"><i class="fa fa-home"></i> Home</a></li>
			  <li ><a class="w3-hover-red"  href="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0)?>"><i class="fa fa-reply"></i> Return <?php echo $this->uri->segment(2,0)?> District</a></li>
			  <li class="w3-dropdown-hover w3-hover-red">
			    <a class="w3-hover-red" href="#"><i class="fa fa-university "></i> DIVISION: <?php echo $this->uri->segment(1,0)?> <i class="fa fa-sort"></i></a>
			    <div class="w3-dropdown-content w3-white w3-card-4">
			    <?php foreach ($division as $div): ?>
                	<a href="/<?php echo $div['division_name'].'/'.$div['name'].'/'.'upazilla/'.'default'?>"><?php echo $div['division_name'];?></a>
                <?php endforeach; ?>
			    </div>
			  </li>

			  <li class="w3-dropdown-hover w3-hover-red">
			    <a class="w3-hover-red" href="#"><i class="fa fa-road "></i> DISTRICT: <?php echo $this->uri->segment(2,0)?>  <i class="fa fa-sort"></i></a>
			    <div class="w3-dropdown-content w3-white w3-card-4">
			   	<?php foreach ($district as $dis): ?>
                	<a href="/<?php echo $this->uri->segment(1,0).'/'.$dis['name'].'/'.'upazilla/default'?>"><?php echo $dis['name'];?></a>
                <?php endforeach;?>
			    </div>
			  </li>

			  <li class="w3-dropdown-hover w3-hover-red">
			    <a class="w3-hover-red" href="#"><i class="fa fa-road "></i> UPAZILLA:
			    				<?php
			    				 
			    				  if($this->uri->segment(4,0)=='default'){
			   							echo $def;
			   						}
			    				  else 
			    				  		echo str_replace("_", " ",$this->uri->segment(4,0))
			  					  ?>  
			  		<i class="fa fa-sort"></i></a>
			    <div class="w3-dropdown-content w3-white w3-card-4">

				 	<?php 	foreach ($upazilla_list as $up): ?>
				 		<?php if($up['name']==$def && $this->uri->segment(4,0)=='default') continue; else {?>
				 		<a class="no-underline" href="<?php echo  $up['name']?>"><?php echo str_replace("_", " ", $up['name'])?></a>
				 		<?php }?>
					   
					<?PHP endforeach;?>
	
			    </div>
			  </li>

			</ul>
		</nav>
		
		<br>

		<!-- admin leader upazilla -->
		<div class="w3-row w3-container">

		    <left class="w3-quarter w3-animate-left">
			  <h2 class="text-center w3-text-brown"> <span class="border-bottom-top"><i class="fa fa-user-secret"></i> Administration</span></h2>
			  
				<ul class="w3-ul w3-card-2 admin">

				 <?php foreach($administration as $admin):?>
				  <li class="w3-padding-8 ">
				    <span class="w3-xlarge"><?php echo $admin['name']?></span><br>
				    <span><?php echo $admin['position'] ?><span><br>

					<div class="w3-dropdown-click">
				    	<div onclick="admin(this)" class="w3-small underline w3-text-blue">View Details <sup> <i class="fa fa-question-circle  w3-small"></i></sup></div>

				   		 <div id="DC" class="admin-details w3-dropdown-content w3-col l4 w3-card-8 w3-blue-grey w3-animate-zoom w3-padding-medium w3-small ">
				   		   <span onclick="close_pop(this)" class="w3-closebtn w3-small">x</span>
						   <p><b>Office :</b><i class="w3-tiny"><?php echo $admin['address']?></i></p>
						   <p><b>Telephone :</b><i class="w3-tiny"><?php echo $admin['contact']?></i></p>
						   <p><b>Since :</b><i class="w3-tiny"> <?php echo $admin['join_date']?>-Present</i></p>
						 </div>
					</div>
				  </li>
				   <?php endforeach;?>
				 
				</ul>

			</left>

			<center class="w3-col l6 text-center w3-animate-zoom w3-container">
				<h2 class="text-center w3-text-brown"> <span class="border-bottom-top"><i class="fa fa-users"></i> Elective Representive</span></h2>

				<div class="w3-padding-medium text-left w3-container w3-animate-zoom">

					<ul class="w3-ul w3-card-2">
					 <?php $i=0; foreach($up_er as $er_up):?>
					  <li class="w3-padding-8">
					   <img src="/img/avatar/no.png" class="w3-left w3-circle w3-margin-right" style="width:60px">
					    <span class="w3-xlarge text-left"><?php echo $er_up['name']?></span><br>
					    <span><?php echo str_replace("-", " ", $er_up['position'])?></span>
					    <a onclick="document.getElementById('elective_details'+<?php echo $i?>).style.display='block'" class="w3-small w3-text-blue" href="#">Learn More...</a>
					  </li>
					  <!-- leaders details -->
					  	<div id="<?php echo 'elective_details'.$i?>" class="w3-modal" >
						  <div class="w3-modal-content w3-animate-top elective_details_modal" style="width:400px">
						    <header class="w3-container w3-teal">
						      <span onclick="document.getElementById('elective_details'+<?php echo $i?>).style.display='none'"
						      class="w3-closebtn">&times;</span>
						      <h2><?php echo $er_up['name']?></h2>
						    </header>
						    <div class="w3-container">
						    <hr>
						      <p><b>Position :</b><span class="w3-text-grey"><?php echo str_replace("-", " ", $er_up['position'])?></p>
						      <p><b>Political Party :</b><span class="w3-text-grey"><?php echo $er_up['party']?></p>
						      <p><b>Address :</b><span class="w3-text-grey"><?php echo $er_up['address']?></p>
						      <p><b>Contact :</b><span class="w3-text-grey"><?php echo $er_up['contact']?></p>
						      <p><b>Join Date :</b><span class="w3-text-grey"><?php echo $er_up['join_date']?></p>
						      <hr>
						    </div>
						   
						  </div>
						</div>

					  <!-- leaders details ends-->
					    <?php $i++; endforeach;?>
					</ul>  
				</div>
			</center>

			<right class="w3-quarter w3-animate-right w3-container">
				<h2 class="text-center w3-text-brown"> <span class="border-bottom-top"><i class="fa fa-tree "></i> Unions</span></h2>
				<?php $i=0; foreach ($union as $un):?>
				<div class="w3-accordion w3-light-grey">
				  <button onclick="unionAccordions('Union'+<?php echo $i?>)" class="w3-btn-block w3-left-align w3-blue-grey"><?php echo str_replace("_"," ",$un['name'])?></button>

				  <div id="<?php echo "Union".$i?>" class="w3-accordion-content w3-animate-zoom" style='margin-top:14px'>
				    <span class="w3-container w3-padding-4"><i class="fa fa-user "></i> Chairman of <?php echo  str_replace("_"," ",$un['name'])?> <img src='/img/i.png' width="15px" onclick="document.getElementById('id'+<?php echo $i?>).style.display='block'" style='cursor: pointer;'  title="See details"></span>
				  </div>

				</div>
				<hr>
				
				<!-- chairman details -->
				<div id="<?php echo 'id'.$i?>" class="w3-modal" >
					 <div class="w3-modal-content w3-card-4 w3-animate-zoom chairman_details_modal" style="width:600px">
					  <header class="w3-container w3-teal">
					   <span onclick="document.getElementById('id'+<?php echo $i?>).style.display='none'"
					   class="w3-closebtn w3-padding-top">&times;</span>
					   <h2>Chairman Details</h2>
					  </header>

					  <ul class="w3-pagination w3-white w3-border-bottom" style="width:100%;">
					   <li><a href="#" class="unilink w3-light-grey" onclick="openChairman(event, 'bio'+<?php echo $i?>)">Chairman Bio</a></li>
					   <li><a href="#" class="unilink" onclick="openChairman(event, 'project'+<?php echo $i?>)">Projects</a></li>
					 
					  </ul>

					  <div id="<?php echo 'bio'.$i?>" class="w3-container chairman" style="display:block">
					   <p>Name: <span class="w3-text-grey w3-medium"><?php echo $un['ch_name']?></span></p>
					   <p>Position : <span class="w3-text-grey w3-medium"><?php echo $un['position']?></span></p>
					   <p>Political Party : <span class="w3-text-grey w3-medium"><?php echo $un['party']?></span></p>
					   <p>Address : <span class="w3-text-grey w3-medium"><?php echo $un['address']?></span></p>
					   <p>Contact : <span class="w3-text-grey w3-medium"><?php echo $un['contact']?></span></p>
					   <p>Join Date : <span class="w3-text-grey w3-medium"><?php echo $un['join_date']?></span></p>
					 
					  </div>

					  <div id="<?php echo 'project'.$i?>" class="w3-container chairman">
					  <table class="w3-table w3-striped w3-border">
							<thead>
							<tr class="w3-red">
							  <th class="w3-green">Completed</th>
							  <th class="w3-orange">Ongoing</th>
							  <th class="w3-red">Upcoming</th>
							</tr>
							</thead>
							<?php foreach($ch_project as $chp):?>
								<?php if($un['un_id']==$chp['location']){?>
								<tr>
									<?php if($chp['type']=='Completed'){?>
									  <td><a href="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'Projects/Ongoing/'.$chp['p_id']?>" class="w3-small"><?php echo $chp['pname']?></a></td>
									<?php } else{?>
										 <td><a href="#" class='w3-small w3-text-grey' style="text-decoration:none">No Completed Project<a></td>
									<?php }?>

									<?php if($chp['type']=='Ongoing'){?>
									  <td><a href="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'Projects/Ongoing/'.$chp['p_id']?>" class="w3-small"><?php echo $chp['pname']?></a></td>
									<?php }else{?>
										 <td><a href="#" class='w3-small w3-text-grey' style="text-decoration:none">No Ongoing Project<a></td>
									<?php }?>

									<?php if($chp['type']=='Upcoming'){?>
									  <td><a href="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'Projects/Ongoing/'.$chp['p_id']?>" class="w3-small"><?php echo $chp['pname']?></a></td>
									<?php }else{?>
										 <td><a href="#" class='w3-small w3-text-grey' style="text-decoration:none">No Upcoming Project<a></td>
									<?php }?>
								</tr>
								<?php } else continue;?>
							<?php endforeach;?>
					  </table>
					  </div>


					  <div class="w3-container w3-light-grey w3-padding">
					   <button class="w3-btn w3-right w3-white w3-border w3-small"
					   onclick="document.getElementById('id'+<?php echo $i?>).style.display='none'">Close</button>
					  </div>
					 </div>
					</div>
					<!-- chairman details ends -->
				<?php $i++; endforeach;?>
			</right>

		</div>
		<!-- admin leader upazilla ends-->


		<!-- projects starts -->
		<div class="w3-container w3-animate-bottom">
			 <h2 class="text-center w3-text-brown"> <span class="border-bottom-top">Upazilla Projects</span></h2>
			<div class="w3-row">
			  <span class="pointer" onclick="openProject(event, 'Ongoing');">
			    <div class="w3-third typelink w3-bottombar w3-hover-light-grey w3-padding">Ongoing</div>
			  </span>
			  <span class="pointer" href="#" onclick="openProject(event, 'Upcoming');">
			    <div class="w3-third typelink w3-bottombar w3-hover-light-grey w3-padding">Upcoming</div>
			  </span>
			  <span class="pointer" onclick="openProject(event, 'Completed');">
			    <div class="w3-third typelink w3-bottombar w3-hover-light-grey w3-padding">Completed</div>
			  </span>
			</div>


			<div id="Ongoing" class="w3-row-padding w3-margin-top project w3-animate-opacity">

				 <?php $prev=""; foreach ($ongoing as $on): if($prev!=$on['p_id']){?>
				  <div class="w3-quarter">
					<div class="w3-card-2">
						<img src="<?php echo str_replace("./", "/", $on['img_link'])?>" style="width:100%;height:200px">
						<div class="w3-container w3-center">
							<h5><?php echo  $on['name']?></h5>
							<a href="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'Projects/Ongoing/'.$on['p_id']?>" class="w3-small">View Details</a>
							<br>
							<br>
						</div>
					</div>
				  </div>
				  <?php $prev=$on['p_id'];} else continue;?>
				<?php endforeach;?>
			  
			</div>

			<div id="Upcoming" class="w3-container w3-margin-top project w3-animate-opacity">
			 
				 <?php $prev=""; foreach ($upcoming as $up): if($prev!=$up['p_id']){?>
				  <div class="w3-quarter">
					<div class="w3-card-2">
						<img src="<?php echo str_replace("./", "/", $up['img_link'])?>" style="width:100%;height:200px"">
						<div class="w3-container w3-center">
							<h5><?php echo  $up['name']?></h5>
							<a href="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'Projects/Upcoming/'.$up['p_id']?>" class="w3-small">View Details</a>
							<br>
							<br>
						</div>
					</div>
				  </div>
				  <?php $prev=$up['p_id'];} else continue;?>
				<?php endforeach;?>
			  
			</div>

			<div id="Completed" class="w3-container w3-margin-top project w3-animate-opacity">
			<?php $prev=""; foreach ($completed as $com): if($prev!=$com['p_id']){?>
				  <div class="w3-quarter" >
					<div class="w3-card-2" style="margin-left:20px;">
						<img src="<?php echo str_replace("./", "/", $com['img_link'])?>" style="width:100%;height:200px"">
						<div class="w3-container w3-center">
							<h5><?php echo  $com['name']?></h5>
							<a href="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'Projects/Completed/'.$com['p_id']?>" class="w3-small">View Details</a>
							<br>
							<br>
						</div>
					</div>
				  </div>
				  <?php $prev=$com['p_id'];} else continue;?>
				<?php endforeach;?>
			</div>

		</div>
		<!-- projects ends-->
		<br>
		<br>

	</div>
	<!-- start body wrapper -->

	<footer class="w3-container w3-green site-footer w3-animate-bottom">
  	  <p style="text-align:center">Copyrigth <i class="fa fa-copyright"></i> 2016. Bangladesh Govt.</p>
	</footer>

<script src="/js/upazilla.js"></script>
</body>
</html>
