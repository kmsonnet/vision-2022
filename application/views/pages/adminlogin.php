<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>ADMIN LOGIN</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css"> -->
<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"> -->
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">        
<link rel="stylesheet" type="text/css" href="/css/home.css">
<link rel="stylesheet" type="text/css" href="/css/w3.css">

<script src="/js/home.js"></script>
</head>

<body>
  <!-- body wrapper -->
  <div class="page-wrap">

      <div class="w3-col l7 w3-animate-zoom" style="margin-left:35%;margin-top:10%">

        <div class="w3-card-4 w3-padding-8" style="width:50%">
            <div class="w3-container w3-brown">
              <h2 class="text-center">ADMINISTRATIVE LOGIN</h2>
            </div>
            <form class="w3-container" action="<?php echo '/admin/login_validation'?>" method="post">
            
              <br>
              <p>
              <label class="w3-label w3-text-brown"><b>Username</b></label>
              <input class="w3-input w3-border w3-sand" name="username" placeholder="Enter Username" type="text" autofocus required="required" autocomplete="OFF">
              </p>
              <br>
              <p>
              <label class="w3-label w3-text-brown"><b>Password</b></label>
              <input class="w3-input w3-border w3-sand" placeholder="Enter password" name="password" type="password" required="required" autofocus autocomplete="OFF">
              </p>
              <br>
              <p>
              <label class="w3-label w3-text-brown"><b>Choose Admin Type</b></label>
              <select name="type">
                <option value="superadmin">Super Admin</option>
                <option value="district">District Admin</option>
                <option value="upazilla">Upazilla Admin</option>
                <option value="pourosova">Pourosova Admin</option>
              </select>
              </p>

              <hr>
              <p class="text-center">
              <button class="w3-btn w3-brown">Login</button>
              </p>

            </form>
            <hr>
            <p class="text-center"> <?php echo validation_errors();?> </p>
           
         </div>

      </div>
              
  </div>
  <!-- body wrapper ends-->



</body>
</html>

