<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
<title><?php echo $this->uri->segment(2,0);?> | Projects </title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css"> -->
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">        
<link rel="stylesheet" type="text/css" href="/css/w3.css">
<link rel="stylesheet" type="text/css" href="/css/projects.css">
</head>


<body onload="default_project() ">
	<!-- start body wrapper -->
	<div class="page-wrap">

		<nav class="w3-row w3-animate-zoom">		
			<ul class="w3-navbar w3-card-1 w3-green">
			  <li ><a class="w3-hover-red"  href="<?php echo base_url()?>"><i class="fa fa-home"></i> Home</a></li>
			  <li ><a class="w3-hover-red"  href="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0)?>"><i class="fa fa-reply"></i> Return <?php echo $this->uri->segment(2,0)?> District</a>
			  </li>

			  <li class="w3-dropdown-hover w3-hover-red">
			    <a class="w3-hover-red" href="#"><i class="fa fa-road "></i> DISTRICT: <?php echo $this->uri->segment(2,0)?>  <i class="fa fa-sort"></i></a>
			    <div class="w3-dropdown-content w3-white w3-card-4">
			   	<?php foreach ($district as $dis): ?>
                	<a href="/<?php echo $this->uri->segment(1,0).'/'.$dis['name'].'/'.'Projects/'.$this->uri->segment(4,0).'/'.'1'?>"><?php echo $dis['name'];?></a>
                <?php endforeach; ?>
			    </div>
			  </li>

			  <li class="w3-dropdown-hover w3-hover-red">
			    <a class="w3-hover-red" href="#"><i class="fa fa-road "></i> Project Type: <?php echo $this->uri->segment(4,0)?>  <i class="fa fa-sort"></i></a>
			    <div class="w3-dropdown-content w3-white w3-card-4">
			    	<?php $i=0;$j=0; $k=0; foreach ($project_nav as $pl):?>
			    		<?php if(!$i>0)if($pl['type']=='Completed' && $this->uri->segment(4,0)!='Completed'){$i++;?>
			  			<a href="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.$this->uri->segment(3,0)?>/Completed/<?php echo $pl['p_id']?>">Completed</a>
			  			<?php }?>

			  			<?php  if(!$j>0) if($pl['type']=='Ongoing' &&  $this->uri->segment(4,0)!='Ongoing'){$j++;?>
			  			<a href="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.$this->uri->segment(3,0)?>/Ongoing/<?php echo $pl['p_id']?>">Ongoing</a>
			  			<?php }?>

			  			<?php  if(!$k>0) if($pl['type']=='Upcoming' && $this->uri->segment(4,0)!='Upcoming'){$k++;?>
			  			<a href="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.$this->uri->segment(3,0)?>/Upcoming/<?php echo $pl['p_id']?>">Upcoming</a>
			  			<?php }?>
			  		<?php endforeach;?>
			    </div>
			  </li>

			</ul>
		</nav>

	<!-- main body starts -->
	<div class="w3-row">	
		<!-- left col -->
		<nav class="w3-sidenav w3-col l2  w3-light-teal w3-card w3-animate-left" style="top:40px;max-height:600px">
		<!-- pouroshova starts -->
		 <!--  <div >
		   <h4 class="w3-container w3-light-green w3-padding-4" style="margin-top:-4px;margin-bottom:-4px;padding:50px;"> Pouroshobhas</h4>

		    <div class="w3-accordion-content  w3-show w3-animate-left w3-white">
		      <ul class="w3-ul w3-border w3-padding-2">
 -->
		      	<!-- nested accordion -->
		      	<!--   <div class="w3-accordion">
					<li onclick="Project(this)" class="w3-teal"><i class="fa fa-plus-square pointer"></i><span> Madaripur </span></li>
					<div  class="w3-accordion-content w3-animate-left project">
					  	  <li><a class="w3-small" href="2">Project-1</a></li>
						  <li><a class="w3-small" href="63">Project-2</a></li>
						  <li><a class="w3-small" href="15">Project-3</a></li>
						  <li><a class="w3-small" href="9400000">Project-4</a></li>
						  <li><a class="w3-small" href="82">Project-5</a></li>
					</div>
				  
				  <li onclick="Project(this)" class="w3-teal"><i class="fa fa-plus-square pointer"></i><span> Rajoir </span></li>
					<div class="w3-accordion-content w3-animate-left project">
					  	  <li><a class="w3-small" href="13">Project-1</a></li>
						  <li><a class="w3-small" href="5">Project-2</a></li>
						  <li><a class="w3-small" href="112">Project-3</a></li>
						  <li><a class="w3-small" href="2900000">Project-4</a></li>
						  <li><a class="w3-small" href="28">Project-5</a></li>
					</div>
				  </div> -->
				<!-- nested accordion ends-->
			<!--   </ul>
		  </div> -->
		  <!-- pouroshova starts -->

		  <!-- upazilla starts -->
		  <!-- <hr> -->
		  <div class="w3-accordion">
		   <h4 class="w3-container w3-light-green w3-padding-4" style="margin-top:-4px;margin-bottom:-4px;padding:50px;"> Upazilla</h4>

		    <div class="w3-accordion-content  w3-show w3-animate-left w3-light-teal">
		      <ul class="w3-ul w3-border w3-padding-2">

		      	<!-- nested accordion -->
		      	<?php foreach ($upazilla as $up): ?>
		      		
		      	  <div class="w3-accordion">
					<li onclick="Project(this)" class="w3-teal"><i class="fa fa-plus-square pointer"></i><span><?php echo $up['name']?></span></li>
					<div  class="w3-accordion-content w3-animate-left project">
						<?php foreach ($project_list as $pl): if($pl['up_id']==$up['up_id']){?>
					  	  <li><a class="w3-small" href="<?php echo $pl['p_id']?>"><?php echo $pl['name']?> </a></li>
					  	  <?php } else continue;?>
						<?php endforeach;?>
					</div>
					
				  </div>
				<!-- nested accordion ends-->
			<?php endforeach;?>
			  </ul>
		  </div>

		  <!-- upazilla ends -->
		</nav>
		<!-- left col ends -->


		<!-- center col starts -->
		<!-- slide  -->
		<div class="w3-col l8" style="margin-left:17%">
			<div class="w3-content slide_con" >
				<!-- <h3 class="text-center">Project Name</h3> -->

				<div id='conSlide'>
					
				</div>
				 
				  <!-- <img class="mySlides w3-animate-left w3-round w3-card-4" src="/img/motor/1.jpg" style="width:100%">
				  <img class="mySlides w3-animate-left w3-round w3-card-4" src="/img/motor/2.jpg" style="width:100%">
				  <img class="mySlides w3-animate-left w3-round w3-card-4" src="/img/motor/3.jpg" style="width:100%">
				  <img class="mySlides w3-animate-left w3-round w3-card-4" src="/img/motor/4.jpg" style="width:100%">
				  <img class="mySlides w3-animate-left w3-round w3-card-4" src="/img/motor/5.jpg" style="width:100%"> -->

				  <div class="w3-row-padding w3-row  w3-section w3-animate-bottom" style="margin-left:10%" id="thumbnail">
				    <div class="w3-col s2 w3-hide">
				      <img class="demo w3-border w3-hover-shadow" src="/img/motor/1.jpg" style="width:100%" onclick="currentDiv(1)">
				    </div>
				    <div class="w3-col s2 w3-hide">
				      <img class="demo w3-border w3-hover-shadow" src="/img/motor/2.jpg" style="width:100%" onclick="currentDiv(2)">
				    </div>
				    <div class="w3-col s2 w3-hide">
				      <img class="demo w3-border w3-hover-shadow" src="/img/motor/3.jpg" style="width:100%" onclick="currentDiv(3)">
				    </div>
				     <div class="w3-col s2 w3-hide">
				      <img class="demo w3-border w3-hover-shadow" src="/img/motor/4.jpg" style="width:100%" onclick="currentDiv(4)">
				    </div>
				     <div class="w3-col s2 w3-hide">
				      <img class="demo w3-border w3-hover-shadow" src="/img/motor/5.jpg" style="width:100%" onclick="currentDiv(5)">
				    </div>


				  </div>
			</div>
		<!-- slide ends -->
			<div>
		
			<div class="w3-col l8"  style="margin-left:17%;margin-top:4%">
				<h3>Progress</h3>
			</div>

			<div class="w3-col l8" id="intervalCon"  style="margin-left:17%;margin-top:4%">
				<?php foreach ($project_progress as $pprog):?>
				<div class="w3-btn w3-round-large interval" onclick="createImg(this)"><?php echo $pprog['start'].' To '.$pprog['end']?>

					<?php foreach ($project_img as $pimg): if($pimg['prog_id']==$pprog['prog_id']){?>
						<img src="<?php echo str_replace("./", "/", $pimg['img_link'])?>" class="w3-hide">
					<?php } else continue; endforeach;?>
				</div>
				<?php endforeach;?>

				<!-- <div class="w3-btn w3-round-large interval" onclick="createImg(this)">Nov 2012 to Jan 2013
					<img src="/img/landscape/1.jpg" class="w3-hide">
					<img src="/img/landscape/2.jpg" class="w3-hide">
					<img src="/img/landscape/3.jpg" class="w3-hide">
					<img src="/img/landscape/4.jpg" class="w3-hide">
					<img src="/img/landscape/5.jpg" class="w3-hide">
				</div>

				<div class="w3-btn w3-round-large interval" onclick="createImg(this)">Feb to Apr 2013
					<img src="/img/photography/1.jpg" class="w3-hide">
					<img src="/img/photography/2.jpg" class="w3-hide">
					<img src="/img/photography/3.jpg" class="w3-hide">
					<img src="/img/photography/4.jpg" class="w3-hide">
				</div> -->

			</div>


			<!-- comment -->
			<div class="w3-col l8" id="intervalCon"  style="margin-left:17%;margin-top:4%">
				<h3>Add Comment or Ask a Question</h3>
				<hr>
				<form class="w3-container"  action="<?php echo $this->uri->segment(5,0).'/'.'addComment'?>" method="post">	
					<p><input class="w3-input w3-border w3-white w3-col l10" name='commentator' placeholder="Enter you name ..." type="text" height="5px;"></p>
					<br>
					<br>
					<br>
					<textarea rows="4" cols="42" placeholder="Enter your comment here..." name='comment' required="required"></textarea>
					<input class="w3-input w3-border w3-white w3-col l10 w3-hide" value="<?php echo $this->uri->segment(5,0)?>" name='pid' type="text" >
					<input class="w3-input w3-border w3-white w3-col l10 w3-hide" value="<?php echo current_url()?>" name='purl' type="text" >
					<p>
					<button style="margin-top:5px;" class="w3-btn w3-tiny w3-brown ">Submit</button></p>
				</form>
				<hr>
			</div>
			<!-- comment ends -->

			<div class="w3-col l8" id="intervalCon"  style="margin-left:17%;margin-top:4%">
			<h3>All Comments</h3>
			<?php foreach ($project_comment as $pc): ?>
				<div class="w3-padding-small w3-light-grey">
			  		<p class="w3-small"><b><?php if($pc['commentator']==""){ echo "Anonymus";} else echo $pc['commentator']?>: </b><span class="w3-tiny"><?php echo $pc['comment']?></span></p>
			  		<p class="w3-tiny"><i><?php echo $pc['date_cmnt']?></i></p>
			  	</div>
			  	<br>
			<?php endforeach;?>
			</div>
			<!-- show comments -->


			<!--end  show comments -->

		</div>
		<!-- center col ends -->
		
		<!-- right col starts -->
		<nav class="w3-sidenav w3-col l2  w3-light-teal w3-card w3-animate-right" style="right:0px;top:40px;max-height:100%">

		<!-- info start -->
		  <div>
		   <h4 class="w3-container w3-center w3-light-green w3-padding-4" style="margin-top:-4px;margin-bottom:-4px;padding:50px;"> Project Info</h4>
		<?php foreach ($project_details as $pd): ?>
		    <div class="w3-accordion-content  w3-show w3-animate-right w3-white">
		      <ul class="w3-ul w3-small w3-border w3-padding-2">
		      <li ><b>Name</b> :<span class="w3-tiny"><?php echo $pd['pname']?></span></li>
		      <li><b>Supervisor Name</b> :<span class="w3-tiny"><?php echo $pd['chname']?></span></li>
		      <li><b>Contractor Name</b> :<span class="w3-tiny"><?php echo $pd['cname']?></span></li>
		      <li><b>Start Date</b> :<span class="w3-tiny"><?php echo $pd['sdate']?></span></li>
		      <?php if($pd['type']=='Ongoing' || $pd['type']=='Upcoming'){?>
		     	 <li><b>Probable End Date </b>:<span class="w3-tiny"><?php echo $pd['edate']?></span></li>
		      <?php } if($pd['type']=='Completed'){?>
		      	 <li><b>End Date </b>:<span class="w3-tiny"><?php echo $pd['edate']?></span></li>
		      <?php }?>
		      <li><b>Project Location </b>:<span class="w3-tiny"><?php echo str_replace("_"," ",$pd['uname'])?> Union</span> </li>
		      <li><b>Budget</b>:<span class="w3-tiny"><?php echo $pd['budget']?> Tk.</span></li>
		      <li><b>Progress</b>: <span class="w3-tiny"><?php echo $pd['pp']?>%</span></li>
		      <li><b>Description</b>: <span class="w3-tiny"><?php echo $pd['description']?></span></li>
			  </ul>
		  </div>
		<?php endforeach;?>
		  <hr>

		  <h5 class="w3-container w3-center w3-light-green" style="margin-top:-4px;margin-bottom:-4px;padding:5px;">Recent Comments</h5>
		  	<br>
		  	<div class="w3-sidenav w3-col l2" style="overflow:scroll;height:200px">
			  	<?php foreach ($recent_comment as $rc): ?>
				<div class="w3-padding-small w3-light-grey">
			  		<p class="w3-small"><b><?php if($rc['commentator']==""){ echo "Anonymus";} else echo $rc['commentator']?>: </b><span class="w3-tiny"><?php echo $rc['comment']?></span></p>

			  	</div>
			  	<br>
			<?php endforeach;?>
			 </div>
		  </div>
		  <!-- info ends-->
		</nav>
		<!-- right col ends -->

		</div>
		<!-- main body ends -->
	</div>
	<!-- ens body wrapper -->

	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<footer class="w3-container w3-green site-footer w3-animate-bottom">
  	  <p style="text-align:center">Copyrigth <i class="fa fa-copyright"></i> 2016. Bangladesh Govt.</p>
	</footer>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script async src="/js/projects.js"></script>
</body>
</html>