<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
<title><?php echo $this->uri->segment(2,0);?> District</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css"> -->
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">        
<link rel="stylesheet" type="text/css" href="/css/w3.css">
<link rel="stylesheet" type="text/css" href="/css/district.css">

<body onload=" project_active(),seat_active()">

	<!-- start body wrapper -->
	<div class="page-wrap">

		<header class="w3-row w3-container">
			<!-- district name starts -->
			<div class="w3-col l3 w3-animate-left" style="position:relative;top:7px;">
				<h2 > <i class="fa fa-home "></i> <?php echo $this->uri->segment(2,0);?> District</h2>
			</div>
			<!-- district name ends -->
			<br>
			<!-- search box starts -->
			<div class="w3-rest w3-animate-right">
				<form >
				 <div class="w3-twothird">
				   <input class="w3-input w3-border w3-hover-light-grey" placeholder="Search <?php echo $this->uri->segment(2,0);?>..." type="text">
				  </div>
				  <div class="w3-third">
				   <button class="w3-btn w3-hover-green w3-red w3-large w3-round">Search</button>
				  </div>
				</form>
			</div>
			<!-- search box ends -->
		</header>

		<br>
		<nav class="w3-row w3-animate-zoom">		
			<ul class="w3-navbar w3-card-1 w3-green">
			  <li ><a class="w3-hover-red"  href="<?php echo base_url()?>"><i class="fa fa-home"></i> Home</a></li>
			  <li class="w3-dropdown-hover w3-hover-red">
			    <a class="w3-hover-red" href="#"><i class="fa fa-university "></i> DIVISION: <?php echo $this->uri->segment(1,0)?> <i class="fa fa-sort"></i></a>
			    <div class="w3-dropdown-content w3-white w3-card-4">
			    <?php foreach ($division as $div): ?>
                	<a href="/<?php echo $div['division_name'].'/'.$div['name']?>"><?php echo $div['division_name'];?></a>
                <?php endforeach; ?>
			    </div>
			  </li>

			  <li class="w3-dropdown-hover w3-hover-red">
			    <a class="w3-hover-red" href="#"><i class="fa fa-road "></i> DISTRICT: <?php echo $this->uri->segment(2,0)?>  <i class="fa fa-sort"></i></a>
			    <div class="w3-dropdown-content w3-white w3-card-4">
			   	<?php foreach ($district as $dis): ?>
                	<a href="<?php echo $dis['name']?>"><?php echo $dis['name'];?></a>
                <?php endforeach; ?>
			    </div>
			  </li>

			</ul>
		</nav>

		<br>

		<!-- admin leader upazilla -->
		<div class="w3-row w3-container">

			<left class="w3-quarter w3-animate-left">
			  <h2 class="text-center w3-text-brown"> <span class="border-bottom-top"><i class="fa fa-user-secret"></i> Administration</span></h2>
			 
			
				<ul class="w3-ul w3-card-2 admin">

				 <?php foreach($administration as $admin):?>
				  <li class="w3-padding-8 ">
				    <span class="w3-xlarge"><?php echo $admin['name']?></span><br>
				    <span><?php echo $admin['position'] ?><span><br>

					<div class="w3-dropdown-click">
				    	<div onclick="admin(this)" class="w3-small underline w3-text-blue">View Details <sup> <i class="fa fa-question-circle  w3-small"></i></sup></div>

				   		 <div id="DC" class="admin-details w3-dropdown-content w3-col l4 w3-card-8 w3-blue-grey w3-animate-zoom w3-padding-medium w3-small ">
				   		   <span onclick="close_pop(this)" class="w3-closebtn w3-small">x</span>
						   <p><b>Office :</b><i class="w3-tiny"><?php echo $admin['address']?></i></p>
						   <p><b>Telephone :</b><i class="w3-tiny"><?php echo $admin['contact']?></i></p>
						   <p><b>Since :</b><i class="w3-tiny"> <?php echo $admin['join_date']?>-Present</i></p>
						 </div>
					</div>
				  </li>
				   <?php endforeach;?>
				 
				</ul>

			</left>


			<center class="w3-col l6 w3-animate-zoom w3-container">
				 <h2 class="text-center w3-text-brown"> <span class="border-bottom-top"><i class="fa fa-users "></i>
				 Political Leaders</span></h2>
				 <h3 class="w3-text-grey">Election Seats</h3>
				 <?php $i=0; foreach ($election_seat as $es): ?>
					<ul class="w3-navbar w3-light-grey w3-card-8" style="float:left;margin-top:8px" >
					  	<li style="width:156.5px;"><a class=" w3-rightbar w3-small  w3-border tablink" href="#"  onclick="openseat(event,'<?php echo "seat".$i?>');"><?php echo $es['name'] ?>  <i class="fa fa-info-circle w3-medium" onclick="document.getElementById('seat_details'+<?php echo $i?>).style.display='block'" ></i></a>
					    </li>
					</ul>

				 
				<!-- seat details  -->
				<div id="<?php echo 'seat_details'.$i?>" class="w3-modal">
					  <div class="w3-modal-content w3-animate-top leader_details_modal">
					    <header class="w3-container w3-teal">
					      <span onclick="document.getElementById('seat_details'+<?php echo $i?>).style.display='none'"
					      class="w3-closebtn">&times;</span>
					      <h2><b>Election Seat :</b> <?php echo $es['name'] ?></h2>
					    </header>

					    <div class="w3-container">
					    	<h4>This Election Seat is consist of following Upazillas :</h4>
					     <?php  foreach ($election_seat_skeleton as $es_skl): ?>
					     	<?php if($es_skl['seat_id']==$es['seat_id']){?>
					     		 <p class="text-left w3-animate-zoom w3-card-2 w3-padding"><?php echo str_replace("_", " ", $es_skl['name'])?></p>
					        <?php } else continue;?>
					    <?php endforeach;?>
					    </div>
					    <hr>
					  </div>
				</div>
				<?php $i++; endforeach ;?>
				
				<!-- seat details  Ends-->
				<br>
				<br>
				<!-- seat leaders name and position -->
				<?php $i=0; foreach ($election_seat as $es): ?>
					
					<div id="<?php echo 'seat'.$i?>" class="w3-padding-medium w3-card-2 text-left w3-container w3-border seat  w3-animate-zoom">
						<ul class="w3-ul w3-card">

						<?php $j=0; foreach ($leader as $ld): ?>
							<?php if($es['seat_id']==$ld['seat_id']){?>


						    	<li class="w3-padding-16">
								   <img src="/img/avatar/no.png" class="w3-left w3-circle w3-margin-right" style="width:60px">
								    <span class="w3-xlarge"><?php echo $ld['name']?></span><br>
								    <span><?php echo $ld['current_position']?></span>
								    <a onclick="document.getElementById('leader_details'+<?php echo $j?>).style.display='block'" class="w3-small w3-text-blue" href="#">Learn More...</a>
								  </li>
								 

								  <!-- leaders details -->
								  	<div id="<?php echo 'leader_details'.$j?>" class="w3-modal">
									  <div class="w3-modal-content w3-animate-top leader_details_modal">
									    <header class="w3-container w3-teal">
									      <span onclick="document.getElementById('leader_details'+<?php echo $j?>).style.display='none'"
									      class="w3-closebtn">&times;</span>
									      <h2 class="w3-center"><?php echo $ld['name']?></h2>
									    </header>
									    <div class="w3-container">
									    <h3 class="w3-border-bottom w3-center w3-animate-right">Personal Info</h3>
									  
									      <p>
									      	<b>Current Position:</b> 
									      	<span class="w3-small w3-text-grey">
									      		<?php echo  str_replace("-", " ",$ld['current_position']) ?>	
									      		<?php if ($ld['current_position']=="MP-Candidate"){?>
									      			<span> [<?php echo $ld['year']?>]</span>
									      		<?php }?>
									        </span>
									      </p>

									      <p>
										    <b>Bio:</b>
									     	<span style="text-indent:20px;" class="w3-small w3-text-grey">
									     	 	<?php echo $ld['bio']?>
									     	</span>
									      </p>

									      <p>
									     	<b>Address:</b>
									        <span class="w3-small w3-text-grey">
									      		<?php echo $ld['address']?>
									        </span>
									      </p>


									      <p>
									     	<b>Contact:</b>
									        <span class="w3-small w3-text-grey">
									      		<?php echo $ld['contact']?>
									        </span>
									      </p>

									      <p>
									     	<b>Political Party:</b>
									        <span class="w3-small w3-text-grey">
									      		<?php echo  str_replace("-"," ",$ld['party'])?>
									        </span>
									      </p>
									      <?php if ($ld['current_position']!="MP-Candidate"){?>
									      		<h3 class="w3-border-bottom w3-center w3-animate-zoom">Former Position</h3>
									      <?php }?>
									  
									     	<?php foreach ($former as $former_position): ?>
									     		<?php if($ld['leader_id']==$former_position['leader_id_fpos']){?>

									     			<ul class="w3-ul w3-card-2 w3-animate-bottom w3-padding w3-text-grey">
									     				<li><b><?php echo str_replace("-", " ", $former_position['fposition'])." From ".$former_position['start']." to ".$former_position['end']?></b>
									     				</li>
									     			</ul>
									     

									     		<?php } else { ?>

									     		<?php continue;}?>
									     	<?php endforeach;?>
									    	<hr>

									    
									    </div>
									   
									  </div>
									</div>
				 				 <!-- leaders details ends-->
							
							<?php }?>

						<?php $j++; endforeach;?>
						 
						</ul>
					</div>
						<!-- seat leaders name and position end -->
				 <?php $i++; endforeach ;?>
		


				<hr>
				

			</center>

			<right class="w3-quarter w3-animate-right">
				 <h2 class="text-center w3-text-brown"> <span class="border-bottom-top"><i class="fa  fa-sitemap "></i> Upazillas</span></h2>
				 	<?php 	foreach ($upazilla as $up): ?>
					 <ul class="w3-ul w3-hoverable">

				 		 <li class="w3-border-bottom"><a class="no-underline" href="<?php echo $this->uri->segment(2,0)?>/upazilla/<?php echo  $up['name']?>"><i class="fa fa-street-view" aria-hidden="true"></i> <?php echo str_replace("_", " ", $up['name'])?></a></li>
					
					</ul>
					<?PHP endforeach;?>
			</right>
			
			<right class="w3-quarter w3-animate-right" style="margin-top:30px;">
			
				<!-- budget starts -->
					<div class="w3-card-8" style="width:100%;">
						<header class="w3-container w3-teal">
						  <h3 class="text-center"><i class="fa fa-suitcase fa-sm"></i> Budget</h3>
						</header>
						<?php foreach ($budget as $b): ?>
						<div class="w3-container w3-center">
						  <code><?php echo $b['amount']?> Crore Tk.</code>
						</div>
						
						<footer class="w3-container w3-teal">
						  <h6 class="w3-center ">Budget for <?php echo $b['year']?></h6>
						</footer>
						<?php endforeach; ?>
					</div>
				<!-- budget ends -->
			</right> 	
		</div>
		<!-- admin leader upazilla ends-->

		<!-- projects starts -->
		<hr>
		<div class="w3-container">
			 <h2 class="text-center w3-text-brown"> <span class="border-bottom-top">District Projects</span></h2>
			<div class="w3-row">
			  <span class="pointer" onclick="openProject(event, 'Ongoing');">
			    <div class="w3-third typelink w3-bottombar w3-hover-light-grey w3-padding">Ongoing</div>
			  </span>
			  <span class="pointer" href="#" onclick="openProject(event, 'Upcoming');">
			    <div class="w3-third typelink w3-bottombar w3-hover-light-grey w3-padding">Upcoming</div>
			  </span>
			  <span class="pointer" onclick="openProject(event, 'Completed');">
			    <div class="w3-third typelink w3-bottombar w3-hover-light-grey w3-padding">Completed</div>
			  </span>
			</div>
			 
			<div id="Ongoing" class="w3-row-padding w3-margin-top project w3-animate-opacity">

				 <?php $prev=""; foreach ($ongoing as $on): if($prev!=$on['p_id']){?>
				  <div class="w3-quarter">
					<div class="w3-card-2">
						<img src="<?php echo str_replace("./", "/", $on['img_link'])?>" style="width:100%;height:200px">
						<div class="w3-container w3-center">
							<h5><?php echo  $on['name']?></h5>
							<a href="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'Projects/Ongoing/'.$on['p_id']?>" class="w3-small">View Details</a>
							<br>
							<br>
						</div>
					</div>
				  </div>
				  <?php $prev=$on['p_id'];} else continue;?>
				<?php endforeach;?>
			  
			</div>

			<div id="Upcoming" class="w3-container w3-margin-top project w3-animate-opacity">
			 
				 <?php $prev=""; foreach ($upcoming as $up): if($prev!=$up['p_id']){?>
				  <div class="w3-quarter">
					<div class="w3-card-2">
						<img src="<?php echo str_replace("./", "/", $up['img_link'])?>" style="width:100%;height:200px"">
						<div class="w3-container w3-center">
							<h5><?php echo  $up['name']?></h5>
							<a href="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'Projects/Upcoming/'.$up['p_id']?>" class="w3-small">View Details</a>
							<br>
							<br>
						</div>
					</div>
				  </div>
				  <?php $prev=$up['p_id'];} else continue;?>
				<?php endforeach;?>
			  
			</div>

			<div id="Completed" class="w3-container w3-margin-top project w3-animate-opacity">
			<?php $prev=""; foreach ($completed as $com): if($prev!=$com['p_id']){?>
				  <div class="w3-quarter" >
					<div class="w3-card-2" style="margin-left:20px;">
						<img src="<?php echo str_replace("./", "/", $com['img_link'])?>" style="width:100%;height:200px"">
						<div class="w3-container w3-center">
							<h5><?php echo  $com['name']?></h5>
							<a href="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'Projects/Completed/'.$com['p_id']?>" class="w3-small">View Details</a>
							<br>
							<br>
						</div>
					</div>
				  </div>
				  <?php $prev=$com['p_id'];} else continue;?>
				<?php endforeach;?>
			</div>

		</div>
		<!-- projects ends-->
		<br>
		<br>
	</div>
	<!-- end body wrapper -->


	<footer class="w3-container w3-green site-footer">
  	  <p style="text-align:center">Copyrigth <i class="fa fa-copyright"></i> 2016. Bangladesh Govt.</p>
	</footer>



<script src="/js/district.js"></script>

</body>
</html>