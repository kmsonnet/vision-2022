<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>Super Admin</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css"> -->
<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"> -->
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">        
<link rel="stylesheet" type="text/css" href="/css/w3.css">
<link rel="stylesheet" type="text/css" href="/css/admin.css">
<script src="/js/admin.js"></script>
</head>

<body>
  <!-- body wrapper -->
  <div class="page-wrap">

	  <div class="w3-row w3-green w3-padding-8" >
	  	<h2 class="text-center w3-col l11 w3-animate-left">Welcome To Super Admin Panel</h2>
	  	<a href="/admin/logout" class="w3-col l1  w3-btn w3-red w3-border w3-border-red w3-small w3-animate-right" style="top:15px;position:relative;right:20px;text-decoration:underline">Logout</a>
	  </div>

	  <div class="w3-row w3-container">
	  <!-- left col -->
	  	<div class="w3-col l3 w3-border-right w3-padding-4" style="height:490px">
	  		<h4 class="w3-border-bottom w3-padding-8  w3-center w3-animate-zoom">Manage Districts & Election Seats</h4>

	  		<button class="w3-btn w3-round w3-teal  w3-center w3-animate-opacity" onclick="showDisNameField()">Add Districts</button>
	  		<button class="w3-btn w3-round w3-teal  w3-center w3-animate-opacity" onclick="showEsNameField()">Add Election Seats</button>

	  		<br>
	  		<br>
	  	
	  		<!-- district start -->
	  		<div id="district" class="w3-animate-left">
		  		<p>Select Division</p>
		  		<select name="division" form="disInField" class="text-left">
		  			<option value="Dhaka">Dhaka</option>
		  			<option value="Barisal">Barisal</option>
		  			<option value="Chittagong">Chittagong</option>
		  			<option value="Sylhet">Sylhet</option>
		  			<option value="Mymensingh">Mymensingh</option>
		  			<option value="Khulna">Khulna</option>
		  			<option value="Rajshahi">Rajshahi</option>
		  			<option value="Rangpur">Rangpur</option>
		  		</select>
		  		<br>
		  		<br>
		  		<p>How many district?</p>
		  		<input class=" w3-border w3-col s2" name="disNum" placeholder="0" min="0"  max="20" type="number" id="disNum">
		  		<br>
		  		<br>
		  		<button class="w3-btn w3-round w3-small w3-container w3-padding" onclick="createDistrictNameField()">Create Dis. Name Fields</button>
		    </div>
		    <!-- district end -->

		    <!-- ES start -->
	  		<div id="es" class="w3-animate-right">
		  		<p>Select District</p>
		  		<select name="district" form="esInField" class="text-left" id="disName">
		  			<?php foreach ($district_list as $d): ?>
                		<option value="<?php echo $d['dis_id']?>"><?php echo $d['name']?></option>
               		<?php endforeach; ?>
		  		</select>
		  		<br>
		  		<br>
		  		<p>How many Seats?</p>
		  		<input class=" w3-border w3-col s2" name="disNum" placeholder="0" min="0"  max="8" type="number" id="esNum">
		  		<br>
		  		<br>
		  		<button class="w3-btn w3-round w3-small w3-container w3-padding" onclick="createEsNameField()">Create ES Name Fields</button>
		    </div>
		    <!-- ES end -->


 			<!-- Admin start -->
		    <h4 class="w3-border-bottom w3-border-top w3-padding-8  w3-center w3-animate-zoom">Manage Administrators</h4>
		    <button class="w3-btn w3-round w3-teal  w3-center w3-animate-opacity" onclick="showAdminNameField()">Add Admins</button>
		    <br>
		    <br>
		   
	  		<div id="admin" class="w3-animate-right">
		  		<p>Select Admin Type</p>
		  		<select name="adminType" form="adminInField" class="text-left" id="adminType">
		  			 <option value="district">District Admin</option>
               		 <option value="upazilla">Upazilla Admin</option>
              		 <option value="pourosova">Pourosova Admin</option>
		  		</select>
		  		<br>
		  		<br>
		  		<button class="w3-btn w3-round w3-small w3-container w3-padding" onclick="createAdminNameField()">Create Admin Fields</button>
		    </div>
		    <!-- Admin end -->

		    <!-- budget start -->
		    <h4 class="w3-border-bottom w3-border-top w3-padding-8  w3-center w3-animate-zoom">Manage Budget</h4>
		   
	  		<div id="budget" class="w3-animate-right">
	  			<form method="post" action="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'addBudget'?>">
	  				<p>Select District</p>
			  		<select name="disid" class="text-left">
			  			<?php foreach ($district_list as $d): ?>
	                		<option value="<?php echo $d['dis_id']?>"><?php echo $d['name']?></option>
	               		<?php endforeach; ?>
			  		</select>
			  		<br>
			  		<br>
			  		<input class="w3-input w3-border w3-col l8" type="number" name="amount" placeholder="Enter Budget amount...">
			  		<br>
			  		<br>
			  		<input class="w3-input w3-border w3-col l8" type="text" name="year" placeholder="Enter Budget year...">
			  		<br>
			  		<br>
			  		<br>
			  		<button class="w3-btn w3-round w3-small w3-container w3-padding w3-teal"> ADD Budget</button>
	  			</form>
		  		
		    </div>
		    <!-- budget end -->

	  	</div>
	  	<!-- left col ends -->

	  	<!-- right col -->
	  	<div class="w3-container w3-col l9" >

	  		<h2 class="w3-text-green w3-animate-opacity">
		  		<?php
		  		  if($this->uri->segment(3)=="success")
		  		  	echo "Successfully Added";
		  		  if($this->uri->segment(3)=="failure")
		  		  	echo "Failure. Try Again !";
		  		 ?>
	  		</h2>

	  		<h2 class="text-center w3-border-bottom w3-animate-top" id="disInFieldText">
	  			Districts Input 
	  		</h2>
	  		<h2 class="text-center w3-border-bottom w3-animate-top" id="esInFieldText">
	  			Elelction Seats Input 
	  		</h2>
	  		<h2 class="text-center w3-border-bottom w3-animate-top" id="adminInFieldText"></h2>


	  		<form id="disInField" action="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'addDistrict'?>" method="post">
	  			<!-- <input type="text" name="dis0"> -->
	  			<button class="w3-btn w3-teal w3-animate-bottom" id="disSaveBtn">Save</button>
	  		</form>

	  		<form id="esInField" action="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'addEs'?>" method="post">
	  			<!-- <input type="text" name="dis0"> -->
	  			<button class="w3-btn w3-teal w3-animate-bottom" id="esSaveBtn">Save</button>
	  		</form>


	  		<form id="adminInField" action="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'addAdmin'?>" method="post">
	  			
	  			<button class="w3-btn w3-teal w3-animate-bottom" id="adminSaveBtn">Save</button>
	  		</form>

	  		
	  	</div>
	  	<!-- right col ends -->

	  </div>
	              
  </div>
  <!-- body wrapper ends-->
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <footer class="w3-container w3-green site-footer">
  	  <p style="text-align:center">Copyrigth <i class="fa fa-copyright"></i> 2016. Bangladesh Govt.</p>
  </footer>


</body>
</html>

