<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">

<head>
<title>CSE311-Project | Home</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css"> -->
<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"> -->
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">        
<link rel="stylesheet" type="text/css" href="/css/w3.css">
<link rel="stylesheet" type="text/css" href="/css/home.css">
<script src="/js/home.js"></script>
</head>

<body>
  <!-- body wrapper -->
  <div class="page-wrap">

  <!-- title section starts -->
    <div class="w3-container w3-light-green" style="padding:20px">
        <h2 style="text-align:center;font-size:36px">Welcome To Bangladesh Encyclopedia</h1>
    </div>
  <!-- title section ends -->

    <br>
    <br>

    <!-- search starts -->
    <form class="w3-container" style="text-align:center;">  
       <div style="width:60%;position:relative;left:20%">
         <input class="w3-input w3-border w3-animate-input"  type="text" placeholder="Type here ..." style="margin-bottom:20px;;width:55%;border-radius:5px;display:inline;">

         <button type="submit" class="w3-btn w3-red " style="margin-top:-5px;border-radius:5px;padding-bottom:12px;">Search <i class="fa fa-search"></i></button>
       </div> 
      
      <!-- <input class="w3-input w3-border w3-animate-input" type="text" style="width:30%"> -->
    </form>
    <!-- search ends -->

    <br>

    <!-- division heading starts -->
    <div class="w3-container">
      <h1 class="w3-xxxlarge " style="text-align:center"><i class="fa fa-home "></i> Divisions</h1>
    </div>
    <!-- division heading ends -->

    <br>

    <!-- first row of division list starts  -->
    <div class="w3-row" style="margin-top:1.15em">

      <!-- dhaka division starts -->
      <div class="w3-container w3-quarter text-center w3-animate-zoom">
        <button class="button" onclick="document.getElementById('dhaka').style.display='block'" class="w3-btn"><span>Dhaka</span></button>

        <!-- modal starts -->
        <div id="dhaka" class="w3-modal">
          <div style="width:300px" class="w3-modal-content w3-animate-top w3-card-8 modal_district_list">
            <header class="w3-container w3-red">
              <span onclick="document.getElementById('dhaka').style.display='none'"
              class="w3-closebtn">&times;</span>
              <h3 class="text-center">Districts Of Dhaka</h3>
            </header>
            <div class="w3-container text-left" style="padding:20px">
              <ol >
                <?php foreach ($dhaka as $d): ?>
                <li><a href="<?php echo $d['division_name'].'/'.$d['name']?>"><?php echo $d['name']; ?></a></li>
                <?php endforeach; ?>
              </ol>
            </div>
            <footer class="w3-container w3-blue-grey" style="padding:10px">
              <p>Click on District to see details...</p>
            </footer>
          </div>
        </div>
        <!-- modal ends -->

      </div>
      <!-- dhaka division ends -->


      <!-- Chittagong division starts -->
      <div class="w3-container w3-quarter text-center w3-animate-zoom">
        <button class="button" onclick="document.getElementById('Chittagong').style.display='block'" class="w3-btn"><span>Chittagong</span></button>

        <!-- modal starts -->
        <div id="Chittagong" class="w3-modal">
          <div style="width:300px" class="w3-modal-content w3-animate-top w3-card-8 modal_district_list">
            <header class="w3-container w3-red">
              <span onclick="document.getElementById('Chittagong').style.display='none'"
              class="w3-closebtn">&times;</span>
              <h3 class="text-center">Districts of Chittagong</h3>
            </header>
            <div class="w3-container text-left" style="padding:20px">
              <ol >
                <?php foreach ($chittagong as $d): ?>
                <li><a href="<?php echo $d['division_name'].'/'.$d['name']?>"><?php echo $d['name']; ?></a></li>
                <?php endforeach; ?>
              </ol>
            </div>
            <footer class="w3-container w3-blue-grey" style="padding:10px">
              <p>Click on District to see details...</p>
            </footer>
          </div>
        </div>
        <!-- modal ends -->

      </div>
      <!-- Chittagong division ends -->

      <!-- Shyllet division starts -->
      <div class="w3-container w3-quarter text-center w3-animate-zoom">
        <button class="button" onclick="document.getElementById('Shyllet').style.display='block'" class="w3-btn"><span>Sylhet</span></button>

        <!-- modal starts -->
        <div id="Shyllet" class="w3-modal">
          <div style="width:300px" class="w3-modal-content w3-animate-top w3-card-8 modal_district_list">
            <header class="w3-container w3-red">
              <span onclick="document.getElementById('Shyllet').style.display='none'"
              class="w3-closebtn">&times;</span>
              <h3 class="text-center">Districts of Sylhet</h3>
            </header>
            <div class="w3-container text-left" style="padding:20px">
              <ol >
                <?php foreach ($sylhet as $d): ?>
                <li><a href="<?php echo $d['division_name'].'/'.$d['name']?>"><?php echo $d['name']; ?></a></li>
                <?php endforeach; ?>
              </ol>
            </div>
            <footer class="w3-container w3-blue-grey" style="padding:10px">
              <p>Click on District to see details...</p>
            </footer>
          </div>
        </div>
        <!-- modal ends -->

      </div>
      <!-- Shyllet division ends -->

      <!-- Barisal division starts -->
      <div class="w3-container w3-quarter text-center w3-animate-zoom">
        <button class="button" onclick="document.getElementById('Barisal').style.display='block'" class="w3-btn"><span>Barisal</span></button>

        <!-- modal starts -->
        <div id="Barisal" class="w3-modal">
          <div style="width:300px" class="w3-modal-content w3-animate-top w3-card-8 modal_district_list">
            <header class="w3-container w3-red">
              <span onclick="document.getElementById('Barisal').style.display='none'"
              class="w3-closebtn">&times;</span>
              <h3 class="text-center">Districts of Barisal</h3>
            </header>
            <div class="w3-container text-left" style="padding:20px">
              <ol >
                <?php foreach ($barisal as $d): ?>
                <li><a href="<?php echo $d['division_name'].'/'.$d['name']?>"><?php echo $d['name']; ?></a></li>
                <?php endforeach; ?>
              </ol>
            </div>
            <footer class="w3-container w3-blue-grey" style="padding:10px">
              <p>Click on District to see details...</p>
            </footer>
          </div>
        </div>
        <!-- modal ends -->

      </div>
      <!-- Barisal division ends -->

      <!-- Khulna division starts -->
      <div class="w3-container w3-quarter text-center w3-animate-zoom">
        <button class="button" onclick="document.getElementById('Khulna').style.display='block'" class="w3-btn"><span>Khulna</span></button>

        <!-- modal starts -->
        <div id="Khulna" class="w3-modal">
          <div style="width:300px" class="w3-modal-content w3-animate-top w3-card-8 modal_district_list">
            <header class="w3-container w3-red">
              <span onclick="document.getElementById('Khulna').style.display='none'"
              class="w3-closebtn">&times;</span>
              <h3 class="text-center">Districts of Khulna</h3>
            </header>
            <div class="w3-container text-left" style="padding:20px">
              <ol >
                <?php foreach ($khulna as $d): ?>
                <li><a href="<?php echo $d['division_name'].'/'.$d['name']?>"><?php echo $d['name']; ?></a></li>
                <?php endforeach; ?>
              </ol>
            </div>
            <footer class="w3-container w3-blue-grey" style="padding:10px">
              <p>Click on District to see details...</p>
            </footer>
          </div>
        </div>
        <!-- modal ends -->

      </div>
      <!-- Khulna division ends -->


       <!-- Mymensingh division starts -->
      <div class="w3-container w3-quarter text-center w3-animate-zoom">
        <button class="button" onclick="document.getElementById('Mymensingh').style.display='block'" class="w3-btn"><span>Mymensingh</span></button>

        <!-- modal starts -->
        <div id="Mymensingh" class="w3-modal">
          <div style="width:350px" class="w3-modal-content w3-animate-top w3-card-8 modal_district_list">
            <header class="w3-container w3-red">
              <span onclick="document.getElementById('Mymensingh').style.display='none'"
              class="w3-closebtn">&times;</span>
              <h3 class="text-center">Districts of Mymensingh</h3>
            </header>
            <div class="w3-container text-left" style="padding:20px">
              <ol >
                <?php foreach ($mymensingh as $d): ?>
                <li><a href="<?php echo $d['division_name'].'/'.$d['name']?>"><?php echo $d['name']; ?></a></li>
                <?php endforeach; ?>
              </ol>
            </div>
            <footer class="w3-container w3-blue-grey" style="padding:10px">
              <p>Click on District to see details...</p>
            </footer>
          </div>
        </div>
        <!-- modal ends -->

      </div>
      <!-- Mymensingh division ends -->

       <!--Rajshahi division starts -->
      <div class="w3-container w3-quarter text-center w3-animate-zoom">
        <button class="button" onclick="document.getElementById('Rajshahi').style.display='block'" class="w3-btn"><span>Rajshahi</span></button>

        <!-- modal starts -->
        <div id="Rajshahi" class="w3-modal">
          <div style="width:300px" class="w3-modal-content w3-animate-top w3-card-8 modal_district_list">
            <header class="w3-container w3-red">
              <span onclick="document.getElementById('Rajshahi').style.display='none'"
              class="w3-closebtn">&times;</span>
              <h3 class="text-center">Districts of Rajshahi</h3>
            </header>
            <div class="w3-container text-left" style="padding:20px">
              <ol >
                <?php foreach ($rajshahi as $d): ?>
                <li><a href="<?php echo $d['division_name'].'/'.$d['name']?>"><?php echo $d['name']; ?></a></li>
                <?php endforeach; ?>
              </ol>
            </div>
            <footer class="w3-container w3-blue-grey" style="padding:10px">
              <p>Click on District to see details...</p>
            </footer>
          </div>
        </div>
        <!-- modal ends -->

      </div>
      <!--Rajshahi division ends -->

        <!--Rangpur division starts -->
      <div class="w3-container w3-quarter text-center w3-animate-zoom">
        <button class="button" onclick="document.getElementById('Rangpur').style.display='block'" class="w3-btn"><span>Rangpur</span></button>

        <!-- modal starts -->
        <div id="Rangpur" class="w3-modal">
          <div style="width:300px" class="w3-modal-content w3-animate-top w3-card-8 modal_district_list">
            <header class="w3-container w3-red">
              <span onclick="document.getElementById('Rangpur').style.display='none'"
              class="w3-closebtn">&times;</span>
              <h3 class="text-center">Districts of Rangpur</h3>
            </header>
            <div class="w3-container text-left" style="padding:20px">
              <ol >
                <?php foreach ($rangpur as $d): ?>
                <li><a href="<?php echo $d['division_name'].'/'.$d['name']?>"><?php echo $d['name']; ?></a></li>
                <?php endforeach; ?>
              </ol>
            </div>
            <footer class="w3-container w3-blue-grey" style="padding:10px">
              <p>Click on District to see details...</p>
            </footer>
          </div>
        </div>
        <!-- modal ends -->

      </div>
      <!--Rangpur division ends -->

    </div>
    <!-- first row of division list ends -->

    <br>
    <br>

</div>


<footer class="w3-container w3-light-green site-footer">
    <p style="text-align:center">Copyrigth <i class="fa fa-copyright"></i> 2016. Bangladesh Govt.</p>
</footer>

</body>
</html>

