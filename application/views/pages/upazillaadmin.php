<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php foreach ($up_id as $u):
       $u_id=$u['up_id'];
	 endforeach;
?>
<!DOCTYPE html>
<html lang="en">

<head>
<title><?php echo $username?> Admin</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
<!-- <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css"> -->
<link rel="stylesheet" type="text/css" href="/css/w3.css">
<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"> -->
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">        
<link rel="stylesheet" type="text/css" href="/css/admin.css">

<script src="/js/admin.js"></script>
</head>

<body>
 
 	<div class="page-wrap">

	  <div class="w3-row w3-green w3-padding-8" >
	  	<h2 class="text-center w3-col l11 w3-animate-left">Welcome To <?php echo $username?> Upazilla Admin Dashboard</h2>
	  	<a href="/admin/logout" class="w3-col l1  w3-btn w3-red w3-border w3-border-red w3-small w3-animate-right" style="top:15px;position:relative;right:20px;text-decoration:underline">Logout</a>
	  </div>

	    <div class="w3-row w3-container w3-padding-8">

	 	<!-- administration starts -->
	 		<div class="w3-col l3 w3-border w3-round w3-card-2 w3-animate-top" onmouseover="focusthis(this)" onmouseout="unfocusthis(this)" style="padding-left:10px;padding-right:10px">
	 			<h3 class="w3-border-bottom w3-center">Manage Administrations</h3>
	 			
	 			<form action="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'addUpAdmin'?>" method="post">
				<div >
				  <p  class="w3-btn  w3-medium w3-teal text-center">UPAZILA NIRBAHI OFFICERS</p>
				  <br>
				  <div id="unoInfo" class="w3-animate-zoom w3-show" style="margin-bottom:20px;margin-left:20px;margin-top:10px">
				   	<input type="text" name="unoname" placeholder="Enter UNO Name" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<br>
				   	<input type="text" name="unoaddress" placeholder="Enter UNO Office Address" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<br>
				   	<input type="text" name="unocontact" placeholder="Enter UNO Contact Number" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<br>
				   	<input type="date" name="unojoin" placeholder="Enter UNO Joining Date" class="w3-input w3-border w3-col l9 w3-small" required="required">
				   	<br>
				   	<input type="text" name="unoposition" value="Upazila Nirbahi Officer (UNO)" class="w3-input w3-border w3-col l9 w3-small w3-hide"> 
				   	<input type="text" name="adminUpId" value='<?php echo $u_id ?>' class="w3-input w3-border w3-col l9 w3-small w3-hide">
				  </div>
				  <hr>
				</div>
				<button class="w3-btn w3-green w3-small">Save</button>
				<hr>
				<h2 class="w3-text-green w3-animate-opacity w3-center">
		  		<?php
		  		  if($this->uri->segment(3)=="successadmin")
		  		  	echo "Successfully Added";
		  		  if($this->uri->segment(3)=="failureadmin")
		  		  	echo "Failure. Try Again !";
		  		 ?>
	  			</h2>
				</form>
			</div>
		<!-- administration ends -->


		<!-- ER starts -->
	 		<div class="w3-col l3 w3-border w3-round w3-card-2 w3-animate-bottom"  onmouseover="focusthis(this)" onmouseout="unfocusthis(this)" style="padding-left:20px;padding-right:20px;margin-left:30px">
	 			<h3 class="w3-border-bottom w3-center">Manage Elective Representative</h3>
	 			<h2 class="w3-text-green w3-animate-opacity w3-center">
			  			<?php
			  		 	 if($this->uri->segment(3)=="successer")
			  		 	 	echo "Successfully Added";
			  		 	 if($this->uri->segment(3)=="failureer")
			  		  		echo "Failure. Try Again !";
			  			 ?>
	  			</h2>
	 			<form action="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'addEr'?>" method="post">
	 				<p>Name</p>
	 				<input type="text" name="er_name" class="w3-input w3-border w3-small" placeholder="Enter Name" required="required">
	 				<br>
	 			
	 				<p>Select Position</p>
	 				<select class="w3-col l8" name="er_pos">
	 					<option value="Upazilla-Chairman">Upazilla Chairman</option>
	 					<option value="Upazilla-Vice-Chairman">Upazilla Vice Chairman</option>
	 					<option value="Upazilla-Female-Vice-Chairman">Upazilla Female Vice Chairman</option>
	 				</select>
	 				<br>
	 				<br>

	 				<p>Address</p>
	 				<input type="text" name="er_address" class="w3-input w3-border w3-small" placeholder="Enter Address" required="required">
	 				<br>

	 				<p>Contact Number</p>
	 				<input type="text" name="er_contact" class="w3-input w3-border w3-small" placeholder="Enter Contact Number" required="required">

	 				<p>Joining Date</p>
	 				<input type="date" name="er_join" class="w3-input w3-border w3-small" placeholder="Enter Join Date" required="required">
	 				<br>
	 				<p>Select Political Party</p>
	 				<select name='er_party'>
	 					<option value="Bangladesh-Awami-League">Bangladesh Awami League</option>
	 					<option value="Bangladesh-Nationalist-Party[BNP]">Bangladesh Nationalist Party[BNP]</option>
	 					<option value="Jatiya-Party">Jatiya Party</option>
	 					<option value="Jamaat-e-Islami-Bangladesh">Jamaat-e-Islami Bangladesh</option>
	 					<option value="Bangladesh-Jatiya-Party[BJP]">Bangladesh Jatiya Party [BJP]</option>
	 				</select>
	 				<br>
	 				<br>
					<input type="text" name="er_UpId" value='<?php echo $u_id ?>' class="w3-input w3-border w3-col l9 w3-small w3-hide">
					<button class="w3-btn w3-green ">Save</button>
					<br>
					<br>
					
	 			</form>

	 		</div>
	 	<!-- ER ends-->

	 	<!-- Upazilla starts -->
	 		<div class="w3-col l2 w3-border w3-card-2 w3-animate-top" onmouseover="focusthis(this)" onmouseout="unfocusthis(this)" style="padding-left:10px;padding-right:10px;margin-left:30px">
	 			<h3 class="w3-border-bottom w3-center">Manage Union</h3>

	 			<h4 class="w3-text-green w3-animate-opacity w3-center">
		  		<?php
		  		  if($this->uri->segment(3)=="successunion")
		  		  	echo "Successfully Added";
		  		  if($this->uri->segment(3)=="failureunion")
		  		  	echo "Failure. Try Again !";
		  		 ?>
	  			</h4>
	  			
	 			<p>How many Unions?</p>
	 			<input type="number" id='unNum' placeholder="0" min="0" max="10">
	 			<button class="w3-btn w3-tiny" style="margin-top:-5px" onclick="createUnionField()">Create</button>
	 			<br>
	 
	 			<div id="upazillaCon">
		 			<form  action="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'addUnion'?>" method="post">
		 				<br>
		 				<div id="upinputCon">
		 					
		 				</div>
		 				<!-- <button class="w3-btn w3-green">Save</button> -->
		 				<hr>
		 			
		 				 <input type="text" name="er_UpId" value='<?php echo $u_id ?>' class="w3-input w3-border w3-col l9 w3-small w3-hide">
		 			</form>
	 			</div>

	 		</div>
	 	<!-- Upazilla ends-->


	 	<!-- CH starts -->
	 		<div class="w3-col l3 w3-border w3-round w3-card-2 w3-animate-bottom"  onmouseover="focusthis(this)" onmouseout="unfocusthis(this)" style="padding-left:20px;padding-right:20px;margin-left:30px">
	 			<h3 class="w3-border-bottom w3-center">Manage Union Chairman</h3>
	 			<h2 class="w3-text-green w3-animate-opacity w3-center">
			  			<?php
			  		 	 if($this->uri->segment(3)=="successch")
			  		 	 	echo "Successfully Added";
			  		 	 if($this->uri->segment(3)=="failurech")
			  		  		echo "Failure. Try Again !";
			  			 ?>
	  			</h2>
	 			<form action="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'addCh'?>" method="post">
	 				<p>Name</p>
	 				<input type="text" name="ch_name" class="w3-input w3-border w3-small" placeholder="Enter Name" required="required">
	 				<br>
	 			
	 				<p>Select Union</p>
	 				<select class="w3-col l8" name="ch_union">
	 				<?php foreach ($union as $un):?>
	 					<option value="<?php echo $un['un_id']?>"><?php echo str_replace("_"," ",$un['name'])?></option>
	 				<?php endforeach;?>
	 				</select>
	 				<br>
	 				<br>
	 				<input type="text" name="ch_pos" class="w3-input w3-border w3-small w3-hide" value='Chairman'>
	 				<p>Address</p>
	 				<input type="text" name="ch_address" class="w3-input w3-border w3-small" placeholder="Enter Address" required="required">
	 				<br>

	 				<p>Contact Number</p>
	 				<input type="text" name="ch_contact" class="w3-input w3-border w3-small" placeholder="Enter Contact Number" required="required">

	 				<p>Joining Date</p>
	 				<input type="date" name="ch_join" class="w3-input w3-border w3-small" placeholder="Enter Join Date" required="required">
	 				<br>
	 				<p>Select Political Party</p>
	 				<select name='ch_party'>
	 					<option value="Bangladesh-Awami-League">Bangladesh Awami League</option>
	 					<option value="Bangladesh-Nationalist-Party[BNP]">Bangladesh Nationalist Party[BNP]</option>
	 					<option value="Jatiya-Party">Jatiya Party</option>
	 					<option value="Jamaat-e-Islami-Bangladesh">Jamaat-e-Islami Bangladesh</option>
	 					<option value="Bangladesh-Jatiya-Party[BJP]">Bangladesh Jatiya Party [BJP]</option>
	 				</select>
	 				<br>
	 				<br>
					
					<button class="w3-btn w3-green ">Save</button>
					<br>
					<br>
					
	 			</form>

	 		</div>
	 	<!-- CH ends-->
		</div>

		<!-- 2nd row starts -->
			<div class="w3-row w3-container w3-padding-8">

	 	<!-- Upload starts -->
	 		<div class="w3-col l4 w3-border w3-card-2 w3-animate-top" onmouseover="focusthis(this)" onmouseout="unfocusthis(this)" style="padding-left:10px;padding-right:10px;margin-left:30px">
	 			<h3 class="w3-border-bottom w3-center">Manage Project</h3>

	 			<h4 class="w3-text-green w3-animate-opacity w3-center">
		  		<?php
		  		  if($this->uri->segment(3)=="successproject")
		  		  	echo "Successfully Added";
		  		  if($this->uri->segment(3)=="failureproject")
		  		  	echo "Failure. Try Again !";
		  		 ?>
	  			</h4>
	  			
	 		
		 		<form  action="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'addProject'?>" method="post" enctype="multipart/form-data">
		 			<p>Project Name</p>
		 			<input type="text" name="pname" class="w3-input w3-border w3-small" placeholder="Enter Project Name" required="required">
		 			<br>
		 			<p>Contractor Name</p>
		 			<input type="text" name="pcname" class="w3-input w3-border w3-small" placeholder="Enter Contractor  Name" required="required">
		 			<br>
		 			<p>Select Project Type</p>
		 			<select name="ptype" class="w3-col l5" width="200px">
		 				<option value="Upcoming">Upcoming</option>
		 				<option value="Ongoing">Ongoing</option>
		 				<option value="Completed">Completed</option>
		 			</select>
		 			<br>
		 			<br>
		 			<p>Project Start Date</p>
		 			<input type="date" name="sdate" class="w3-input w3-border w3-small" required="required">
		 			<br>
		 			<p>Project Finish Date</p>
		 			<input type="date" name="edate" class="w3-input w3-border w3-small" required="required">
		 			<br>
		 			<p>Project Budget</p>
		 			<input type="text" name="pbudget" class="w3-input w3-border w3-small" placeholder="Enter Project Budget" required="required">
					<br>
		 			<p>Select Project Location</p>
		 			<select class="w3-col l5" name="plocation">
	 				<?php foreach ($union as $un):?>
	 					<option value="<?php echo $un['un_id']?>"><?php echo str_replace("_"," ",$un['name'])?></option>
	 				<?php endforeach;?>
	 				</select>		 			<br>
		 			<br>		
		 			<p>Project Brief Description</p>
	 				<textarea rows="4" cols="42" placeholder="Describe project here..." name='pdes' required="required"></textarea>
	 				<p>Project Progress</p>
		 			<input type="number" name="progress" class="w3-input w3-border w3-small" placeholder="Enter Project Progress" required="required">
					<br>
		 			<p>Upload Some Project Image</p>
		 			<p>Interval Start Date</p>
		 			<input type="date" name="isdate" class="w3-input w3-border w3-small" required="required">
		 			<br>
		 			<p>Interval End Date</p>
		 			<input type="date" name="iedate" class="w3-input w3-border w3-small" required="required">
		 			<br>
		 			 <input type="file" id="file" name="userfile[]" multiple="multiple" accept="image/*"/>
		 			 <br>
		 			 <br>
		 			 <button class="w3-btn w3-green ">Save</button>
		 			 <br>
		 			 <br>
		 		</form>

	 		</div>
	 	<!-- Upload ends-->

	 	<!-- Progress ENds -->
	 		<div class="w3-col l4 w3-border w3-card-2 w3-animate-top" onmouseover="focusthis(this)" onmouseout="unfocusthis(this)" style="padding-left:10px;padding-right:10px;margin-left:30px">
	 			<h3 class="w3-border-bottom w3-center">Update Project Progress</h3>

	 			<h4 class="w3-text-green w3-animate-opacity w3-center">
		  		<?php
		  		  if($this->uri->segment(3)=="successprogress")
		  		  	echo "Successfully Added";
		  		  if($this->uri->segment(3)=="failureprogress")
		  		  	echo "Failure. Try Again !";
		  		 ?>
	  			</h4>
	  			
	 		
		 		<form  action="/<?php echo $this->uri->segment(1,0).'/'.$this->uri->segment(2,0).'/'.'addProgress'?>" method="post" enctype="multipart/form-data">
		 			
		 			<p>Select Project </p>
		 			<select class="w3-col l5" name="pid">
	 				<?php foreach ($ongoing_project as $op):?>
	 					<option value="<?php echo $op['p_id']?>"><?php echo $op['name']?></option>
	 				<?php endforeach;?>
	 				</select>		 			
	 				<br>
		 			<br>		
	 				<p>Progress %</p>
		 			<input type="number" name="progress" class="w3-input w3-border w3-small" placeholder="Enter Project Progress" required="required">
					<br>
		 			<p>Upload Some Project Image</p>
		 			<p>Interval Start Date</p>
		 			<input type="date" name="isdate" class="w3-input w3-border w3-small" required="required">
		 			<br>
		 			<p>Interval End Date</p>
		 			<input type="date" name="iedate" class="w3-input w3-border w3-small" required="required">
		 			<br>
		 			 <input type="file" id="file" name="userfile[]" multiple="multiple" accept="image/*"/>
		 			 <br>
		 			 <br>
		 			 <button class="w3-btn w3-green ">Save</button>
		 			 <br>
		 			 <br>
		 		</form>

	 		</div>
	 	<!-- PROGRESS ends-->
			</div>
		<!-- 2nd row ends -->

	</div>
</body>
</html>