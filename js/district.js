function admin(x) {
    var x = x.nextElementSibling;
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
}
function close_pop(y){
  var y=y.parentElement;
  y.className +='close-pop';
  y.className = y.className.replace(" w3-show", "");
}

function openseat(evt, seatName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("seat");
  for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
  }
  document.getElementById(seatName).style.display = "block";
  evt.currentTarget.className += " w3-red";
}
function seat_active() {
  var x = document.getElementsByClassName("seat");
  x[0].style.display="block";

  var tablinks = document.getElementsByClassName("tablink");
  tablinks[0].className+= " w3-red";

}

function openProject(evt, projectName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("project");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("typelink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
  }
  document.getElementById(projectName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-border-red";
}

function project_active() {
  var x = document.getElementsByClassName("project");
  x[0].style.display="block";

  var tablinks = document.getElementsByClassName("typelink");
  tablinks[0].className+= " w3-border-red";

}