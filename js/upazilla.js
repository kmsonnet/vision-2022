function admin(x) {
    var x = x.nextElementSibling;
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
}
function close_pop(y){
  var y=y.parentElement;
  y.className +='close-pop';
  y.className = y.className.replace(" w3-show", "");
}
function unionAccordions(id) {
    var x = document.getElementById(id);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
}

document.getElementsByClassName("tablink")[0].click();

function openChairman(evt, ChairmanName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("chairman");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("unilink");
  for (i = 0; i < x.length; i++) {
    tablinks[i].classList.remove("w3-light-grey");
  }
  document.getElementById(ChairmanName).style.display = "block";
  evt.currentTarget.classList.add("w3-light-grey");
}

function chairman_active() {
  var x = document.getElementsByClassName("chairman");
  x.style.display="block";

  var tablinks = document.getElementsByClassName("unilink");
  tablinks.className+= " w3-light-grey";

}

function openProject(evt, projectName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("project");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("typelink");
  for (i = 0; i < x.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
  }
  document.getElementById(projectName).style.display = "block";
  evt.currentTarget.firstElementChild.className += " w3-border-red";
}

function project_active() {
  var x = document.getElementsByClassName("project");
  x[0].style.display="block";

  var tablinks = document.getElementsByClassName("typelink");
  tablinks[0].className+= " w3-border-red";

}

