function showDisNameField(){
	document.getElementById("es").style.display="none";
	document.getElementById("admin").style.display="none";
	document.getElementById("district").style.display="block";

}
function createDistrictNameField(){
	document.getElementById("esInFieldText").style.display="none";
	document.getElementById("esInField").style.display="none";
	document.getElementById("adminInFieldText").style.display="none";
	document.getElementById("adminInField").style.display="none";
	var dis_num=document.getElementById("disNum").value;
	var container=document.getElementById("disInField");
	if(container.getElementsByTagName('input').length >=1){
			while (container.firstChild) {
  		    container.removeChild(container.firstChild);
		}
	}
	var button= document.createElement("button");
		button.className="w3-btn w3-teal w3-animate-bottom";
		button.innerHTML="Save";
		container.appendChild(button);

	for(var i=0; i<dis_num; i++){

	
		var input =document.createElement("INPUT");
		if(i%2==0)
			input.className="w3-input w3-border w3-col l3 w3-animate-left";
		else 
			input.className="w3-input w3-border w3-col l3 w3-animate-right";

		input.setAttribute("placeholder", "Enter District-"+(i+1)+" Name");
		input.setAttribute("name", "dis[]");
		input.style.marginRight="70px";
		input.style.marginBottom="20px";
		container.insertBefore(input, container.childNodes[0]);
	}
	// document.getElementById('disSaveBtn').style.display="block";
	document.getElementById('disInFieldText').style.display="block";
	document.getElementById("disInField").style.display="block";

}
function showEsNameField(){
	document.getElementById("district").style.display="none";
	document.getElementById("admin").style.display="none";
	document.getElementById("es").style.display="block";
}
function createEsNameField(){

	document.getElementById("disInFieldText").style.display="none";
	document.getElementById("disInField").style.display="none";
	document.getElementById("adminInFieldText").style.display="none";
	document.getElementById("adminInField").style.display="none";

	var dis_num=document.getElementById("esNum").value;
	var dis_name=document.getElementById("disName");
	var v=dis_name.options[dis_name.selectedIndex].text;
	var container=document.getElementById("esInField");
	if(container.getElementsByTagName('input').length >=1){
			while (container.firstChild) {
  		    container.removeChild(container.firstChild);
		}
	}
	var button= document.createElement("button");
		button.className="w3-btn w3-teal w3-animate-bottom";
		button.innerHTML="Save";
		container.appendChild(button);
	for(var i=0; i<dis_num; i++){

		var input =document.createElement("INPUT");
		if(i%2==0)
			input.className="w3-input w3-border w3-col l3 w3-animate-left";
		else 
			input.className="w3-input w3-border w3-col l3 w3-animate-right";

		input.setAttribute("placeholder", "Enter Seat-"+(i+1)+" Name");
		input.setAttribute("value", v+"-"+(i+1));
		input.setAttribute("name", "es[]");
		input.style.marginRight="70px";
		input.style.marginBottom="20px";
		container.insertBefore(input, container.childNodes[0]);

	}
	// document.getElementById('esSaveBtn').style.display="block";
	document.getElementById('esInFieldText').style.display="block";
	document.getElementById("esInField").style.display="block";

}
function showAdminNameField(){
	document.getElementById("district").style.display="none";
	document.getElementById("es").style.display="none";
	document.getElementById("admin").style.display="block";
	
}

function createAdminNameField(){
	document.getElementById("esInFieldText").style.display="none";
	document.getElementById("esInField").style.display="none";
	document.getElementById("disInFieldText").style.display="none";
	document.getElementById("disInField").style.display="none";
	var admin_type=document.getElementById("adminType");
	document.getElementById("adminInFieldText").innerHTML=admin_type.options[admin_type.selectedIndex].text;

	var container=document.getElementById("adminInField");

	if(container.getElementsByTagName('input').length >1){
		container.removeChild(container.childNodes[0]);
		container.removeChild(container.childNodes[1]);
	}
	var input1 =document.createElement("INPUT");
		input1.className="w3-input w3-border w3-col l3 w3-animate-right";
		input1.setAttribute("placeholder", "Enter General Admin ID");
		input1.setAttribute("name", "adminId");
		input1.style.marginRight="70px";
		input1.style.marginBottom="20px";
		container.insertBefore(input1, container.childNodes[1]);
		
	var input2 =document.createElement("INPUT");
		input2.className="w3-input w3-border w3-col l3 w3-animate-right";
		input2.setAttribute("placeholder", "Set Default Password");
		input2.setAttribute("name", "defaultPass");
		input2.style.marginRight="70px";
		input2.style.marginBottom="20px";	
		container.insertBefore(input2, container.childNodes[0]);

	document.getElementById("adminSaveBtn").style.display="block";
	document.getElementById("adminInFieldText").style.display="block";
	document.getElementById("adminInField").style.display="block";


}


// DISTRICT Admin
function dcAccording(id) {
    var x = document.getElementById(id);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
}

function createMpCandYear(x){
	var container= document.getElementById('mpcan');
	if(container.hasChildNodes()){
			while (container.firstChild) {
  		    container.removeChild(container.firstChild);
		}
	}
	if(x.value=='MP-Candidate'){
		
		var input=document.createElement('INPUT');
		input.className="w3-input w3-border w3-small";
		input.setAttribute('type', 'text');
		input.setAttribute('name', 'mpcanyr');
		input.setAttribute('placeholder', 'Enter years separated by comma [Ex. 2012, 2013, 2014]');
		container.appendChild(input);
	}
}

function focusthis(x) {
	// var this =document.getElementById("leader");
	x.className=x.className.replace(" w3-card-2", " w3-card-8 w3-border-teal");
}
function unfocusthis(x) {
	// var x =document.getElementById("leader");
	x.className=x.className.replace(" w3-card-8 w3-border-teal", " w3-card-2");
}

function showFormer() {
	var radios = document.getElementsByName('formerPos');

for (var i = 0, length = radios.length; i < length; i++) {
	    if (radios[0].checked) {
	        document.getElementById("formerPosNum").style.display="block";
	         document.getElementById('formerCon').style.display="block";
	         break;
			}
		}

}

function hideFormer() {
	var radios = document.getElementsByName('formerPos');

  for (var i = 0, length = radios.length; i < length; i++) {
	    if (radios[1].checked) {
	        document.getElementById("formerPosNum").style.display="none";
	        document.getElementById('formerCon').style.display="none";
	         break;
			}
		}
	
}

function createFormerPositionField() {

	var numOfField =document.getElementById("numOfField").value;
	var container=document.getElementById("formerCon");
	if(container.hasChildNodes()){
			while (container.firstChild) {
  		    container.removeChild(container.firstChild);
		}
	}
	for (var i =0; i<numOfField; i++) {
		var h3=document.createElement("H3");
		h3.innerHTML="Position-"+(i+1);
		container.appendChild(h3);

		var hr=document.createElement("hr");
		container.appendChild(hr);

		var select = document.createElement("SELECT");
	    select.setAttribute("id", "posSelect"+i);
	    select.setAttribute("name", "posSelect[]");
	    container.appendChild(select);

	    var option1 = document.createElement("option");
	    var option2 = document.createElement("option");

	    option1.setAttribute("value", "Member-of-Parliament (MP)");
	    option2.setAttribute("value", "Minister");

	    var t1 = document.createTextNode("Member of Parliament");
	    var t2 = document.createTextNode("Minister");

	    option1.appendChild(t1);
	    option2.appendChild(t2);

	    document.getElementById("posSelect"+i).appendChild(option1);
	    document.getElementById("posSelect"+i).appendChild(option2);

	    var br1=document.createElement('BR');
	    var br2=document.createElement('BR');
	    container.appendChild(br1);
	    container.appendChild(br2);

	    var pstart=document.createElement('p');
	    pstart.innerHTML="Enter Start Priod";
	    container.appendChild(pstart);

	    var inputstart=document.createElement('INPUT');
	    inputstart.className="w3-col l6 w3-border w3-input";
	    inputstart.setAttribute("type", "month");
	    inputstart.setAttribute("name", "fstart[]");
	    inputstart.setAttribute("required", "required");
	    container.appendChild(inputstart);

	    var br3=document.createElement('BR');
	    var br4=document.createElement('BR');
	    container.appendChild(br3);
	    container.appendChild(br4);

	    var pend=document.createElement('p');
	    pend.innerHTML="Enter End Priod";
	    container.appendChild(pend);

	    var inputend=document.createElement('INPUT');
	    inputend.className="w3-col l6 w3-border w3-input";
	    inputend.setAttribute("type", "month");
	    inputend.setAttribute("name", "fend[]");
	    inputend.setAttribute("required", "required");
	    container.appendChild(inputend);

	    var br5=document.createElement('BR');
	    var br6=document.createElement('BR');
	    container.appendChild(br5);
	    container.appendChild(br6);

	    var hr1=document.createElement("hr");
		container.appendChild(hr1);
	}
	
}

function createUpazillaField(){
		var upnum=document.getElementById('upNum').value;
		var container=document.getElementById('upinputCon');
		if(container.hasChildNodes()){
				while (container.firstChild) {
  		  	    container.removeChild(container.firstChild);
			}
		}
		for (var i =1; i<=upnum; i++) {
		 
		 var pstart=document.createElement('p');
	     pstart.innerHTML="Upazilla"+i;
	     container.appendChild(pstart);

	    var inputstart=document.createElement('INPUT');
	    inputstart.className="w3-input w3-border w3-small";
	    inputstart.setAttribute("type", "text");
	    inputstart.setAttribute("name", "upname[]");
	    inputstart.setAttribute("placeholder", "Enter Upazilla Name");
	    inputstart.setAttribute("required", "required");
	    container.appendChild(inputstart);


		var select = document.createElement("SELECT");
	    select.setAttribute("id", "upes"+i);
	    select.setAttribute("name", "upes[]");
	    container.appendChild(select);

	    var x = document.getElementById("election_seat").options.length;
	    for(var j=0;j<x;j++){
	    	var option1 = document.createElement("option");
		   	var v= document.getElementById("election_seat").options.item(j).value;
		   	var t= document.getElementById("election_seat").options.item(j).text;
		    option1.setAttribute("value", v);
		    var t1 = document.createTextNode(t);
		    option1.appendChild(t1);
		    document.getElementById("upes"+i).appendChild(option1);		  
	    }
	      var br1=document.createElement('BR');
		    var br2=document.createElement('BR');
		  
	      container.appendChild(br1);
		    container.appendChild(br2);
	  
	}
	var btn =document.createElement("button");
	btn.className=" w3-btn w3-green";
	btn.innerHTML="Save";
	container.appendChild(btn);
}

function createUnionField(){
		var unnum=document.getElementById('unNum').value;
		var container=document.getElementById('upinputCon');
		if(container.hasChildNodes()){
				while (container.firstChild) {
  		  	    container.removeChild(container.firstChild);
			}
		}
		for (var i =1; i<=unnum; i++) {
		 
		 var pstart=document.createElement('p');
	     pstart.innerHTML="Union-"+i;
	     container.appendChild(pstart);

	    var inputstart=document.createElement('INPUT');
	    inputstart.className="w3-input w3-border w3-small";
	    inputstart.setAttribute("type", "text");
	    inputstart.setAttribute("name", "unname[]");
	    inputstart.setAttribute("placeholder", "Enter Union Name");
	    inputstart.setAttribute("required", "required");
	    container.appendChild(inputstart);
	      var br1=document.createElement('BR');
		    var br2=document.createElement('BR');
		  
	      container.appendChild(br1);
		    container.appendChild(br2);
	  
	}
	var btn =document.createElement("button");
	btn.className=" w3-btn w3-green";
	btn.innerHTML="Save";
	container.appendChild(btn);
}