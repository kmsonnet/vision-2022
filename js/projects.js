
// function Pouroshobhas(y) {
//     var x = document.getElementById("Pouroshobhas");

//     if (y.className.indexOf("fa-minus-square") == -1) {
//         y.className = y.className.replace( " fa-plus-square"," fa-minus-square");
//     }
//     else{
//         y.className = y.className.replace(" fa-minus-square", " fa-plus-square");
//     }

//     if (x.className.indexOf("w3-show") == -1) {
//         x.className += " w3-show";
//         x.previousElementSibling.className = x.previousElementSibling.className.replace( " w3-teal"," w3-brown");
//     } else {
//         x.className = x.className.replace(" w3-show", "");
//         x.previousElementSibling.className =
//         x.previousElementSibling.className.replace(" w3-brown", " w3-teal");
//     }
// }

function Project(y) {
    // var x = document.getElementsByClassName("project");

    if (y.firstChild.className.indexOf("fa-minus-square") == -1) {
        y.firstChild.className = y.firstChild.className.replace( " fa-plus-square"," fa-minus-square");
    }
    else{
        y.firstChild.className = y.firstChild.className.replace(" fa-minus-square", " fa-plus-square");
    }

    if (y.nextElementSibling.className.indexOf("w3-show") == -1) {
        y.nextElementSibling.className += " w3-show";
        y.nextElementSibling.className = y.nextElementSibling.className.replace( " w3-teal"," w3-brown");
    } else {
        y.nextElementSibling.className = y.nextElementSibling.className.replace(" w3-show", "");
        y.nextElementSibling.className =
        y.nextElementSibling.className.replace(" w3-brown", " w3-teal");
    }
}

$(document).ready(function(){
    var pathname=window.location.pathname.split( '/' );
    var get_Pid=pathname[5];
    $("[href="+get_Pid+"]" ).addClass("w3-grey").closest("div").addClass("w3-show").prev().find("i").removeClass("fa fa-plus-square").addClass(" fa fa-minus-square ");
});


// slide container
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  var i;
  var container=document.getElementById("conSlide");
  container.children[0].className="mySlides w3-animate-left w3-round w3-card-4 full-width";
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");

  if (n > x.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = x.length;
  }
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" w3-bottombar w3-border-orange w3-border", "");
  }
  x[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " w3-bottombar w3-border-orange w3-border";
}

function createImg(y) {
  var container=document.getElementById("conSlide");
  if(container.hasChildNodes()){
    container.innerHTML="";
  }
  var thumbCon=document.getElementById("thumbnail");
  var thumb,j;

    for(j=0;j<thumbCon.children.length;j++)
      thumbCon.children[j].className="w3-hide";


    for(i=0;i<y.children.length;i++){
      img=document.createElement("img");
      img.setAttribute("width", "300px");
      img.src=y.children[i].src;
      img.className="mySlides w3-animate-opacity w3-round w3-card-4 full-width";
      container.appendChild(img);

      thumb=thumbCon.children[i];
      thumb.className="w3-col s2";
      thumb.children[0].src=y.children[i].src;
   }
   var x=thumbCon.children[0];

  container.children[0].className="w3-animate-opacity w3-round w3-card-4 full-width";
  x.children[0].className+= " w3-bottombar w3-border-orange w3-border";
}

function default_project() {
  var container=document.getElementById("conSlide");
  var prentCon=document.getElementById("intervalCon");

  var thumbCon=document.getElementById("thumbnail");
 
  var y=prentCon.children[0];
  var thumb;

  if(container.hasChildNodes()){
    container.innerHTML="";
  }

    for(i=0;i<y.children.length;i++){
      var img=document.createElement("img");
      img.src=y.children[i].src;
      img.className="mySlides w3-animate-opacity w3-round w3-card-4 full-width";
      container.appendChild(img);

      thumb=thumbCon.children[i];
      thumb.className="w3-col s2";
      thumb.children[0].src=y.children[i].src;
   }
  var x=thumbCon.children[0];
  container.children[0].className="w3-animate-opacity w3-round w3-card-4 full-width";
  x.children[0].className+= " w3-bottombar w3-border-orange w3-border";
}